var deleteSvgIcon = "<svg width='38' height='38' viewBox='0 0 38 38' fill='none' xmlns='http://www.w3.org/2000/svg'>\
                        <g opacity='0.5'>\
                        <g filter='url(#filter0_d)'>\
                        <circle cx='19' cy='17' r='17' fill='black'></circle>\
                        <circle cx='19' cy='17' r='16.25' stroke='white' stroke-width='1.5'></circle>\
                        </g>\
                        <path d='M18.125 16H16.375V22H18.125V16Z' fill='white'></path>\
                        <path d='M21.625 16H19.875V22H21.625V16Z' fill='white'></path>\
                        <path d='M22.5 10C22.5 9.4 22.15 9 21.625 9H16.375C15.85 9 15.5 9.4 15.5 10V12H12V14H12.875V24C12.875 24.6 13.225 25 13.75 25H24.25C24.775 25 25.125 24.6 25.125 24V14H26V12H22.5V10ZM17.25 11H20.75V12H17.25V11ZM23.375 14V23H14.625V14H23.375Z' fill='white'></path>\
                        </g>\
                        <defs>\
                        <filter id='filter0_d' x='0' y='0' width='38' height='38' filterUnits='userSpaceOnUse' color-interpolation-filters='sRGB'>\
                        <feFlood flood-opacity='0' result='BackgroundImageFix'></feFlood>\
                        <feColorMatrix in='SourceAlpha' type='matrix' values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'></feColorMatrix>\
                        <feOffset dy='2'></feOffset>\
                        <feGaussianBlur stdDeviation='1'></feGaussianBlur>\
                        <feColorMatrix type='matrix' values='0 0 0 0 0.375 0 0 0 0 0.375 0 0 0 0 0.375 0 0 0 0.15 0'></feColorMatrix>\
                        <feBlend mode='normal' in2='BackgroundImageFix' result='effect1_dropShadow'></feBlend>\
                        <feBlend mode='normal' in='SourceGraphic' in2='effect1_dropShadow' result='shape'></feBlend>\
                        </filter>\
                        </defs>\
                      </svg>";

var closeSvgIcon = "<svg width='40' height='40' viewBox='0 0 40 40' fill='none' xmlns='http://www.w3.org/2000/svg'>\
                        <g opacity='0.5'>\
                            <g filter='url(#filter0_d)'>\
                                <circle cx='20' cy='18' r='18' fill='black'></circle>\
                                <circle cx='20' cy='18' r='17.25' stroke='white' stroke-width='1.5'></circle>\
                            </g>\
                            <path d='M26.7498 11.25L13.25 24.7498' stroke='white' stroke-width='2' stroke-miterlimit='10' stroke-linecap='square'></path>\
                            <path d='M26.7498 24.7498L13.25 11.25' stroke='white' stroke-width='2' stroke-miterlimit='10' stroke-linecap='square'></path>\
                        </g>\
                        <defs>\
                            <filter id='filter0_d' x='0' y='0' width='40' height='40' filterUnits='userSpaceOnUse' color-interpolation-filters='sRGB'>\
                                <feFlood flood-opacity='0' result='BackgroundImageFix'></feFlood>\
                                <feColorMatrix in='SourceAlpha' type='matrix' values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'></feColorMatrix>\
                                <feOffset dy='2'></feOffset>\
                                <feGaussianBlur stdDeviation='1'></feGaussianBlur>\
                                <feColorMatrix type='matrix' values='0 0 0 0 0.375 0 0 0 0 0.375 0 0 0 0 0.375 0 0 0 0.15 0'></feColorMatrix>\
                                <feBlend mode='normal' in2='BackgroundImageFix' result='effect1_dropShadow'></feBlend>\
                                <feBlend mode='normal' in='SourceGraphic' in2='effect1_dropShadow' result='shape'></feBlend>\
                            </filter>\
                       </defs>\
                    </svg>";

var quoteSvgIcon = "<svg width='17' height='15' viewBox='0 0 17 15' fill='none' xmlns='http://www.w3.org/2000/svg'>\
                      <path d='M3.24397 7.73301C3.20544 7.00638 3.45214 6.08913 4.22268 5.1409C4.89767 4.31026 5.69758 3.70735 6.62486 3.32223L7.12871 3.11296L6.87639 2.62923L6.42756 1.76876L6.20673 1.3454L5.77365 1.54651C4.03826 2.35241 2.70982 3.42714 1.82176 4.78438C0.93861 6.13411 0.5 7.66057 0.5 9.34884C0.5 10.648 0.805292 11.9217 1.48788 12.8856C2.18435 13.8691 3.25562 14.5 4.68505 14.5C5.96711 14.5 7.1237 13.8508 7.78129 12.7718C8.56109 11.4923 8.47386 9.51742 7.22037 8.36454L7.22037 8.36454C6.19119 7.41799 4.59055 7.10008 3.24397 7.73301ZM11.4645 7.73356C11.4258 7.00684 11.6724 6.08938 12.4432 5.1409C13.1181 4.31026 13.918 3.70735 14.8453 3.32222L15.3492 3.11295L15.0969 2.62923L14.648 1.76876L14.4272 1.3454L13.9941 1.54651C12.2587 2.35241 10.9303 3.42714 10.0422 4.78438C9.15908 6.13411 8.72047 7.66058 8.72047 9.34884C8.72047 10.648 9.02576 11.9217 9.70834 12.8856C10.4048 13.8691 11.4761 14.5 12.9055 14.5C14.8729 14.5 16.5 13.0244 16.5 10.9535C16.5 9.85928 16.101 8.96078 15.4295 8.33861C14.7633 7.72143 13.8627 7.40697 12.9055 7.40697C12.3975 7.40697 11.916 7.51787 11.4645 7.73356Z' stroke='#333333'>\
                      </path>\
                    </svg>"

var brandSvgIcon = '<svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">\
                      <path d="M16 19H2V5H11V3H2C0.9 3 0 3.9 0 5V19C0 20.1 0.9 21 2 21H16C17.1 21 18 20.1 18 19V10H16V19ZM8.21 15.83L6.25 13.47L3.5 17H14.5L10.96 12.29L8.21 15.83ZM18 3V0H16V3H13C13.01 3.01 13 5 13 5H16V7.99C16.01 8 18 7.99 18 7.99V5H21V3H18Z" fill="#B0B0B0"></path>\
                    </svg>'

var highlightSvgIcon = '<svg width="16" height="15" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">\
                          <path d="M15 16H1C0.4 16 0 15.6 0 15V1C0 0.4 0.4 0 1 0H15C15.6 0 16 0.4 16 1V15C16 15.6 15.6 16 15 16ZM1 15H15V1H1V15Z" fill="#333333"></path>\
                          <path d="M8.69107 12V5.5H11V4H5V5.5H7.31429V12H8.69107Z" fill="#333333"></path>\
                        </svg>';

var hrSvgIcon = '<svg class="svg-icon-addon-hr" width="25" height="15">\
                <g fill-rule="evenodd">\
                  <path d="M8.45 12H5.3c-.247 0-.45.224-.45.5 0 .274.203.5.45.5h5.4c.247 0 .45-.226.45-.5 0-.276-.203-.5-.45-.5H8.45z"></path>\
                  <path d="M17.45 12H14.3c-.247 0-.45.224-.45.5 0 .274.203.5.45.5h5.4c.248 0 .45-.226.45-.5 0-.276-.202-.5-.45-.5h-2.25z"></path>\
                </g>\
              </svg>';

var imageDescriptionSvgIcon = "<svg width='16' height='16' viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>\
                              <path d='M0.833374 1.1665H3.50004' stroke='black' stroke-miterlimit='10' stroke-linecap='square'/>\
                              <path d='M0.833374 5.83301H3.50004' stroke='black' stroke-miterlimit='10' stroke-linecap='square'/>\
                              <path d='M0.833374 10.5H3.50004' stroke='black' stroke-miterlimit='10' stroke-linecap='square'/>\
                              <path d='M0.833374 15.1665H3.50004' stroke='black' stroke-miterlimit='10' stroke-linecap='square'/>\
                              <path d='M2.16671 3.5H0.833374' stroke='black' stroke-miterlimit='10' stroke-linecap='square'/>\
                              <path d='M2.16671 8.1665H0.833374' stroke='black' stroke-miterlimit='10' stroke-linecap='square'/>\
                              <path d='M2.16671 12.833H0.833374' stroke='black' stroke-miterlimit='10' stroke-linecap='square'/>\
                              <path d='M15.5 1.1665H7.5V15.1665H15.5V1.1665Z' stroke='black' stroke-miterlimit='10' stroke-linecap='square'/>\
                            </svg>";

var backSvgIcon = "<svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>\
                    <path d='M19 12H5' stroke='black' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/>\
                    <path d='M12 19L5 12L12 5' stroke='black' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/>\
                  </svg> Back";


var alignBlockSvgIcon = "<svg width='32' height='16' viewBox='0 0 32 16'>\
                  <path d='M28.909 7.08544H32V8.91403H28.909V12.5712L23.9999 7.99973L28.909 3.42828V7.08544Z'></path>\
                  <path d='M3.09098 8.91437H0V7.08579H3.09098V3.42863L8.00006 8.00008L3.09098 12.5715V8.91437Z'></path>\
                  <path d='M10 0H22V16H10V0Z'></path>\
                </svg>";

var imageFitSvgIcon = "<span class='lh-button-align-fit'><svg width='24' height='16' viewBox='0 0 24 16'>\
                <path d='M6 0H18V16H6V0Z'></path>\
                <path d='M0 0H2V16H0V0Z'></path>\
                <path d='M24 16H22V0H24V16Z'></path>\
                </svg>\
              </span>";

var imageFullSvgIcon = "<span class='lh-button-align-full'><svg width='32' height='16' viewBox='0 0 32 16'>\
                <path d='M27.091 7.08551H24V8.91409H27.091V12.5712L32.0001 7.9998L27.091 3.42834V7.08551Z'></path>\
                <path d='M4.90902 7.08551H8V8.91409H4.90902V12.5712L-6.10352e-05 7.9998L4.90902 3.42834V7.08551Z'></path>\
                <path d='M10 0H22V16H10V0Z'></path>\
                </svg>\
              </span>";

var imageWideSvgIcon = "<span class='lh-button-align-block'>\
                <svg width='32' height='16' viewBox='0 0 32 16'>\
                <path d='M28.909 7.08544H32V8.91403H28.909V12.5712L23.9999 7.99973L28.909 3.42828V7.08544Z'></path>\
                <path d='M3.09098 8.91437H0V7.08579H3.09098V3.42863L8.00006 8.00008L3.09098 12.5715V8.91437Z'></path>\
                <path d='M10 0H22V16H10V0Z'></path>\
                </svg>\
              </span>";

var imageSvgIcon = "<svg width='16' height='14' viewBox='0 0 16 14' fill='none' xmlns='http://www.w3.org/2000/svg'>\
                      <path d='M9 11.5C10.933 11.5 12.5 9.933 12.5 8C12.5 6.067 10.933 4.5 9 4.5C7.067 4.5 5.5 6.067 5.5 8C5.5 9.933 7.067 11.5 9 11.5Z' stroke='#333333' stroke-miterlimit='10' stroke-linecap='round' stroke-linejoin='round'></path>\
                      <path d='M2.5 4.5H3.5' stroke='#333333' stroke-miterlimit='10' stroke-linecap='round' stroke-linejoin='round'></path>\
                      <path d='M14.5 2.5H12.5L11.5 0.5H6.5L5.5 2.5H1.5C0.948 2.5 0.5 2.948 0.5 3.5V12.5C0.5 13.052 0.948 13.5 1.5 13.5H14.5C15.052 13.5 15.5 13.052 15.5 12.5V3.5C15.5 2.948 15.052 2.5 14.5 2.5Z' stroke='#333333' stroke-miterlimit='10' stroke-linecap='round' stroke-linejoin='round'></path>\
                    </svg>";
