// All icons (ends with ...SvgIcon) are defined in ./components/icons.js

/* =========CHANGE CONFIGURATION TO MATCH SERVER for image upload ======== */
const linkhay_url = 'https://linkhay.com' // change this to domain
const linkhay_post_url = 'https://post.linkhay.com';
const postKey = '1'; // ==> change to generated key
/* ==========END CHANGE ======================== */

var editor = new MediumEditor('.editable', {
  toolbar: {
      buttons: [
          'bold', 'italic', 'anchor', 'justifyLeft', 'justifyCenter', 'justifyRight',
          'h2','h3', 'unorderedlist', 'quote','removeFormat',
      ],
  },
  buttonLabels: 'fontawesome',
  imageDragging: false, /** disable image dragging **/
  targetBlank: true, /** enables/disables target="_blank" for anchor tags. Default: false **/
  autoLink: true,
  anchor: {
      customClassOption: null,
      customClassOptionText: 'Button',
      linkValidation: false,
      placeholderText: 'Paste or type a link',
      targetCheckboxText: 'Open in new window'
  },
  paste: {
      forcePlainText: true,
      cleanPastedHTML: false,
      cleanAttrs: ['class', 'style', 'dir'],
      cleanTags: []
  },
  spellcheck: false
});

$(function() {
  $('.editable').mediumInsert({
    editor: editor,
    addons: {
      images: {
        label: imageSvgIcon,
        fileUploadOptions: {
            // url: linkhay_url + `/user/media/upload?is_editor=true&name=${postKey}`, //Change name to match blog_post_key at line 4
            // url: 'https://www.googleapis.com/upload/drive/v3/files?uploadType=media',
            url: 'http://react-backend.test/api/upload',
            type: 'post',
            acceptFileTypes: /(.|\/)(gif|jpe?g|png)$/i
        },
        uploadCompleted: function($el, data) {
            if (!data.result || typeof data.result.files[0].url != 'string') {
                alert('Can not get image file');
                return;
            }
            $el.find("img").attr({'data-source':'lh-user-media', 'data-lh-media-id': data.result.files[0].media_id, 'data-w': data.result.files[0].width, 'data-h': data.result.files[0].height});
            //self._insertImageCollection(data.result.files[0]);
        },

        deleteScript: null,
        captionPlaceholder: 'Nhập tiêu đề ảnh',
        styles: {
          wide: {
            label: imageWideSvgIcon,
          },
          left: false,
          right: false,
          grid: false,
          fit: {
            label: imageFitSvgIcon,
          },
          full: {
            label: imageFullSvgIcon,
          },
        },
        actions: {
          remove: {
            label: deleteSvgIcon,
          }
        },
        sorting: () => { } // => disable image reposition, to enable image reposition, remove this line
      },
      embeds: {
        label:"<svg width='13' height='14' viewBox='0 0 13 14' fill='none' xmlns='http://www.w3.org/2000/svg'>\
        <path d='M1.1875 0.4375L11.6875 7L1.1875 13.5625V0.4375Z' stroke='#333333' stroke-miterlimit='10' stroke-linecap='round' stroke-linejoin='round'></path>\
        </svg>",
        styles: {
          wide: false,
          left: false,
          right: false,
          grid: false,
          fit: {
            label: imageFitSvgIcon
          },
          full: {
            label: imageFullSvgIcon
          },
        },
        actions: {
          'remove': {
            label: deleteSvgIcon
          }
        }
      },
      // customQuote: {
      //   fileUploadOptions: {
      //     // url: linkhay_url + `/user/media/upload?is_editor=true&name=${postKey}`, //Change name to match blog_post_key at line 4
      //     // url: 'https://www.googleapis.com/upload/drive/v3/files?uploadType=media',
      //     url: 'http://react-backend.test/api/upload',
      //     type: 'post',
      //     acceptFileTypes: /(.|\/)(gif|jpe?g|png)$/i
      //   },
      // },
      highLight: {
        actions: {
          'remove': {
            label: deleteSvgIcon
          }
        }
      },
      imageDescription: true,
      album: true,
      hr: true,
      divider: true,
      highlighter: true,
      quoter: true,
    },
    position: {
        left: function() {
            // console.log((window.innerWidth - 760) / 2 - 45);
            // return (window.innerWidth - 760) / 2 - 45;
        }
    }
  });
})

// this._submitForm = function (editor) {
//     $(".js-btn-publish").on('click', function () {
//         var content = editor.serialize()['linkhay-editor'].value,
//             media_collection = [],
//             _content = $('<div />').html(content);

//         $("img[data-source='lh-user-media']", _content).each(function(){
//             media_collection.push($(this).attr('data-lh-media-id'));
//         });

//         if (_form.attr('disabled') === 'disabled') {
//             return;
//         }
//         publish_flag = 1;

//         var btn = $(this),
//             blog_link_id = $("input[name='blog_link_id']").val(),
//             data = {
//                 id: $("#blog_id").val(),
//                 blog_title: $(".js-input-blog-title").osc_nodeTextEditor('getcontent'),
//                 blog_type: $("input[name='blog_type']").val(),
//                 cover_photo: $("input[name='cover_photo']").val(),
//                 cover_description: $(".js-cover-description").osc_nodeTextEditor('getcontent'),
//                 source_photo: $("input[name='source_photo']").val(),
//                 source_link: $("input[name='source_link']").val(),
//                 media_collection: media_collection,
//                 blog_content: content,
//                 publish_flag: publish_flag ? 1 : 0,
//                 blog_link_id: blog_link_id
//             };

//         if (!data.blog_title) {
//             alert("Vui lĂ²ng nháº­p tiĂªu Ä‘á» blog");
//             return;
//         }

//         if (!data.blog_content) {
//             alert("Vui lĂ²ng nháº­p ná»™i dung blog");
//             return;
//         }

//         _form.attr('disabled', 'disabled');
//         $.ajax({
//             url: linkhay_url + '/blog/post/save',
//             type: 'post',
//             data: data,
//             crossDomain: true,
//             xhrFields: {withCredentials: true},
//             headers: {
//                 'X-Requested-With': 'XMLHttpRequest',
//                 'X-OSC-Cross-Request': 'OK'
//             },
//             beforeSend: function () {
//                 self._disabledForm(_form, btn);
//             },
//             success: function (response) {
//                 if (response.result === 'OK') {
//                     if (blog_link_id > 0) {
//                         alert("Blog đã được cập nhật");
//                         self._activeForm(_form, btn);
//                     } else {
//                         window.location.href = response.data;
//                     }

//                 } else {
//                     self._activeForm(_form, btn);
//                     alert(response.message);
//                 }
//             },
//             error: function () {
//                 self._activeForm(_form, btn);
//                 alert("Error");
//             }
//         });

//     });
// };
// self._submitForm(editor);

