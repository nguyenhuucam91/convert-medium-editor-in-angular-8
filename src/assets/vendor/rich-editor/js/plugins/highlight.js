const removeHighlightButton = "<div class='medium-insert-highlight-toolbar2 medium-editor-toolbar medium-editor-toolbar-active'>\
                        <ul class='medium-editor-toolbar-actions clearfix'>\
                        <li>\
                            <button class='medium-editor-action' data-action='remove'>"+deleteSvgIcon+"</button>\
                        </li>\
                      </ul></div>";
/** insert hr **/
;(function($, window, document, undefined) {
    'use strict';

    /** Default values */
    var pluginName = 'mediumInsert',
        addonName = 'HighLight', // first char is uppercase
        defaults = {
            label: highlightSvgIcon,
        },
        options = {
          actions: {
            remove: {
              label: deleteSvgIcon,
              clicked: function () {
                  var e = $.Event("keydown");
                  e.which = 0, $(document).trigger(e)
              }
            }
          }
        }

    /**
     * Custom Addon object
     *
     * Sets options, variables and calls init() function
     *
     * @constructor
     * @param {DOM} el - DOM element to init the plugin on
     * @param {object} options - Options to override defaults
     * @return {void}
     */

    function CustomAddon(el, options) {
        this.el = el;
        this.$el = $(el);
        this.templates = window.MediumInsert.Templates;
        this.core = this.$el.data('plugin_'+ pluginName);

        this.options = $.extend(true, {}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    /**
     * Initialization
     *
     * @return {void}
     */

    CustomAddon.prototype.init = function () {
        this.events();
    };

    CustomAddon.prototype.selectHighlight = function (e) {
      var t, i = this,
      t = $(e.target);
      t.addClass('lh-template-highlight-selected');
      setTimeout(function () {
        i.addToolbar()
    }, 50);
    };

    /**
     * Add toolbar
     */
    CustomAddon.prototype.addToolbar = function () {
      var o = this.core.getEditor().options.elementsContainer || "body",
      $toolbar2 = $('.medium-insert-highlight-toolbar2');
      $(o).append(removeHighlightButton);
      this.repositionToolbars();
      $toolbar2.fadeIn();
    };

    /**
     * Perform remove highlight
     */
    CustomAddon.prototype.removeHighlight = function (e) {
       //Kiểm tra nút bấm, nếu nút bấm = 8 (Backspace), 46 (Delete) hoặc = 0 (Click chuột)
      if (e.which === 8 || e.which === 46 || e.which === 0) {
        //lấy highlighter selected hiện tại
        var $currentHighlighter = this.$el.find(".lh-template-highlight-selected");
        //nếu tồn tại selected highlighter
        if ($currentHighlighter.length) {
          //Kiểm tra nếu ấn Backspace mà content không còn kí tự nào (<br>) <== xoá cạn dòng hoặc click chuột
          if ($currentHighlighter[0].innerHTML === "<br>" || e.which === 0) {
            //Tạo thêm 1 thẻ <p></p> để nhập nội dung, chèn vào trước highlighter được xoá
            var caret = $("<p><br/></p>");
            $currentHighlighter.before(caret);
            //Xoá highlighter và xoá button "Delete"
            $currentHighlighter.remove();
            $(".medium-insert-highlight-toolbar2").remove();
            this.core.hideAddons();
            //trỏ caret đến caret vừa tạo, caret ở đây chính là (I) nhấp nháy
            this.core.moveCaret(caret);
          }
        }
      }
    }

    /**
     * Trigger action to remove highlight
     */
    CustomAddon.prototype.toolbar2Action = function(e) {
      var t = $(e.target).is("button") ? $(e.target) : $(e.target).closest("button"),
      i = (options.actions[t.data("action")] || {}).clicked;
      i && i(this.$el.find(".lh-template-highlight-selected")), this.core.triggerInput(), this.repositionToolbars()
    }

    /**
     * Reposition toolbar 2
     */

    CustomAddon.prototype.repositionToolbars = function () {
      var $toolbar2 = $('.medium-insert-highlight-toolbar2'),
          $highlight = this.$el.find('.lh-template-highlight-selected'),
          elementsContainer = this.core.getEditor().options.elementsContainer,
          elementsContainerAbsolute = ['absolute', 'fixed'].indexOf(window.getComputedStyle(elementsContainer).getPropertyValue('position')) > -1,
          elementsContainerBoundary = elementsContainerAbsolute ? elementsContainer.getBoundingClientRect() : null,
          containerWidth = $(window).width(),
          position = {};

      if ($toolbar2.length) {
        position.top = $highlight.offset().top + 5;
        position.left = $highlight.offset().left + $highlight.width(); // 4px - distance from a border
        if (elementsContainerAbsolute) {
            position.top += elementsContainer.scrollTop - elementsContainerBoundary.top;
            position.left -= elementsContainerBoundary.left;
            containerWidth = $(elementsContainer).width();
        }

        if (position.left + $toolbar2.width() > containerWidth) {
          position.left = containerWidth - $toolbar2.width();
        }

        $toolbar2.css(position);
      }
    }

    /**
    * Autoreposition toolbar
    */
   CustomAddon.prototype.autoRepositionToolbars = function () {
    setTimeout(function () {
        this.repositionToolbars(), this.repositionToolbars()
    }.bind(this), 0)
},

     /**
      * UnselectHighlight
      */
    CustomAddon.prototype.unselectHighlight = function (e) {
      var t = $(e.target).hasClass("lh-template-highlight") ? $(e.target) : $(e.target).closest(".lh-template-highlight"),
          i = this.$el.find(".lh-template-highlight-selected"),
          b = $(e.target);
      if ($(b).hasClass('medium-editor-action') || $(b).parents('button:first').hasClass('medium-editor-action')) {
          return;
      }
      if (t.hasClass("lh-template-highlight-selected")) return i.not(t).removeClass("lh-template-highlight-selected"), $(".medium-insert-highlight-toolbar2").remove();
      i.removeClass("lh-template-highlight-selected"), $(".medium-insert-highlight-toolbar2").remove();
    }
    /**
     * Event listeners
     *
     * @return {void}
     */

    CustomAddon.prototype.events = function () {
      $(document).on("click", $.proxy(this, "unselectHighlight"))
      .on("keydown", $.proxy(this, "removeHighlight"))
      .on("click", ".medium-insert-highlight-toolbar2 .medium-editor-action", $.proxy(this, "toolbar2Action")),
      this.$el.on("click", ".lh-template-highlight", $.proxy(this, "selectHighlight")),
      $(window).on('resize', $.proxy(this, 'autoRepositionToolbars'));
    };

    /**
     * Get the Core object
     *
     * @return {object} Core object
     */
    CustomAddon.prototype.getCore = function () {
        return this.core;
    };

    /**
     * Add custom content
     *
     * This function is called when user click on the addon's icon
     *
     * @return {void}
     */

    CustomAddon.prototype.add = function () {
        var html = '<p class="lh-template-highlight"><br/></p><p><br/></p>',
            options = {
                forcePlainText: false,
                cleanPastedHTML: false,
                cleanAttrs: []
            };
        editor.pasteHTML(html, options);
        /**
         * hide button insert
         */
        try {
            this.$el.data('plugin_mediumInsert').hideButtons()
        } catch (e) {}
    };


    /** Addon initialization */

    $.fn[pluginName + addonName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName + addonName)) {
                $.data(this, 'plugin_' + pluginName + addonName, new CustomAddon(this, options));
            }
        });
    };
})(jQuery, window, document);
