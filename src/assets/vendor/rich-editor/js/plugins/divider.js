;(function ($, window, document, undefined) {

  'use strict';

  /** Default values */
  var pluginName = 'mediumInsert',
      addonName = 'Divider', // first char is uppercase
      defaults = {
          label: '<span> * </span>' // icon được hiển thị ra ngoài thế nào
      },
      options = {}

  /**
   * Custom Addon object
   *
   * Sets options, variables and calls init() function
   *
   * @constructor
   * @param {DOM} el - DOM element to init the plugin on
   * @param {object} options - Options to override defaults
   * @return {void}
   */

  function Divider (el, options) {
      this.el = el;
      this.$el = $(el);
      this.templates = window.MediumInsert.Templates;
      this.core = this.$el.data('plugin_'+ pluginName);

      this.options = $.extend(true, {}, defaults, options);

      this._defaults = defaults;
      this._name = pluginName;

      this.init();
  }

  /**
   * Initialization
   *
   * @return {void}
   */

  Divider.prototype.init = function () {
      this.events();
  };

  /**
   * Event listeners
   *
   * @return {void}
   */

  Divider.prototype.events = function () {

  };

  /**
   * Get the Core object
   *
   * @return {object} Core object
   */
  Divider.prototype.getCore = function () {
      return this.core;
  };

  /**
   * Add custom content
   *
   * This function is called when user click on the addon's icon
   *
   * @return {void}
   */

  Divider.prototype.add = function () {
      var options = {
              forcePlainText: true,
              cleanPastedHTML: true,
              cleanAttrs: [] //allow to have class(es) in HTML tags, if deleted, no class will be displayed on line 84
          };

      var parser = new UAParser();
      var browserName = parser.getBrowser().name;

      var hr = $('<hr class="section-divider"/>'),
      // detect if current browser is firefox, then dont add <br/> tag,
      // a 'hack' to allow firefox to move to next line without <br/>
      p = browserName === "Firefox" ? $('<p/>').addClass('medium-insert-active')
                                    : $('<p/>').addClass('medium-insert-active').html("<br/>");

      editor.pasteHTML(hr[0].outerHTML + p[0].outerHTML, options); // tiến hành dán block HTML vào phần được ấn

      /**
       * hide button insert
       */
      try {
          this.$el.data('plugin_mediumInsert').hideButtons()
      } catch (e) {}
  };


  /** Addon initialization */

  $.fn[pluginName + addonName] = function (options) {
      return this.each(function () {
          if (!$.data(this, 'plugin_' + pluginName + addonName)) {
              $.data(this, 'plugin_' + pluginName + addonName, new Divider(this, options));
          }
      });
  };

})(jQuery, window, document);
