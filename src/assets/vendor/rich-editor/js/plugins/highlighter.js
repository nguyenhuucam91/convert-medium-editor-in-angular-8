const removeHighlighterButton = "<div class='medium-insert-highlighter-toolbar2 medium-editor-toolbar medium-editor-toolbar-active'>\
                        <ul class='medium-editor-toolbar-actions clearfix'>\
                        <li>\
                            <button class='medium-editor-action' data-action='remove'>"+
                            deleteSvgIcon
                        +"</button>\
                        </li>\
                      </ul></div>";


;(function ($, window, document, undefined) {

  'use strict';

  /** Default values */
  var pluginName = 'mediumInsert',
      addonName = 'Highlighter', // first char is uppercase
      defaults = {
          label: '<span class="fa fa-plus-circle"></span>'
      },
      options = {
        actions: {
          remove: {
            label: deleteSvgIcon,
            clicked: function () {
                var e = $.Event("keydown");
                e.which = 0, $(document).trigger(e)
            }
          }
        }
      }

  /**
   * Custom Addon object
   *
   * Sets options, variables and calls init() function
   *
   * @constructor
   * @param {DOM} el - DOM element to init the plugin on
   * @param {object} options - Options to override defaults
   * @return {void}
   */

  function Highlighter (el, options) {
      this.el = el;
      this.$el = $(el);
      this.templates = window.MediumInsert.Templates;
      this.core = this.$el.data('plugin_'+ pluginName);

      this.options = $.extend(true, {}, defaults, options);

      this._defaults = defaults;
      this._name = pluginName;

      this.init();
  }

  /**
   * Initialization
   *
   * @return {void}
   */

  Highlighter.prototype.init = function () {
      this.events();
  };

  /**
   * Event listeners
   *
   * @return {void}
   */

  Highlighter.prototype.events = function () {
    $(document).on("click", $.proxy(this, "unselectHighlighter"))
    .on('keydown', $.proxy(this, 'removeHighlighter'))
    //kích hoạt nút xoá
    .on("click", ".medium-insert-highlighter-toolbar2 .medium-editor-action", $.proxy(this, "toolbar2Action")),
    this.$el.on("click", ".lh-template-highlighter", $.proxy(this, "selectHighlighter")); // this.$el points to linkhay-editor
    $(window).on('resize', $.proxy(this, 'repositionToolbars'));
  };

  /**
   * Remove highlighter out of DOM
   *
   * @return {void}
   */
  Highlighter.prototype.removeHighlighter = function (e) {
    //Kiểm tra nút bấm, nếu nút bấm = 8 (Backspace), 46 (Delete) hoặc = -1 (Click chuột)
    if (e.which === 8 || e.which === 46 || e.which === 0) {
      //lấy highlighter selected hiện tại
      var $currentHighlighter = this.$el.find(".lh-template-highlighter-selected");
      //nếu tồn tại selected highlighter
      if ($currentHighlighter.length) {
        //Kiểm tra nếu ấn Backspace mà content không còn kí tự nào (<br>) <== xoá cạn dòng hoặc click chuột
        if ($currentHighlighter[0].innerHTML === "<br>" || e.which === 0) {
          //Tạo thêm 1 thẻ <p></p> để nhập nội dung, chèn vào trước highlighter được xoá
          var caret = $("<p><br/></p>");
          $currentHighlighter.before(caret);
          //Xoá highlighter và xoá button "Delete"
          $currentHighlighter.remove();
          $(".medium-insert-highlighter-toolbar2").remove();
          this.core.hideAddons();
          //trỏ caret đến caret vừa tạo, caret ở đây chính là (I) nhấp nháy
          this.core.moveCaret(caret);
        }
      }
    }

  }

  /**
   * Activate "Delete" function when clicking on toolbar2 (defined at top)
   *
   * @return {void}
   */
  Highlighter.prototype.toolbar2Action = function (e) {
    //kiểm tra xem nút ấn có phải là button hay không, tương tự như với dòng 98
    var $button = $(e.target).is("button") ? $(e.target) : $(e.target).closest("button");
    var i = options.actions[$button.data('action')].clicked;

    //trigger hàm ở trên đầu dòng 27
    i && i(this.$el.find(".lh-template-highlighter-selected"));
    //ẩn button và trigger input
    this.core.hideButtons();
    this.core.triggerInput();
  }

  /**
   * When highlighter is not focused, trigger this function
   *
   * @returns {void}
   */
  Highlighter.prototype.unselectHighlighter = function (e) {
    //kiểm tra current element có phải là highlighter hay không, nếu là highlighter thì set = target, nếu không thì lấy element cha gần nhất
    //ứng với highlighter đó, do khi click vào highlighter có thể bị click vào element bên trong highlighter nên cần lấy gần nhất
    var $currentHighlighter = $(e.target).hasClass("lh-template-highlighter") ? $(e.target) : $(e.target).closest(".lh-template-highlighter"),
    //get all highlighter selected element when we click each highlighter.
        $highlighters = this.$el.find('.lh-template-highlighter-selected');

    //if selected element is another highlighter
    if ($currentHighlighter.hasClass('lh-template-highlighter-selected')) {
      // then remove only other classes except current highlighter which contain selected
      // so that only 1 selected is available
      $highlighters.not($currentHighlighter).removeClass('lh-template-highlighter-selected');
    }

    //if selected element is not highlighter (maybe image, text, etc.)
    else {
      //remove all selected highlighter
      $highlighters.removeClass('lh-template-highlighter-selected');
    }

    //remove toolbar 2 if it's not selected; here, remove "Trash icon"
    $('.medium-insert-highlighter-toolbar2').remove();
  }

  /**
   * When highlighter is selected, trigger this action
   *
   * @returns {void}
   */
  Highlighter.prototype.selectHighlighter = function (e) {
    var that = this, $currentHighlighter;
    if (this.core.options.enabled) {
      $currentHighlighter = $(e.target);
      $currentHighlighter.addClass('lh-template-highlighter-selected'); //add class selected for highlighter which is focused

      //show toolbar after 0.05s if current highlighter is selected, triggered via e.target
      setTimeout(function () {
        that.addToolbar();
      }, 50);
    }
  }

  /**
   * Add toolbar for highlighter
   *
   * @return {void}
   */
  Highlighter.prototype.addToolbar = function () {
    var mediumEditor = this.core.getEditor(), //lấy editor hiện tại
    toolbarContainer = mediumEditor.options.elementsContainer || 'body',  // lấy toolbar container
    $toolbar2 = $('.medium-insert-highlighter-toolbar2'); //khối toolbar2 được định nghĩa ở trên
    $(toolbarContainer).append(removeHighlighterButton); //chèn khối toolbar2 được định nghĩa ở trên vào element
    this.repositionToolbars()
    $toolbar2.fadeIn();//hiển thị toolbar2 ra bên ngoài, có cũng được không có cũng không sao, chèn vào nhằm thêm hiệu ứng fadeIn cho đẹp
  }

  /**
   * Reposition toolbars for highlighter when select, window resize
   *
   * @return {void}
   */
  Highlighter.prototype.repositionToolbars = function () {
    var $toolbar2 = $('.medium-insert-highlighter-toolbar2'), //chỉnh sửa class để lấy class của toolbar
    $selectedHighlighter = this.$el.find('.lh-template-highlighter-selected'), //chỉnh sửa class để lấy class của selected plugin
    elementsContainer = this.core.getEditor().options.elementsContainer,
    elementsContainerAbsolute = ['absolute', 'fixed'].indexOf(window.getComputedStyle(elementsContainer).getPropertyValue('position')) > -1,
    elementsContainerBoundary = elementsContainerAbsolute ? elementsContainer.getBoundingClientRect() : null,
    containerWidth = $(window).width(),
    position = {};

    if ($toolbar2.length) {
        // thay đổi số này để chỉnh sửa nút theo trục dọc
        position.top = $selectedHighlighter.offset().top + 2;
        // thay đổi số này để chỉnh sửa nút theo trục ngang
        position.left = $selectedHighlighter.offset().left + $selectedHighlighter.width() - $toolbar2.width() - 8;

        if (elementsContainerAbsolute) {
            position.top += elementsContainer.scrollTop - elementsContainerBoundary.top;
            position.left -= elementsContainerBoundary.left;
            containerWidth = $(elementsContainer).width();
        }

        if (position.left + $toolbar2.width() > containerWidth) {
            position.left = containerWidth - $toolbar2.width();
        }

        $toolbar2.css(position);
    }
  }
  /**
   * Get the Core object
   *
   * @return {object} Core object
   */
  Highlighter.prototype.getCore = function () {
      return this.core;
  };

  /**
   * Add custom content
   *
   * This function is called when user click on the addon's icon
   *
   * @return {void}
   */

  Highlighter.prototype.add = function () {
    var html = '<p class="lh-template-highlighter"><br/></p><p><br/></p>',
    options = {
        forcePlainText: false,
        cleanPastedHTML: false,
        cleanAttrs: []
    };
    editor.pasteHTML(html, options);

    /**
     * hide button insert
     */
    try {
        this.$el.data('plugin_mediumInsert').hideButtons()
    } catch (e) {}
  };


  /** Addon initialization */

  $.fn[pluginName + addonName] = function (options) {
      return this.each(function () {
          if (!$.data(this, 'plugin_' + pluginName + addonName)) {
              $.data(this, 'plugin_' + pluginName + addonName, new Highlighter(this, options));
          }
      });
  };

})(jQuery, window, document);
