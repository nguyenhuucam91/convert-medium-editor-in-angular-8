var toolbar1 = "<div class='medium-insert-album-toolbar medium-editor-toolbar medium-toolbar-arrow-under medium-editor-toolbar-active' style='top: 712px; left: 311.5px;'>\
                  <ul class='medium-editor-toolbar-actions clearfix'>\
                    <li>\
                      <button class='medium-editor-action medium-editor-button-active' data-action='block'><span class='lh-button-align-block'>"+imageWideSvgIcon+"</span></button>\
                    </li>\
                    <li>\
                      <button class='medium-editor-action' data-action='fit'><span class='lh-button-align-fit'>"+imageFitSvgIcon+"</span></button>\
                    </li>\
                    <li>\
                      <button class='medium-editor-action' data-action='edit'>Edit</button>\
                    </li>\
                  </ul>\
                </div>";

var toolbar2 = "<div class='medium-insert-album-toolbar2 medium-editor-toolbar medium-editor-toolbar-active' style='top: 769px; left: 652.5px;'>\
                  <ul class='medium-editor-toolbar-actions clearfix'>\
                    <li>\
                        <button class='medium-editor-action' data-action='remove'>"+deleteSvgIcon+"</button>\
                    </li>\
                  </ul>\
                </div>";

/** Change line 368, 375, 513, 515, 583, 587, 703, 710, 1078, 1085 to match server side */
var editorContainer = $('#linkhay-editor');
/** insert Album **/
;(function($, window, document, undefined) {
  'use strict';

  /** Default values */
  var instance = null,
      pluginName = 'mediumInsert',
      addonName = 'Album', // first char is uppercase
      activeStyle = 'style-1',
      _loading_icon = '<svg xml:space="preserve" style="enable-background:new 0 0 50 50;" viewBox="0 0 24 30" height="20px" width="21px" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" id="Layer_1" version="1.1"><rect opacity="0.2" fill="#000" height="8" width="3" y="10" x="0"><animate repeatCount="indefinite" dur="0.6s" begin="0s" values="0.2; 1; .2" attributeType="XML" attributeName="opacity"/><animate repeatCount="indefinite" dur="0.6s" begin="0s" values="10; 20; 10" attributeType="XML" attributeName="height"/><animate repeatCount="indefinite" dur="0.6s" begin="0s" values="10; 5; 10" attributeType="XML" attributeName="y"/></rect><rect opacity="0.2" fill="#000" height="8" width="3" y="10" x="8">      <animate repeatCount="indefinite" dur="0.6s" begin="0.15s" values="0.2; 1; .2" attributeType="XML" attributeName="opacity"/><animate repeatCount="indefinite" dur="0.6s" begin="0.15s" values="10; 20; 10" attributeType="XML" attributeName="height"/><animate repeatCount="indefinite" dur="0.6s" begin="0.15s" values="10; 5; 10" attributeType="XML" attributeName="y"/></rect><rect opacity="0.2" fill="#000" height="8" width="3" y="10" x="16"><animate repeatCount="indefinite" dur="0.6s" begin="0.3s" values="0.2; 1; .2" attributeType="XML" attributeName="opacity"/><animate repeatCount="indefinite" dur="0.6s" begin="0.3s" values="10; 20; 10" attributeType="XML" attributeName="height"/><animate repeatCount="indefinite" dur="0.6s" begin="0.3s" values="10; 5; 10" attributeType="XML" attributeName="y"/></rect></svg>',
      defaults = {
          label: '<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
              '<path d="M3.5 0.5H0.5V15.5H3.5V0.5Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>\n' +
              '<path d="M15.5 0.5H6.5V3.5H15.5V0.5Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>\n' +
              '<path d="M9.5 6.5H6.5V9.5H9.5V6.5Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>\n' +
              '<path d="M15.5 6.5H12.5V15.5H15.5V6.5Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>\n' +
              '<path d="M9.5 12.5H6.5V15.5H9.5V12.5Z" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"/>\n' +
              '</svg>\n',
          selectItem: '<div class="lh-editorSidebar-checkbox">\n' +
              '            <div class="round">\n' +
              '                <input type="checkbox" id="" class="editorSelectTemplateCheckbox" value="">\n' +
              '                <label for=""></label>\n' +
              '            </div>\n' +
              '        </div>\n' +
              '        <div class="lh-editorSidebar-content" data-template="">\n' +
              '        </div>'
      },
      albumConfig = {
          icons: {
            link: '<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
                '<path d="M11.0002 14.1926C10.2471 13.9628 9.56236 13.551 9.0064 12.9936V12.9936C8.55947 12.5467 8.20494 12.0162 7.96306 11.4323C7.72118 10.8484 7.59668 10.2225 7.59668 9.59048C7.59668 8.95846 7.72118 8.33262 7.96306 7.74871C8.20494 7.1648 8.55947 6.63425 9.0064 6.18736L12.4095 2.78423C12.8564 2.3373 13.387 1.98277 13.9709 1.74089C14.5548 1.49901 15.1806 1.37451 15.8126 1.37451C16.4447 1.37451 17.0705 1.49901 17.6544 1.74089C18.2383 1.98277 18.7689 2.3373 19.2158 2.78423V2.78423C19.6627 3.23112 20.0172 3.76167 20.2591 4.34558C20.501 4.92949 20.6255 5.55533 20.6255 6.18736C20.6255 6.81939 20.501 7.44523 20.2591 8.02914C20.0172 8.61305 19.6627 9.1436 19.2158 9.59048L17.1189 11.6874" stroke="#606770" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="square"/>\n' +
                '<path d="M11.0001 7.80713C11.7531 8.03689 12.4379 8.44871 12.9939 9.00613V9.00613C13.4408 9.45302 13.7953 9.98357 14.0372 10.5675C14.2791 11.1514 14.4036 11.7772 14.4036 12.4093C14.4036 13.0413 14.2791 13.6671 14.0372 14.251C13.7953 14.8349 13.4408 15.3655 12.9939 15.8124L9.59073 19.2155C9.14384 19.6624 8.61329 20.017 8.02938 20.2589C7.44547 20.5007 6.81963 20.6252 6.1876 20.6252C5.55558 20.6252 4.92974 20.5007 4.34583 20.2589C3.76191 20.017 3.23137 19.6624 2.78448 19.2155V19.2155C2.33754 18.7686 1.98301 18.2381 1.74113 17.6542C1.49925 17.0702 1.37476 16.4444 1.37476 15.8124C1.37476 15.1804 1.49925 14.5545 1.74113 13.9706C1.98301 13.3867 2.33754 12.8561 2.78448 12.4093L4.88135 10.3124" stroke="#606770" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="square"/>\n' +
                '</svg>\n',
            trash: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
                  '<path d="M2.5 6.6665V17.4998C2.5 18.1629 2.76339 18.7988 3.23223 19.2676C3.70107 19.7364 4.33696 19.9998 5 19.9998H15C15.663 19.9998 16.2989 19.7364 16.7678 19.2676C17.2366 18.7988 17.5 18.1629 17.5 17.4998V6.6665H2.5ZM7.5 15.8332H5.83333V9.99984H7.5V15.8332ZM10.8333 15.8332H9.16667V9.99984H10.8333V15.8332ZM14.1667 15.8332H12.5V9.99984H14.1667V15.8332Z" fill="#C7C7C7"/>\n' +
                  '<path d="M19.1667 3.33333H14.1667V0.833333C14.1667 0.61232 14.0789 0.400358 13.9226 0.244078C13.7663 0.0877974 13.5543 0 13.3333 0L6.66667 0C6.44565 0 6.23369 0.0877974 6.07741 0.244078C5.92113 0.400358 5.83333 0.61232 5.83333 0.833333V3.33333H0.833333C0.61232 3.33333 0.400358 3.42113 0.244078 3.57741C0.0877974 3.73369 0 3.94565 0 4.16667C0 4.38768 0.0877974 4.59964 0.244078 4.75592C0.400358 4.9122 0.61232 5 0.833333 5H19.1667C19.3877 5 19.5996 4.9122 19.7559 4.75592C19.9122 4.59964 20 4.38768 20 4.16667C20 3.94565 19.9122 3.73369 19.7559 3.57741C19.5996 3.42113 19.3877 3.33333 19.1667 3.33333ZM7.5 1.66667H12.5V3.33333H7.5V1.66667Z" fill="#C7C7C7"/>\n' +
                  '</svg>\n',
            plus: '<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
                  '<path d="M7 1V13M1 7H13" stroke="#606770" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
                  '</svg>\n'
          },
          albumTemplate: {
              style_1: {
                  items: 4,
                  styleName: 'style-1'
              },
              style_2: {
                  items: 4,
                  styleName: 'style-2'
              },
              style_3: {
                  items: 5,
                  styleName: 'style-3'
              },
              style_4: {
                  items: 3,
                  styleName: 'style-4'
              },
              style_5: {
                  items: 4,
                  styleName: 'style-5'
              },
              style_6: {
                  items: 4,
                  styleName: 'style-6'
              },
              style_7: {
                  items: 2,
                  styleName: 'style-7'
              },
              style_8: {
                  items: 2,
                  styleName: 'style-8'
              },
              style_9: {
                  items: 5,
                  styleName: 'style-9'
              },
              style_10: {
                  items: 6,
                  styleName: 'style-10'
              },
              style_11: {
                  items: 6,
                  styleName: 'style-11'
              },
              style_12: {
                  items: 7,
                  styleName: 'style-12'
              },
              style_13: {
                  items: 7,
                  styleName: 'style-13'
              },
          }
      },
      options = {
          styles: {
              block: {
                  label: '<span class="lh-button-align-block">\n'+imageWideSvgIcon+'</span>'
              },
              fit: {
                  label: '<span class="lh-button-align-fit">'+imageFitSvgIcon+'</span>'
              },
              edit: {
                  label: 'Edit',
                  clicked: function() {
                      instance.edit = true;
                      var selectedElement = $('.medium-insert-album.medium-insert-album-selected'),
                          style = selectedElement.find('.lh-album').attr('data-style'),
                          albumId = selectedElement.find('.lh-album').attr('data-album'),
                          images = [];
                      selectedElement.find('img').each(function() {
                          images.push({'media_id':$(this).data('lh-media-id'), 'url': $(this).attr('src'), 'w': $(this).attr('data-w'), 'h': $(this).attr('data-h')});
                      });
                      instance.selectedElement = selectedElement;
                      instance.add(images, style, true, albumId);
                  }
              }
          },
          actions: {
              remove: {
                  label: deleteSvgIcon,
                  clicked: function() {
                      var e = $.Event("keydown");
                      e.which = 8, $(document).trigger(e)
                  }
              }
          }
      };

  /**
   * Custom Addon object
   *
   * Sets options, variables and calls init() function
   *
   * @constructor
   * @param {DOM} el - DOM element to init the plugin on
   * @param {object} options - Options to override defaults
   * @return {void}
   */

  function Album(el, options) {
      this.el = el;
      this.$el = $(el);
      this.templates = window.MediumInsert.Templates;
      this.core = this.$el.data('plugin_'+ pluginName);

      this.options = $.extend(true, {}, defaults, options);

      this._defaults = defaults;
      this._name = pluginName;

      this.edit = false;

      this.init();
  }

  /**
   * Initialization
   *
   * @return {void}
   */

  Album.prototype.init = function () {
      var e = this.$el.find(".medium-insert-album");
      e.find("figure").attr("contenteditable", !1), this.events()
  },

  Album.prototype.selectAlbum = function(e) {
      var t, i = this,
          t = $(e.target);
      if (t.hasClass('medium-insert-album') == false) {
          t = $(t).parents('.medium-insert-album');
      }

      t.addClass('medium-insert-album-selected'),
          setTimeout(function() {
              i.addToolbar()
          }, 50),
          this.$currentAlbum = t,
          this.$el.blur();
  },

  Album.prototype.unselectAlbum = function(e) {
      var t = $(e.target).hasClass("medium-insert-album") ? $(e.target) : $(e.target).closest(".medium-insert-album"),
          i = this.$el.find(".medium-insert-album-selected"),
          b = $(e.target);
      if ($(b).hasClass('medium-editor-action') || $(b).parents('button:first').hasClass('medium-editor-action')) {
          return;
      }
      if (t.hasClass("medium-insert-album-selected")) return i.not(t).removeClass("medium-insert-album-selected"), $(".medium-insert-album-toolbar, .medium-insert-album-toolbar2").remove();
      i.removeClass("medium-insert-album-selected"), $(".medium-insert-album-toolbar, .medium-insert-album-toolbar2").remove();
  },

  Album.prototype.removeAlbum = function(e) {
      var t, i;
      8 !== e.which && 46 !== e.which || (t = this.$el.find(".medium-insert-album-selected")).length && (e.preventDefault(), $(".medium-insert-album-toolbar, .medium-insert-album-toolbar2").remove(), i = $(this.templates["src/js/templates/core-empty-line.hbs"]().trim()), t.before(i), t.remove(), this.core.hideAddons(), this.core.moveCaret(i), this.core.triggerInput())
  },

   Album.prototype.addToolbar = function() {
      var e, t, i = this.$el.find(".medium-insert-album-selected").closest(".medium-insert-album"),
          s = !1,
          o = this.core.getEditor().options.elementsContainer || "body";

      $(o).append(toolbar1);
      $(o).append(toolbar2);

      e = $(".medium-insert-album-toolbar"), t = $(".medium-insert-album-toolbar2"), e.find("button").each(function() {
          i.hasClass("medium-insert-album-" + $(this).data("action")) && ($(this).addClass("medium-editor-button-active"), s = !0)
      }), !1 === s && e.find("button").first().addClass("medium-editor-button-active"), this.repositionToolbars(), e.fadeIn(), t.fadeIn()
  },

   Album.prototype.autoRepositionToolbars = function() {
      setTimeout(function() {
          this.repositionToolbars(), this.repositionToolbars()
      }.bind(this), 0)
  },

  Album.prototype.repositionToolbars = function() {
      var e = $(".medium-insert-album-toolbar"),
          t = $(".medium-insert-album-toolbar2"),
          i = this.$el.find(".medium-insert-album-selected .lh-album"),
          s = this.core.getEditor().options.elementsContainer,
          o = -1 < ["absolute", "fixed"].indexOf(window.getComputedStyle(s).getPropertyValue("position")),
          n = o ? s.getBoundingClientRect() : null,
          a = $(window).width(),
          r = {};
      if (typeof(i.offset()) == 'undefined') {
          return;
      }

      t.find('.medium-editor-action[data-action="edit"]').css({position: 'absolute', top: i.height() - 46, left: -(i.width() / 2 + 40)});
      t.length && (r.top = i.offset().top + 2, r.left = i.offset().left + i.width() - t.width() - 4, o && (r.top += s.scrollTop - n.top, r.left -= n.left, a = $(s).width()), r.left + t.width() > a && (r.left = a - t.width()), t.css(r)), e.length && (r.left = i.offset().left + i.width() / 2 - e.width() / 2, r.top = i.offset().top - e.height() - 8 - 2 - 5, o && (r.top += s.scrollTop - n.top, r.left -= n.left), r.top < 0 && (r.top = 0), e.css(r))
  },

  Album.prototype.toolbarAction = function(e) {
      var t = $(e.target).is("button") ? $(e.target) : $(e.target).closest("button"),
          i = t.closest("li"),
          s = i.closest("ul").find("li"),
          o = this.$el.find(".medium-insert-album-selected"),
          z = (options.styles[t.data("action")] || {}).clicked,
          n = this,
          _initClass = '';

      if(o.hasClass('medium-insert-album-fit')) {
          _initClass = 'medium-insert-album-fit';
      }
      if(o.hasClass('medium-insert-album-block')) {
          _initClass = 'medium-insert-album-block';
      }
      z && z(this.$el.find(".medium-insert-album-selected")),
      t.addClass("medium-editor-button-active"),
      i.siblings().find(".medium-editor-button-active").removeClass("medium-editor-button-active");
      s.find("button").each(function() {
          var f = "medium-insert-album-" + $(this).data("action");
           $(this).hasClass("medium-editor-button-active") ? (o.addClass(f), (options.styles[$(this).data("action")] || {}).added && options.styles[$(this).data("action")].added(o)) : (o.removeClass(f), (options.styles[$(this).data("action")] || {}).removed && options.styles[$(this).data("action")].removed(o));
      });
      if(t.data("action") == 'edit') {
          t.removeClass('medium-editor-button-active');
          o.addClass(_initClass);
      }
      this.core.triggerInput(), this.repositionToolbars();

  },


  Album.prototype.toolbar2Action = function(e) {
      var t = $(e.target).is("button") ? $(e.target) : $(e.target).closest("button"),
          i = (options.actions[t.data("action")] || {}).clicked;
      i && i(this.$el.find(".medium-insert-album-selected")), this.core.triggerInput(), this.repositionToolbars();
  };

  /**
   * Event listeners
   *
   * @return {void}
   */

  Album.prototype.events = function () {
      $(document).on("click", $.proxy(this, "unselectAlbum"))
          .on("keydown", $.proxy(this, "removeAlbum"))
          .on("click", ".medium-insert-album-toolbar .medium-editor-action", $.proxy(this, "toolbarAction"))
          .on("click", ".medium-insert-album-toolbar2 .medium-editor-action", $.proxy(this, "toolbar2Action")),
          this.$el.on("click", ".medium-insert-album .lh-album", $.proxy(this, "selectAlbum")),
          $(document).on("resize", $.proxy(this, "autoRepositionToolbars"));
  };

  /**
   * Get the Core object
   *
   * @return {object} Core object
   */
  Album.prototype.getCore = function () {
      return this.core;
  };

  /**
   * Add custom content
   *
   * This function is called when user click on the addon's icon
   *
   * @return {void}
   */

  Album.prototype.add = function (images, style, edit, albumId) {
      this.edit = typeof(edit) != 'undefined' && edit == true ? true : false;
      this.selectedElement = this.edit == true ? this.selectedElement : null;
      editor.saveSelection();
      var _el = this.$el,sel, range, self = this, html;
      this.activeStyle = style?style:activeStyle;
      this.album_form = $('<div />').addClass('popup popup__insertAlbum');
      this.albumId = albumId?albumId:null;
      this.album_media_count = $("#album_media_count").val();

      this._wrap = null;
      this._previewAlbum = null;
      this._albumSelectTemplateList = null;

      $.ajax({
          url: 'http://react-backend.test/api/album', // change this to match server side
          type: 'post',
          data: {
              temp: 1,
              id: self.albumId
          },
          crossDomain: true,
          xhrFields: {withCredentials: false}, // change this to match server side
          headers: {
              'X-Requested-With': 'XMLHttpRequest',
              'X-OSC-Cross-Request': 'OK'
          },
          success: function (response) {
              if (response.result === 'OK') {
                  // self._wrap = $('#mediaV2_post_frm');
                  self._setup(response.data.main, response.data.sidebar);
                  self.albumId = $('#albumIdCreated').val();
              } else {
                  //self.close();
              }
          }
      });

      this._resetScroll = function () {
          $('body').removeClass('disabled-scroll');
      };

      this._disabledScroll = function () {
          $('body').addClass('disabled-scroll');
      };

      this.getItem = null;
      this.addImageItemBtn = null;
      this.getImagesBox = null;
      this.contentWrap = null;
      this._contentContainer = null;
      this._post_key = null;
      this._closePopup = null;

      this.AlbumMediaCount = 0;

      this._setup = function(main,sidebar) {
          var win = null, self = this;

          win = $.mediumEditorTemplates({
              destroy_hook: function () {
                  self._resetScroll();
              },
              title: 'Upload photos album',
              popupOverlayBg: true,
              content: main,
              sidebar: sidebar
          });

          self._wrap = $('.lh-editor-wrap');
          self._contentContainer = self._wrap.find('.content-container');
          self.getItem = self._wrap.find('.getItem');
          self.addImageItemBtn = self._wrap.find('.addAnImage button');
          self.getImagesBox = self._wrap.find('.getImagesBox');
          self.contentWrap = $('#mediaPostContent');
          self._inputForm = self._wrap.find('#lhAlbumPopUpload');
          self._previewAlbum = $('#selectAlbumTemplatesForm');
          self._albumSelectTemplateList = $('#albumSelectTemplateList');

          self._setupGetFromImageUrl();

          self._setupUploader(self._wrap.find('#albumEditorUpload'));

          self._closePopup = self._wrap.find('.editorClosePopup').click(function(){
              $('.lh-editor-wrap *').unbind();
              $('.addAnImage button').unbind();
              $('.lh-editor-wrap').remove();
              self._resetScroll();
          });

          if (typeof(images) != 'undefined') {

              for (var i = 0; i < images.length; i++) {
                  var _fillItem = null, _fillImage = null;

                  _fillImage = $('<img />').attr('src', images[i].url);
                  _fillItem = self._addMediaContentItem('image', _fillImage[0], {width: images[i].w, height: images[i].h});

                  var _img_class = (images[i].h < images[i].w)?'scaleByHeight':'scaleByWidth';

                  $(_fillItem).attr('data-content-key', images[i].media_id);
                  $(_fillItem).attr('data-item-type', images[i].type);
                  $(_fillItem).attr({'data-orig-w': images[i].w, 'data-orig-h':images[i].h}).find('img').attr('src', images[i].url).addClass(_img_class);

                  self._listenerUploaderHasFile(true);

                  self._resizeImageListRow($(_fillItem).parent()[0]);

                  self._wrap.find('#albumEditorUpload').addClass('hasImage');


              }
          }

          self._initAction();
      };

      this._setupGetFromImageUrl = function () {
          var self = this;
          $(document).on('change', ".imageInput", function () {
              var _imgUrl = $(this).val(),
                  _item = $(this).closest('.getItem');
              if(!_imgUrl) return;
              var res = _imgUrl.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)\.(jpeg|jpg|gif|png)/g);
              if(!res) {
                  return;
              }
              self._uploadFromImageUrl(_item, _imgUrl);
          });

          $(document).on('click', '.removeAdded', function(){
              var _item = $(this).closest('.getItem');
              _item.find('input').val('');
              _item.find('.imageAdded').removeAttr('style');
          });
      };

      this._uploadFromImageUrl = function(selector, imgUrl) {
          var _imgInput =  selector.find('input.imageInput'),
              self = this;

          if(!_imgInput.val()) return false;

          var item = null, image = null;

          self._post_key = self._wrap.data('key');
          $.ajax({
              url: "http://react-backend.test/api/image", //change this to match server side
              crossDomain: true,
              xhrFields: {withCredentials: false},  //change this to match server side
              headers: {
                  'X-Requested-With': 'XMLHttpRequest',
                  'X-OSC-Cross-Request': 'OK'
              },
              type: 'post',
              beforeSend: function() {
                  image = $('<img />').attr('src', imgUrl);
                  item = self._addMediaContentItem('image', image[0], {width: 760, height: 500});
                  self._resizeImageListRow($(item).parent()[0]);
              },
              success: function (response) {
                  if (response.result === 'OK') {
                      var _img_class = (response.data.height < response.data.width)?'scaleByHeight':'scaleByWidth';

                      $(item).attr('data-content-key', response.data.media_id);
                      $(item).attr('data-item-type', response.data.type);
                      $(item).attr('data-orig-w', response.data.width).attr('data-orig-h', response.data.height).find('img').attr('src', response.data.url).addClass(_img_class);

                      var itemChoose = $('<div />').addClass('round').html('<input type="checkbox" name="selectAlbumCover" id="selectAlbumCover_'+response.data.media_id+'" value="'+response.data.media_id+'"/><label for="selectAlbumCover_'+response.data.media_id+'"></label>').appendTo(item),
                          itemRemove = $('<button />').addClass('albumRemoveItem').html(albumConfig.icons.trash).appendTo(item).on('click', function () {
                              $(item).remove();
                              self._resizeImageListRow($(item).parent()[0]);
                          });

                      self._listenerUploaderHasFile(true);

                      self._resizeImageListRow($(item).parent()[0]);

                      self._wrap.find('#albumEditorUpload').addClass('hasImage');

                  } else {
                      alert(response.message);
                      $(item).removeClass('uploading-progress').find('.uploading-progress-bar').remove();

                      $(item).find('img').remove();
                      setTimeout(function () {
                          $(item).fadeOut({complete: function () {
                                  var row = $(item).parent()[0];
                                  $(item).remove();
                                  self._resizeImageListRow(row);
                              }});
                      }, 500);
                  }
              },
              error: function () {
                  alert('Upload fail, please try again!');
                  $(item).removeClass('uploading-progress').find('.uploading-progress-bar').remove();

                  $(item).find('img').remove();
                  setTimeout(function () {
                      $(item).fadeOut({complete: function () {
                              var row = $(item).parent()[0];
                              $(item).remove();
                              self._resizeImageListRow(row);
                          }});
                  }, 500);
              }
          });
      };

      this._setupUploader = function (uploader) {
          var self = this;
          self._post_key = self._wrap.data('key');

          uploader.oscV2_uploader({
              max_files: -1,
              max_connections: 5,
              process_url: "http://react-backend.test/api/image", //change this to match server side
              btn_content: '<span class="dragdrop-text">Drag &amp; drop multi photos here<br>hoặc</span><button class="uploadBtn">Choose photos computer</button>',
              dragdrop_content: 'Drop files here to start uploading...',
              extensions: ['jpg', 'jpeg', 'png', 'gif'],
              xhrFields: {withCredentials: false},//change this to match server side
              headers: {
                  'X-Requested-With': 'XMLHttpRequest',
                  'X-OSC-Cross-Request': 'OK'
              },
              callback_validate_files: function (files, instance) {
                  var img_counter = 0;

                  for (var i = 0; i < files.length; i++) {
                      var file = files[i];

                      if (!(file instanceof File)) {
                          continue;
                      }

                      var file_extension = instance._getFileNameFromFileObj(file).replace(/^.+\.([^\.]+)$/i, '$1').toLowerCase();

                      if (instance.extensions.indexOf(file_extension) < 0) {
                          continue;
                      }

                      if (['jpg', 'png', 'gif'].indexOf(file_extension) >= 0) {
                          img_counter++;
                      }
                  }
              }
          }).bind('uploader_add_file', function (e, file_id, file, file_name, file_size) {
              self._listenerUploaderAddFile(file_id, file, file_name);
          }).bind('uploader_upload_start', function (e, file_id, file, file_name, file_size) {
              self._listenerUploaderUploadStart(file_id);
          }).bind('uploader_upload_progress', function (e, file_id, file, file_size, uploaded_size, uploaded_percent) {
              self._listenerUploaderUploadProgress(file_id, uploaded_percent);
          }).bind('uploader_upload_complete', function (e, file_id, response, pointer) {
              self._listenerUploaderUploadComplete(pointer, file_id, response);
          }).bind('uploader_error', function (e, file_id, error_code, error_message) {
              self._listenerUploaderUploadError(file_id, error_code, error_message);
          });

          return uploader;
      };

      this._listenerUploaderAddFile = function(file_id, file, file_name) {
          var extension = file_name.replace(/^.+\.([^\.]+)$/i, '$1').toLowerCase(),
          self = this;

          var canvas = document.createElement('canvas');

          canvas.width = 760;
          canvas.height = 500;

          var ctx = canvas.getContext('2d');
          ctx.globalAlpha = 0;
          ctx.fillStyle = 'rgba(0,0,0,.05)';
          ctx.fillRect(0, 0, 760, 500);

          var image = $('<img />').attr('src', canvas.toDataURL('image/png'));

          self._addMediaContentItem('image', image[0], {width: 760, height: 500});

          image.parent().addClass('upload-holding').attr('file-id', file_id);
      };

      this._listenerUploaderUploadStart = function(file_id) {
          var item = this.contentWrap.find('[file-id="' + file_id + '"]');

          if (!item[0]) {
              return;
          }

          item.removeClass('upload-holding').addClass('uploading-progress');
          item.append($('<div />').addClass('uploading-progress-bar').append($('<div />')));
      };

      this._listenerUploaderUploadProgress = function(file_id, uploaded_percent) {
          var item = this.contentWrap.find('[file-id="' + file_id + '"]');

          if (!item[0]) {
              return;
          }

          item.find('.uploading-progress-bar > div').css('width', uploaded_percent + '%');
      };

      this._listenerUploaderUploadComplete = function(pointer, file_id, response) {
          var self = this;
          pointer.success = false;

          var item = this.contentWrap.find('[file-id="' + file_id + '"]');

          if (!item[0]) {
              return;
          }

          item.removeAttr('file-id').removeClass('uploading-progress').find('.uploading-progress-bar').remove();

          eval('response = ' + response);
          if(response.result != 'ERROR') {
              var _img_class = (response.files[0].height < response.files[0].width)?'scaleByHeight':'scaleByWidth';
              item.attr('data-content-key', response.files[0].media_id);
              item.attr('data-item-type', response.files[0].type);
              item.attr('data-orig-w', response.files[0].width).attr('data-orig-h', response.files[0].height).find('img').attr('src', response.files[0].url).addClass(_img_class);

              var itemChoose = $('<div />').addClass('round').html('<input type="checkbox" name="selectAlbumCover" id="selectAlbumCover_'+response.files[0].media_id+'" value="'+response.files[0].media_id+'"/><label for="selectAlbumCover_'+response.files[0].media_id+'"></label>').appendTo(item),
                  itemRemove = $('<button />').addClass('albumRemoveItem').html(albumConfig.icons.trash).appendTo(item).on('click', function () {
                      var row = item.parent()[0];
                      item.remove();
                      self._resizeImageListRow(row);
                      self._listenerUploaderHasFile();
                  });

              this._resizeImageListRow(item.parent()[0]);
              self._wrap.find('#albumEditorUpload').addClass('hasImage');
              self._listenerUploaderHasFile();

              // save album image assoc
              $.ajax({
                  url: 'http://react-backend.test/api/album', // change this to match server side
                  type: 'post',
                  data: {
                      mediaId: response.files[0].media_id,
                      id: self.albumId
                  },
                  crossDomain: true,
                  xhrFields: {withCredentials: false}, //change this to match server side
                  headers: {
                      'X-Requested-With': 'XMLHttpRequest',
                      'X-OSC-Cross-Request': 'OK'
                  },
                  beforeSend: function () {
                  },
                  success: function (response) {
                      if (response.result === 'OK') {

                      } else {
                          alert(response.message);
                      }
                  },
                  error: function () {
                      alert("Error");
                  }
              });
          } else {
              alert('upload fail! Please try again');
          }

      };

      this._listenerUploaderUploadError = function(file_id, error_code, error_message){
          var item = this.contentWrap.find('[file-id="' + file_id + '"]');

          if (!item[0]) {
              return;
          }

          item.removeClass('uploading-progress').find('.uploading-progress-bar').remove();

          item.find('img').remove();
          item.append($('<div />').addClass('error-message').html('<div>ERROR [' + error_code + ']</div>' + error_message));
          var self = this;
          setTimeout(function () {
              item.fadeOut({complete: function () {
                      var row = item.parent()[0];
                      item.remove();
                      self._resizeImageListRow(row);
                  }});
          }, 5000);
      };

      this._listenerUploaderHasFile = function(hasImage) {
          var self = this,
              uploader = self._wrap.find('#albumEditorUpload'),
              mediaList = self._wrap.find('#mediaPostContent ul li');

          if(mediaList.length == 0) {
              hasImage = false;
          } else {
              hasImage = true;
          }

          if(hasImage) {
              uploader.addClass('hasImage');
              self._wrap.find('.headbar').show();
              uploader.find('.dragdrop-text').hide();
              uploader.find('button.uploadBtn').html('<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
                  '                <path d="M16.7619 19.9048H2.09524V5.2381H11.5238V3.14286H2.09524C0.942857 3.14286 0 4.08571 0 5.2381V19.9048C0 21.0571 0.942857 22 2.09524 22H16.7619C17.9143 22 18.8571 21.0571 18.8571 19.9048V10.4762H16.7619V19.9048ZM8.60095 16.5838L6.54762 14.1114L3.66667 17.8095H15.1905L11.4819 12.8752L8.60095 16.5838ZM18.8571 3.14286V0H16.7619V3.14286H13.619C13.6295 3.15333 13.619 5.2381 13.619 5.2381H16.7619V8.37048C16.7724 8.38095 18.8571 8.37048 18.8571 8.37048V5.2381H22V3.14286H18.8571Z" fill="white"></path>\n' +
                  '            </svg> Thêm ảnh');

          } else {
              uploader.removeClass('hasImage');
              self._wrap.find('.headbar').hide();
              self._wrap.find('#importImageFromUrl').hide();
              uploader.find('button.uploadBtn').html('Choose photos computer');
              uploader.find('.dragdrop-text').show();
          }

      }

      this._addMediaContentItem = function (type, image, dim) {
          var self = this,
              img_list = self.contentWrap.find('ul');

          if (!img_list[0]) {
              img_list = $('<ul />').prependTo(this.contentWrap);
          }

          var row = img_list.find('li').last();

          if (!row[0] || row.children().length > 1) {
              row = $('<li />').appendTo(img_list);
          }

          $(image).bind('dragstart', function () {
              return false;
          });

          if (typeof dim !== 'object' || dim === null) {
              dim = {width: image.width, height: image.height};
          }

          $(image).bind('dragstart', function () {
              return false;
          });

          if (typeof dim !== 'object' || dim === null) {
              dim = {width: image.width, height: image.height};
          }

          var item = $('<div />').attr('item-type', type).attr('data-orig-w', dim.width).attr('data-orig-h', dim.height).append(image).appendTo(row);

          //var item = $('<li />').attr('item-type', type).attr('data-orig-w', dim.width).attr('data-orig-h', dim.height).append(image).appendTo(img_list);

          self._resizeImageListRow(row[0]);
          return item[0];
      };

      this._resizeImageListRow = function (row) {
          row = $(row);

          var collection = row.find('> div');

          if (collection.length < 1) {
              if (row.hasAttr('new-line-helper') !== 1) {
                  row.remove();
              }
              return;
          }

          collection.css({width: '', height: ''});

          var row_bonus_width = 0;

          var row_bonus_data = [row.css('padding-left'), row.css('padding-right')];

          for (var i = 0; i < row_bonus_data.length; i++) {
              row_bonus_data[i] = parseInt(row_bonus_data[i]);

              if (!isNaN(row_bonus_data[i])) {
                  row_bonus_width += row_bonus_data[i];
              }
          }

          var total_item_width = 0;

          var data = [];

          collection.each(function () {
              var item = $(this);

              if (!item.prev()[0]) {
                  var margin_left = parseInt(item.css('margin-left'));

                  if (!isNaN(margin_left)) {
                      row_bonus_width += margin_left;
                  }
              }

              var margin_right = parseInt(item.css('margin-right'));

              if (item.next()[0]) {
                  var sibling_margin_left = parseInt(item.next().css('margin-left'));

                  if (!isNaN(sibling_margin_left) && (isNaN(margin_right) || sibling_margin_left > margin_right)) {
                      margin_right = sibling_margin_left;
                  }
              }

              if (!isNaN(margin_right)) {
                  row_bonus_width += margin_right;
              }

              var new_w = item.attr('data-orig-w') * 100 / item.attr('data-orig-h');

              data.push({node: item, width: new_w});

              total_item_width += new_w;
          });

          $.each(data, function (i, item) {
              item.node.width('calc((100% - ' + row_bonus_width + 'px) * ' + item.width / total_item_width + ')');

              if (item.node.children().length === 0) {
                  item.node.height(item.node.attr('data-orig-h') * item.node.width() / item.node.attr('data-orig-w'));
              }
          });
      };

      this._fillImages = function(imgSrc, imgId, w, h) {
          var self = this;
          self._wrap.find('.lh-album').each(function (index) {
              var _al = $(this),
                  _items = _al.data('items'),
                  _figure = $("figure"),
                  _figures = _al.find(_figure),
                  _last_figure = _figures[_items-1],
                  _img = $('<img />').attr({'src': imgSrc,'data-source':'lh-user-media', 'data-lh-media-id': imgId, 'data-w': w, 'data-h': h});
              var empty = _al.find("figure:empty");
              if(empty.length == 0) {
                  $("<figure />").addClass('album-item').append(_img).attr('data-idx', _figures.length).appendTo(_al);
                  var moreItem = _figures.length - _items + 1;
                  if(moreItem > 0) {
                      $(_last_figure).attr('data-more','+'+moreItem);
                  }
                  return true;
              }
              $(empty[0]).append(_img);
              return true;
          });
      };

      this._getAlbumHtml = function(wrapper) {
          var self = this;
          var _album = self._previewAlbum.find('.lh-album')[0].outerHTML;
          return '<div class="medium-insert-album medium-insert-album-selected" contenteditable="false">' + _album + '</div>';
      };

      this._fillImageTemplate = function () {
          var self = this;
          var imageCount = $(self.contentWrap).find('div[item-type="image"]').length;
          $.each(albumConfig.albumTemplate, function(i, val) {
              var _itemTemplate = $('<div />').addClass('item').attr({'data-cover':val.items, 'data-style':val.styleName}).html(defaults.selectItem);
              var _item = $('<div />').addClass('lh-album').attr({'data-style':val.styleName, 'data-items': val.items, 'data-album': self.albumId});
              for (i = 0; i < val.items; i++) {
                  $('<figure />').addClass('album-item').attr('data-idx', i).appendTo(_item);
              }
              _item.on('click', function(){
                  $('.lh-album').removeClass('active');
                  $(this).addClass('active');
                  $('.editorSelectTemplateCheckbox').prop('checked', false);
                  $(this).closest('.item').find(".editorSelectTemplateCheckbox").prop('checked', true);
                  self._activePreviewAlbum(imageCount, val.styleName);
              });

              _itemTemplate.find("input").attr('id',val.styleName).on('change',function () {
                  $('.lh-album').removeClass('active');
                  _item.addClass('active');
                  self._activePreviewAlbum(imageCount, val.styleName);
              });
              _itemTemplate.find('label').attr('for', val.styleName);
              _item.appendTo(_itemTemplate.find(".lh-editorSidebar-content"));
              $(_itemTemplate).appendTo(self._albumSelectTemplateList);
          });

          self._albumSelectTemplateList.osc_scroller({});
      };

      this._initActiveTemplteByCoverNum = function () {
          var self = this;
          self._wrap.find('.actionButtonBlock button').on('click', function(){
              var _coverNumberToShow = $(this).data('covernumber');
              $('.actionButtonBlock button').removeClass('active');
              $(this).addClass('active');
              self._albumSelectTemplateList.find('.item').hide();
              self._albumSelectTemplateList.find('.item[data-cover="'+_coverNumberToShow+'"]').show();
              self._albumSelectTemplateList.osc_scroller('reset');
              self._albumSelectTemplateList.osc_scroller('scrollToPosition',1);
          });
      };

      this._activePreviewAlbum = function(imageCount, styleName){
          var self = this,
              styleDefault = 0,
              _activeStyle = null,
              _activeItem = null;
          if(styleName) {
              self._wrap.find('.actionButtonBlock button').each(function () {
                  var _coverNumberToShow = $(this).data('covernumber');
                  if(_coverNumberToShow > imageCount) {
                      $(this).hide();
                  }
              });

              _activeStyle = styleName;
              _activeItem = self._albumSelectTemplateList.find('.item[data-style="'+_activeStyle+'"]');
          }
          else {
              self._wrap.find('.actionButtonBlock button').each(function () {
                  var _coverNumberToShow = $(this).data('covernumber');
                  if(_coverNumberToShow > imageCount) {
                      $(this).hide();
                  } else {
                      if(styleDefault < _coverNumberToShow) styleDefault = _coverNumberToShow;
                  }
              });
              _activeStyle = self._albumSelectTemplateList.find('.item[data-cover="'+styleDefault+'"]').first().data('style');
              _activeItem = self._albumSelectTemplateList.find('.item[data-style="'+_activeStyle+'"]');

          }

          self._albumSelectTemplateList.find('.item').each(function () {
              var _coverCanShow = $(this).data('cover');
              if(_coverCanShow > imageCount) {
                  $(this).hide();
              }
          });
          self._albumSelectTemplateList.osc_scroller('reset');

          $("#albumIdCreatedStyle").val(_activeStyle);

          $('.lh-album').removeClass('active');
          $('.editorSelectTemplateCheckbox').prop('checked', false);
          _activeItem.find('.lh-album').addClass('active');
          _activeItem.find(".editorSelectTemplateCheckbox").prop('checked', true);

          var _albumHtml = _activeItem.find('.lh-editorSidebar-content').html(),
              _insertHtml = $(_albumHtml).removeClass('active');
          self._previewAlbum.html(_insertHtml);
      };

      this._initAction = function() {
          var self = this;

          self._wrap.find("#addMoreImagesFromUrl").on('click', function(){
              $('#importImageFromUrl').toggle();
          });

          self._wrap.find(".js-editorAlbumEditImages").on('click', function(){
              self._wrap.find('.lh-button-complete').hide();
              self._wrap.find('.sidebar-container').css({'right':'-333px'});
              $(self._contentContainer).removeClass('sidebar-active').addClass('overlay-bg');
              self._inputForm.show();
              self._previewAlbum.hide();
              self._albumSelectTemplateList.empty().osc_scroller('reset');
              self._wrap.find('.actionButtonBlock button').show();
          });

          self._wrap.find(".js-selectAlbumTemplate").on('click', function () {
              var imageCount = $(self.contentWrap).find('div[item-type="image"]').length;
              if(imageCount < 2) {
                  alert('Bạn phải upload ít nhất 2 ảnh cho Album!');
                  return;
              }
              self._fillImageTemplate();
              /* Start animation */
              self._wrap.find('.lh-button-complete').show();
              self._wrap.find('.sidebar-container').css({'right':'0px'});
              $(self._contentContainer).addClass('sidebar-active').removeClass('overlay-bg');
              self._inputForm.hide();
              self._previewAlbum.show();

              if(self.edit == true) {
                  self._activePreviewAlbum(imageCount, self.activeStyle);
              }  else {
                  self._activePreviewAlbum(imageCount, null);
              }

              $(self.contentWrap).find('div[item-type="image"]').each(function(){
                  var  imgSrc = $(this).find('img').attr('src'),
                      imgId = $(this).data('content-key'),
                      w = $(this).data('orig-w'),
                      h = $(this).data('orig-h');
                  self._fillImages(imgSrc, imgId, w, h);
              });

          });

          self._initActiveTemplteByCoverNum();

          self._wrap.find('.js-editorInsertAlbum').on('click', function () {
              var _btn = $(this),
                  selected,
                  _activeStyle = $("#albumIdCreatedStyle").val();

              if (self.edit == true) {
                  $(self.selectedElement).html(html = self._getAlbumHtml(false));
                  selected = self.selectedElement[0];
              } else {
                  $('.medium-insert-active').before(self._getAlbumHtml());
                  selected = editorContainer.find('.medium-insert-album:first')[0];
              }

              $.ajax({
                  url: linkhay_url + '/user/album/save', // change this to match server side
                  type: 'post',
                  data: {
                      id: self.albumId,
                      style: _activeStyle
                  },
                  crossDomain: true,
                  xhrFields: {withCredentials: true}, // change this to match server side
                  headers: {
                      'X-Requested-With': 'XMLHttpRequest',
                      'X-OSC-Cross-Request': 'OK'
                  },
                  beforeSend: function () {
                      _btn.attr('disabled','disabled');
                  },
                  success: function (response) {
                      if (response.result === 'OK') {

                      } else {
                          alert(response.message);
                      }
                      _btn.removeAttr('disabled');
                  },
                  error: function () {
                      alert("Error");
                      _btn.removeAttr('disabled');
                  }
              });

              if (typeof(editorContainer) != 'undefined') {
                  var p = editorContainer.find('p');
                  p.length == 1 && editorContainer.find('.medium-insert-album').length == 1 ? (editor.selectElement(p[0]), editor.checkContentChanged(), editor.selectElement(selected)) : null;
              }
              self._resetScroll();

              self._closePopup.trigger('click');
          });
      };

      if (typeof(style) == 'undefined') {
          style = 'style-1';
      }
      $('.popup__insertAlbum .lh-album[data-style="' + style + '"]').addClass('active');

      /**
       * hide button insert
       */
      try {
          this.$el.data('plugin_mediumInsert').hideButtons()
      } catch (e) {}
  };


  /** Addon initialization */

  $.fn[pluginName + addonName] = function (options) {
      return this.each(function () {
          if (!$.data(this, 'plugin_' + pluginName + addonName)) {
              $.data(this, 'plugin_' + pluginName + addonName, instance = new Album(this, options));
          }
      });
  };
})(jQuery, window, document);
