/** Change line 424, 428 to meet new situation */
const edit = "<div class='medium-insert-imageDescription-toolbar medium-editor-toolbar medium-toolbar-arrow-under medium-editor-toolbar-active' style='top: 708px; left: 533px;'>\
             <ul class='medium-editor-toolbar-actions clearfix'>\
            <li>\
                <button class='medium-editor-action medium-editor-button-active' data-action='edit'>Edit</button>\
            </li>\
            </>\
          </div>";

const deleteBtnToolbar = "<div class='medium-insert-imageDescription-toolbar2 medium-editor-toolbar medium-editor-toolbar-active' style='top: 765px; left: 889px;'>\
            <ul class='medium-editor-toolbar-actions clearfix'>\
              <li>\
                  <button class='medium-editor-action' data-action='remove'>"+deleteSvgIcon+"</button>\
              </li>\
            </ul>\
          </div>";

/** insert imageDescription **/
;(function ($, window, document, undefined) {
  'use strict';

  /** Default values */
  var instance = null,
      pluginName = 'mediumInsert',
      addonName = 'ImageDescription', // first char is uppercase
      defaults = {
          label: imageDescriptionSvgIcon
      },
      options = {
          styles: {
              edit: {
                  label: 'Edit',
                  clicked: function (e) {
                      instance.edit = true;
                      var selectedElement = $('.medium-insert-imageDescription.medium-insert-imageDescription-selected'),
                          elementContent = selectedElement.find('.boxContent p').html(),
                          elementImage = $('<div />').addClass('previewImage').html(selectedElement.find('.boxImage').html());

                      instance.selectedElement = selectedElement;

                      instance.add(true, elementImage[0].outerHTML, elementContent);
                  }
              }
          },
          actions: {
              remove: {
                  label: deleteSvgIcon,
                  clicked: function () {
                      var e = $.Event("keydown");
                      e.which = 8, $(document).trigger(e)
                  }
              }
          }
      };

  /**
   * Custom Addon object
   *
   * Sets options, variables and calls init() function
   *
   * @constructor
   * @param {DOM} el - DOM element to init the plugin on
   * @param {object} options - Options to override defaults
   * @return {void}
   */

  function ImageDescription(el, options) {
      this.el = el;
      this.$el = $(el);
      this.templates = window.MediumInsert.Templates;
      this.core = this.$el.data('plugin_'+ pluginName);

      this.options = $.extend(true, {}, defaults, options);

      this._defaults = defaults;
      this._name = pluginName;

      this.edit = false;

      this.init();
  }

  /**
   * Initialization
   *
   * @return {void}
   */

  ImageDescription.prototype.init = function () {
      var e = this.$el.find(".medium-insert-imageDescription");
      e.find(".imageDescriptionContent").attr("contenteditable", !1), this.events()
  },

  ImageDescription.prototype.selectimageDescription = function (e) {
      var t, i = this,
          t = $(e.target);
      if (t.hasClass('medium-insert-imageDescription') == false) {
          t = $(t).parents('.medium-insert-imageDescription');
      }

      t.addClass('medium-insert-imageDescription-selected'),
          setTimeout(function () {
              i.addToolbar()
          }, 50),
          this.$currentimageDescription = t,
          this.$el.blur();
  },

  ImageDescription.prototype.unselectimageDescription = function (e) {
      var t = $(e.target).hasClass("medium-insert-imageDescription") ? $(e.target) : $(e.target).closest(".medium-insert-imageDescription"),
          i = this.$el.find(".medium-insert-imageDescription-selected"),
          b = $(e.target);
      if ($(b).hasClass('medium-editor-action') || $(b).parents('button:first').hasClass('medium-editor-action')) {
          return;
      }
      if (t.hasClass("medium-insert-imageDescription-selected")) return i.not(t).removeClass("medium-insert-imageDescription-selected"), $(".medium-insert-imageDescription-toolbar, .medium-insert-imageDescription-toolbar2").remove();
      i.removeClass("medium-insert-imageDescription-selected"), $(".medium-insert-imageDescription-toolbar, .medium-insert-imageDescription-toolbar2").remove();
  },

  ImageDescription.prototype.removeimageDescription = function (e) {
      var t, i;
      8 !== e.which && 46 !== e.which || (t = this.$el.find(".medium-insert-imageDescription-selected")).length && (e.preventDefault(), $(".medium-insert-imageDescription-toolbar, .medium-insert-imageDescription-toolbar2").remove(), i = $(this.templates["src/js/templates/core-empty-line.hbs"]().trim()), t.before(i), t.remove(), this.core.hideAddons(), this.core.moveCaret(i), this.core.triggerInput())
  },

  ImageDescription.prototype.addToolbar = function () {
      var e, t,
          i = this.$el.find(".medium-insert-imageDescription-selected").closest(".medium-insert-imageDescription"),
          s = !1,
          o = this.core.getEditor().options.elementsContainer || "body";

      $(o).append(edit);
      $(o).append(deleteBtnToolbar);

      e = $(".medium-insert-imageDescription-toolbar"), t = $(".medium-insert-imageDescription-toolbar2"), e.find("button").each(function () {
          i.hasClass("medium-insert-imageDescription-" + $(this).data("action")) && ($(this).addClass("medium-editor-button-active"), s = !0)
      }), !1 === s && e.find("button").first().addClass("medium-editor-button-active"), this.repositionToolbars(), e.fadeIn(), t.fadeIn()
  },

  ImageDescription.prototype.autoRepositionToolbars = function () {
      setTimeout(function () {
          this.repositionToolbars(), this.repositionToolbars()
      }.bind(this), 0)
  },

  ImageDescription.prototype.repositionToolbars = function () {
      var e = $(".medium-insert-imageDescription-toolbar"),
          t = $(".medium-insert-imageDescription-toolbar2"),
          i = this.$el.find(".medium-insert-imageDescription-selected .imageDescriptionContent"),
          s = this.core.getEditor().options.elementsContainer,
          o = -1 < ["absolute", "fixed"].indexOf(window.getComputedStyle(s).getPropertyValue("position")),
          n = o ? s.getBoundingClientRect() : null,
          a = $(window).width(),
          r = {};
      if (typeof (i.offset()) == 'undefined') {
          return;
      }

      t.find('.medium-editor-action[data-action="edit"]').css({
          position: 'absolute',
          top: i.height() - 46,
          left: -(i.width() / 2 + 40)
      });
      t.length && (r.top = i.offset().top + 2, r.left = i.offset().left + i.width() - t.width() - 4, o && (r.top += s.scrollTop - n.top, r.left -= n.left, a = $(s).width()), r.left + t.width() > a && (r.left = a - t.width()), t.css(r)), e.length && (r.left = i.offset().left + i.width() / 2 - e.width() / 2, r.top = i.offset().top - e.height() - 8 - 2 - 5, o && (r.top += s.scrollTop - n.top, r.left -= n.left), r.top < 0 && (r.top = 0), e.css(r))
  },

  ImageDescription.prototype.toolbarAction = function (e) {
      var t = $(e.target).is("button") ? $(e.target) : $(e.target).closest("button"),
          i = (options.styles[t.data("action")] || {}).clicked;
      i && i(this.$el.find(".medium-insert-imageDescription-selected")), this.core.triggerInput(), this.repositionToolbars()
  },

  ImageDescription.prototype.toolbar2Action = function (e) {
      var t = $(e.target).is("button") ? $(e.target) : $(e.target).closest("button"),
          i = (options.actions[t.data("action")] || {}).clicked;
      i && i(this.$el.find(".medium-insert-imageDescription-selected")), this.core.triggerInput(), this.repositionToolbars()
  };

  /**
   * Event listeners
   *
   * @return {void}
   */

  ImageDescription.prototype.events = function () {
      $(document).on("click", $.proxy(this, "unselectimageDescription"))
          .on("keydown", $.proxy(this, "removeimageDescription"))
          .on("click", ".medium-insert-imageDescription-toolbar .medium-editor-action", $.proxy(this, "toolbarAction"))
          .on("click", ".medium-insert-imageDescription-toolbar2 .medium-editor-action", $.proxy(this, "toolbar2Action")),
          this.$el.on("click", ".medium-insert-imageDescription .imageDescriptionContent", $.proxy(this, "selectimageDescription")),
          $(document).on("resize", $.proxy(this, "autoRepositionToolbars"));
  };

  /**
   * Get the Core object
   *
   * @return {object} Core object
   */
  ImageDescription.prototype.getCore = function () {
      return this.core;
  };

  /**
   * Add custom content
   *
   * This function is called when user click on the addon's icon
   *
   * @return {void}
   */

  ImageDescription.prototype.add = function (isEdit, image, content) {
      this.edit = typeof(isEdit) != 'undefined' && isEdit == true ? true : false;
      this.selectedElement = this.edit == true ? this.selectedElement : null;
      editor.saveSelection();
      var _el = this.$el, self = this, html;
      this.image = image ? image : '';
      this.content = content?self._replaceBrN(content,true):'';
      this._mainBlock = '<div class="lh-editor-popupForm" id="lhImageDescription" data-key="">\n' +
          '                <span class="editorClosePopup">\n' + closeSvgIcon + "</span>\n'" +
          '                <div class="headbar">\n' +
          '                    <div class="titlebar">Tạo ảnh đính kèm nội dung</div>\n' +
          '                </div>\n' +
          '                <div class="imageDescriptionBox">\n' +
          '                    <div class="boxContent">\n' +
          '                        <textarea name="imageDescriptionContent" placeholder="Nhập nội dung tại đây">'+this.content+'</textarea>\n' +
          '                    </div>\n' +
          '                    <div class="boxImage" id="mediaPostContent">' + this.image + '\n'+
          '                        <div id="imageDescriptionUploader" class="dragdropBox"></div>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="imageSubmitBox">\n' +
          '                    <button class="js-editorInsertImageDescription new-btn new-btn__width-icon new-btn__width-icon--blue">Tạo mới</button>\n' +
          '                </div>\n' +
          '            </div>';
      this._sidebarBlock = null;
      this._wrap = null;
      this.contentWrap = null;
      this.textarea = null;
      this._closePopup = null;

      this._setup = function (main, sidebar) {
          var win = null, self = this;

          win = $.mediumEditorTemplates({
              destroy_hook: function () {
                  //Remove html
              },
              title: '',
              popupOverlayBg: true,
              content: main,
              sidebar: sidebar
          });


          self._wrap = $('#lhImageDescription');
          self.contentWrap = $('#mediaPostContent');

          self._setupUploader(self._wrap.find('#imageDescriptionUploader'));
          self._initAction();

      };

      this._resetScroll = function () {
          $('body').removeClass('disabled-scroll');
      };

      this._disabledScroll = function () {
          $('body').addClass('disabled-scroll');
      };

      this._replaceBrN = function (content, rev) {
          var regex = /<br\s*[\/]?>/gi;
          if(rev) {
              content = content.replace(regex, "\n");
          } else {
              regex = /(?:\r\n|\r|\n)/g;
              content = content.replace(regex, '<br>');
          }
          return content;
      };

      this._getHtml = function () {
          var self = this;
          var _html = self._wrap.find('.imageDescriptionBox'),
              _content = _html.find("textarea").val(),
              _img = _html.find('.previewImage').html();
          _content = self._replaceBrN(_content, false);
          if(!_img || _img == null) {
              return false;
          } else {
              return '<div class="medium-insert-imageDescription" contenteditable="false"><div class="imageDescriptionContent"><div class="boxContent"><p>' + _content + '</p></div><div class="boxImage">' + _img + '</div></div></div>';
          }
      };

      this._initAction = function () {
          var self = this;
          self.textarea = self._wrap.find('textarea');
          self.textarea.on('keydown', function(){
              var _el = $(this);
              setTimeout(function(){
                  _el.css({'height':_el[0].scrollHeight + 'px'});
              },0);
          });
          self._closePopup = self._wrap.find('.editorClosePopup').click(function () {
              $('.lh-editor-wrap *').unbind();
              $('.addAnImage button').unbind();
              $('.lh-editor-wrap').remove();
              self._resetScroll();
          });
          self._wrap.find('.js-editorInsertImageDescription').on('click', function () {
              var _html = self._wrap.find('.imageDescriptionBox'),
                  _content = _html.find("textarea").val(),
                  _img = _html.find('.previewImage').html();
              if(!_content || _content == null) {
                  alert('Bạn chưa nhập nội dung');
                  return;
              }
              if(!_img || _img == null) {
                  alert('Bạn chưa upload hình ảnh !');
                  return;
              }
              if (self.edit == true) {
                  $(self.selectedElement).html(html = self._getHtml());
              } else {
                  $('.medium-insert-active').before(self._getHtml());
              }

              self._closePopup.trigger('click');
          });

          if(self.edit == true) {
              self._wrap.find(".imageSubmitBox button").text('Update');
              self.textarea.css({'height':self.textarea[0].scrollHeight + 'px'});
              self._listenerUploaderHasFile(true);
              self._wrap.find('#imageDescriptionEditorUpload').addClass('hasImage');
          }
      };

      this._setupUploader = function (uploader) {
          var self = this;
          self._post_key = self._wrap.data('key');

          uploader.oscV2_uploader({
              max_files: -1,
              max_connections: 5,
              process_url: 'http://react-backend.test/api/image', // for local testing purposes, change this to fit in production
              btn_content: '<span class="dragdrop-text">Drag & drop photo here<br>hoặc</span><button class="uploadBtn">Choose photo computer</button>',
              dragdrop_content: 'Drop files here to start uploading...',
              extensions: ['jpg', 'jpeg', 'png', 'gif'],
              xhrFields: {withCredentials: false}, // for local testing purposes, change this to fit in production from false to true
              headers: {
                  'X-Requested-With': 'XMLHttpRequest',
                  'X-OSC-Cross-Request': 'OK',
              },
              callback_validate_files: function (files, instance) {
                  var img_counter = 0;

                  for (var i = 0; i < files.length; i++) {
                      var file = files[i];

                      if (!(file instanceof File)) {
                          continue;
                      }

                      var file_extension = instance._getFileNameFromFileObj(file).replace(/^.+\.([^\.]+)$/i, '$1').toLowerCase();

                      if (instance.extensions.indexOf(file_extension) < 0) {
                          continue;
                      }

                      if (['jpg', 'png', 'gif'].indexOf(file_extension) >= 0) {
                          img_counter++;
                      }
                  }
              }
          }).bind('uploader_add_file', function (e, file_id, file, file_name, file_size) {
              self._listenerUploaderAddFile(file_id, file, file_name);
          }).bind('uploader_upload_start', function (e, file_id, file, file_name, file_size) {
              self._listenerUploaderUploadStart(file_id);
          }).bind('uploader_upload_progress', function (e, file_id, file, file_size, uploaded_size, uploaded_percent) {
              self._listenerUploaderUploadProgress(file_id, uploaded_percent);
          }).bind('uploader_upload_complete', function (e, file_id, response, pointer) {
              self._listenerUploaderUploadComplete(pointer, file_id, response);
          }).bind('uploader_error', function (e, file_id, error_code, error_message) {
              self._listenerUploaderUploadError(file_id, error_code, error_message);
          });

          return uploader;
      };

      this._listenerUploaderAddFile = function (file_id, file, file_name) {
          var extension = file_name.replace(/^.+\.([^\.]+)$/i, '$1').toLowerCase(),
              self = this;

          var canvas = document.createElement('canvas');

          canvas.width = 1000;
          canvas.height = 600;

          var ctx = canvas.getContext('2d');
          ctx.globalAlpha = 0;
          ctx.fillStyle = 'rgba(0,0,0,.05)';
          ctx.fillRect(0, 0, 1000, 600);

          var image = $('<img />').attr('src', canvas.toDataURL('image/png'));

          self._addMediaContentItem('image', image[0], {width: 1000, height: 600});

          image.parent().addClass('upload-holding').attr('file-id', file_id);
      };

      this._listenerUploaderUploadStart = function (file_id) {
          var item = this.contentWrap.find('[file-id="' + file_id + '"]');

          if (!item[0]) {
              return;
          }

          item.removeClass('upload-holding').addClass('uploading-progress');
          item.append($('<div />').addClass('uploading-progress-bar').append($('<div />')));
      };

      this._listenerUploaderUploadProgress = function (file_id, uploaded_percent) {
          var item = this.contentWrap.find('[file-id="' + file_id + '"]');

          if (!item[0]) {
              return;
          }

          item.find('.uploading-progress-bar > div').css('width', uploaded_percent + '%');
      };

      this._listenerUploaderUploadComplete = function (pointer, file_id, response) {
          var self = this;
          pointer.success = false;

          var item = this.contentWrap.find('[file-id="' + file_id + '"]');

          if (!item[0]) {
              return;
          }

          item.removeAttr('file-id').removeClass('uploading-progress').find('.uploading-progress-bar').remove();

          eval('response = ' + response);
          console.log(response);
          if (response.result != 'ERROR') {
              item.attr('content-key', response.files[0].media_id);
              item.attr('item-type', response.files[0].type);
              item.attr('data-orig-w', response.files[0].width).attr('data-orig-h', response.files[0].height).find('img').attr('src', response.files[0].url);

              self._listenerUploaderHasFile(true);
              self._wrap.find('#imageDescriptionEditorUpload').addClass('hasImage');

          } else {
              alert('upload fail! Please try again');
          }

      };

      this._listenerUploaderUploadError = function (file_id, error_code, error_message) {
        console.error('Error in uploading file:', error_message)
      };

      this._listenerUploaderHasFile = function (hasImage) {
          var self = this;
          var uploader = self._wrap.find('#imageDescriptionUploader');
          if (hasImage) {
              uploader.addClass('hasImage');
              uploader.find('button.uploadBtn').addClass('bg-blue').html('Choose');
              uploader.find('.dragdrop-text').text('Drag & drop photo here hoặc');

          } else {
              uploader.removeClass('hasImage');
              uploader.find('button.uploadBtn').removeClass('bg-blue').html('Choose photo computer');
              uploader.find('.dragdrop-text').html('Drag & drop photo here<br/>hoặc');
          }
      }

      this._addMediaContentItem = function (type, image, dim) {
          var imgPreview = this.contentWrap.find('.previewImage');

          if (!imgPreview[0]) {
              imgPreview = $('<div />').addClass('previewImage').prependTo(this.contentWrap);
          }

          if (typeof dim !== 'object' || dim === null) {
              dim = {width: image.width, height: image.height};
          }

          var item = $('<div />').attr('item-type', type).attr('data-orig-w', dim.width).attr('data-orig-h', dim.height).append(image);

          imgPreview.html(item);

          return imgPreview[0];
      };

      this._setup(self._mainBlock, self._sidebarBlock);

      /**
       * hide button insert
       */
      try {
          this.$el.data('plugin_mediumInsert').hideButtons()
      } catch (e) {
      }
  };


  /** Addon initialization */

  $.fn[pluginName + addonName] = function (options) {
      return this.each(function () {
          if (!$.data(this, 'plugin_' + pluginName + addonName)) {
              $.data(this, 'plugin_' + pluginName + addonName, instance = new ImageDescription(this, options));
          }
      });
  };
})(jQuery, window, document);
