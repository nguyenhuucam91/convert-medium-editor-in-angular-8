/** insert hr **/
;(function($, window, document, undefined) {
  'use strict';

  /** Default values */
  var pluginName = 'mediumInsert',
      addonName = 'Hr', // first char is uppercase
      defaults = {
          label: hrSvgIcon
      },
      options = {}

  /**
   * Custom Addon object
   *
   * Sets options, variables and calls init() function
   *
   * @constructor
   * @param {DOM} el - DOM element to init the plugin on
   * @param {object} options - Options to override defaults
   * @return {void}
   */

  function CustomAddon(el, options) {
      this.el = el;
      this.$el = $(el);
      this.templates = window.MediumInsert.Templates;
      this.core = this.$el.data('plugin_'+ pluginName);

      this.options = $.extend(true, {}, defaults, options);

      this._defaults = defaults;
      this._name = pluginName;

      this.init();
  }

  /**
   * Initialization
   *
   * @return {void}
   */

  CustomAddon.prototype.init = function () {
      this.events();
  };

  /**
   * Event listeners
   *
   * @return {void}
   */

  CustomAddon.prototype.events = function () {

  };

  /**
   * Get the Core object
   *
   * @return {object} Core object
   */
  CustomAddon.prototype.getCore = function () {
      return this.core;
  };

  /**
   * Add custom content
   *
   * This function is called when user click on the addon's icon
   *
   * @return {void}
   */

  CustomAddon.prototype.add = function (e) {
      var sel, range, html = '<hr /><p class="medium-insert-active"></p>',
          options = {
              forcePlainText: true,
              cleanPastedHTML: true,
          };

      var parser = new UAParser();
      var browserName = parser.getBrowser().name;

      var hr = $('<hr/>'),
      // detect if current browser is firefox, then dont add <br/> tag
      p = browserName === "Firefox" ? $('<p/>').addClass('medium-insert-active')
                                    : $('<p/>').addClass('medium-insert-active').html("<br/>");

      editor.pasteHTML(hr[0].outerHTML + p[0].outerHTML, options);

      /**
       * hide button insert
       */
      try {
          this.$el.data('plugin_mediumInsert').hideButtons()
      } catch (e) {}
  };


  /** Addon initialization */

  $.fn[pluginName + addonName] = function (options) {
      return this.each(function () {
          if (!$.data(this, 'plugin_' + pluginName + addonName)) {
              $.data(this, 'plugin_' + pluginName + addonName, new CustomAddon(this, options));
          }
      });
  };
})(jQuery, window, document);

