/** insert hr **/
;(function($, window, document, undefined) {
  'use strict';

  /** Default values */
  var instance = null,
  pluginName = 'mediumInsert',
      addonName = 'Quoter', // first char is uppercase
      defaults = {
          label: '"',
          icons: {  // icon key => tuỳ chọn
            defaultBrand: brandSvgIcon
          }
      },
      options = {
        actions: {
          remove: {
            clicked: function() {
              var e = $.Event("keydown");
              e.which = 8, $(document).trigger(e)
            }
          },
          edit: {
            clicked: function() {
              instance.isEditClicked = true;
              //lấy giá trị của customQuote được selected
              var selectedElement = $('.medium-insert-customQuote.medium-insert-customQuote-selected'),
                  elementContent = selectedElement.find('.card-text').html(),
                  elementImage = selectedElement.find('.avatar').html(),
                  elementBrandName = selectedElement.find('.official').text(),
                  elementLink = selectedElement.find('.link-site a').attr('href'),
                  elementTemplate = selectedElement.find('.lh-quote-card').data('template');
              //set selectedCustomQuote to selected element to replace HTML after editing
              instance.selectedCustomQuote = selectedElement
              instance.add(true, [elementContent,elementImage,elementBrandName,elementLink,elementTemplate]);
            }
          }
        }
      }

  /**
   * Custom Addon object
   *
   * Sets options, variables and calls init() function
   *
   * @constructor
   * @param {DOM} el - DOM element to init the plugin on
   * @param {object} options - Options to override defaults
   * @return {void}
   */

  function Quoter(el, options) {
      this.el = el;
      this.$el = $(el);
      this.templates = window.MediumInsert.Templates;
      this.core = this.$el.data('plugin_'+ pluginName);

      this.options = $.extend(true, {}, defaults, options);

      this._defaults = defaults;
      this._name = pluginName;

      this.init();
  }

  /**
   * Initialization
   *
   * @return {void}
   */

  Quoter.prototype.init = function () {
    this.events();
  };

  /**
   * Event listeners
   *
   * @return {void}
   */

  Quoter.prototype.events = function () {
    $(document).on("click", $.proxy(this, "unselectCustomquote"))
    .on('keydown', $.proxy(this, 'removeCustomQuote'))
    //kich hoat nut sua
    .on("click", ".medium-insert-customQuote-toolbar .medium-editor-action", $.proxy(this, "toolbarAction"))
    // //kích hoạt nút xoá
    .on("click", ".medium-insert-customQuote-toolbar2 .medium-editor-action", $.proxy(this, "toolbar2Action")),
    this.$el.on("click", ".medium-insert-customQuote", $.proxy(this, "selectCustomQuote")); // this.$el points to linkhay-editor
    $(window).on('resize', $.proxy(this, 'repositionToolbars'));
  };

  /**
   * Get the Core object
   *
   * @return {object} Core object
   */
  Quoter.prototype.getCore = function () {
      return this.core;
  };

  /**
   * Unselect quote
   */
  Quoter.prototype.unselectCustomquote = function(e) {
    //kiểm tra current element có phải là highlighter hay không, nếu là highlighter thì set = target, nếu không thì lấy element cha gần nhất
    //ứng với highlighter đó, do khi click vào highlighter có thể bị click vào element bên trong highlighter nên cần lấy gần nhất
    var $currentCustomQuote = $(e.target).hasClass("medium-insert-customQuote") ? $(e.target) : $(e.target).closest(".medium-insert-customQuote"),
    //get all highlighter selected element when we click each highlighter.
    $quotes = this.$el.find('.medium-insert-customQuote-selected');

    //if selected element is another highlighter
    if ($currentCustomQuote.hasClass('medium-insert-customQuote-selected')) {
      // then remove only other classes except current highlighter which contain selected
      // so that only 1 selected is available
      $quotes.not($currentCustomQuote).removeClass('medium-insert-customQuote-selected');
    }

    //if selected element is not highlighter (maybe image, text, etc.)
    else {
      //remove all selected highlighter
      $quotes.removeClass('medium-insert-customQuote-selected');
    }

    //remove both toolbars if it's not selected; here, remove "Trash icon"
    $('.medium-insert-customQuote-toolbar').remove();
    $('.medium-insert-customQuote-toolbar2').remove();


  }

  /**
   * Remove quote
   */
  Quoter.prototype.removeCustomQuote = function(e) {
    if (e.which === 8) {
      //lấy customQuote selected hiện tại
      var $currentSelectedQuote = this.$el.find(".medium-insert-customQuote-selected");
      //nếu tồn tại selected customQuote
      if ($currentSelectedQuote.length) {
        var caret = $("<p><br/></p>");
        $currentSelectedQuote.before(caret);
        //Xoá selected customQuote và xoá button "Delete"
        $currentSelectedQuote.remove();
        $(".medium-insert-customQuote-toolbar2").remove();
        this.core.hideAddons();
        //trỏ caret đến caret vừa tạo, caret ở đây chính là (I) nhấp nháy
        this.core.moveCaret(caret);
      }
    }
  }

  /**
   * Activate "Edit" function when clicking on toolbar (defined at top)
   *
   * @return {void}
   */
  Quoter.prototype.toolbarAction = function(e) {
    //thêm 1 property để biết tình trạng hiện giờ là edit
    this.toolbar2Action(e);
  }

  /**
   * Activate "Delete" function when clicking on toolbar2 (defined at top)
   *
   * @return {void}
   */
  Quoter.prototype.toolbar2Action = function(e) {

    //kiểm tra xem nút ấn có phải là button hay không, tương tự như với dòng 98
    var $button = $(e.target).is("button") ? $(e.target) : $(e.target).closest("button");
    var i = options.actions[$button.data('action')].clicked;
    //trigger hàm ở trên đầu dòng 27
    i && i(this.$el.find(".medium-insert-customQuote-selected")); //change this only
    //ẩn button và trigger input
    this.core.hideButtons();
    this.core.triggerInput();
  }

  /**
   * Select customQuote
   */
  Quoter.prototype.selectCustomQuote = function(e) {
    var that = this, $currentCustomQuote;
    if (this.core.options.enabled) {
      //do bên trong customQuote có nhiều thành phần, vì thế phải check closest để lấy thành phần cha gần nhất ứng với element được click
      $currentCustomQuote = $(e.target).is('.medium-insert-customQuote') ? $(e.target) : $(e.target).closest('.medium-insert-customQuote');
      $currentCustomQuote.addClass('medium-insert-customQuote-selected'); //add class selected for highlighter which is focused
      //show toolbar after 0.05s if current highlighter is selected, triggered via e.target
      setTimeout(function () {
        that.addToolbar();
      }, 50);
    }
  }

  /**
   * Add toolbar
   */
  Quoter.prototype.addToolbar = function() {
    const editToolbar = "<div class='medium-insert-customQuote-toolbar medium-editor-toolbar medium-toolbar-arrow-under medium-editor-toolbar-active'>\
        <ul class='medium-editor-toolbar-actions clearfix'>\
            <li>\
                <button class='medium-editor-action medium-editor-button-active' data-action='edit'>Edit</button>\
            </li>\
        </ul>\
      </div>";
    const removeButton = "<div class='medium-insert-customQuote-toolbar2 medium-editor-toolbar medium-editor-toolbar-active'>\
                        <ul class='medium-editor-toolbar-actions clearfix'>\
                        <li>\
                            <button class='medium-editor-action' data-action='remove'>"+
                            deleteSvgIcon
                        +"</button>\
                        </li>\
                      </ul></div>";
    var mediumEditor = this.core.getEditor(), //lấy editor hiện tại
    toolbarContainer = mediumEditor.options.elementsContainer || 'body',  // lấy toolbar container
    $toolbar1 = $('.medium-insert-customQuote-toolbar'), //khối editToolbar được định nghĩa ở trên
    $toolbar2 = $('.medium-insert-customQuote-toolbar2'); //khối removeButton được định nghĩa ở trên
    $(toolbarContainer).append(removeButton); //chèn khối remove được định nghĩa ở trên vào element
    $(toolbarContainer).append(editToolbar); //chèn khối edit vào document

    this.repositionToolbars(); //căn lại toolbar
    $toolbar1.fadeIn();
    $toolbar2.fadeIn();//hiển thị toolbar2 ra bên ngoài, có cũng được không có cũng không sao, chèn vào nhằm thêm hiệu ứng fadeIn cho đẹp
  }


  /**
   * Reposition toolbars for highlighter when select, window resize
   *
   * @return {void}
   */
  Quoter.prototype.repositionToolbars = function () {
    var $toolbar = $('.medium-insert-customQuote-toolbar'), //toolbar cho edit
    $toolbar2 = $('.medium-insert-customQuote-toolbar2'), //toolbar cho delete

    //chỉnh sửa class để lấy class của selected plugin
    $selectedHighlighter = this.$el.find('.medium-insert-customQuote-selected .customQuoteContent'),
    elementsContainer = this.core.getEditor().options.elementsContainer,
    elementsContainerAbsolute = ['absolute', 'fixed'].indexOf(window.getComputedStyle(elementsContainer).getPropertyValue('position')) > -1,
    elementsContainerBoundary = elementsContainerAbsolute ? elementsContainer.getBoundingClientRect() : null,
    containerWidth = $(window).width();


    if ($toolbar.length) {
      var position = {}
      // thay đổi số này để chỉnh sửa nút theo trục dọc
      position.top = $selectedHighlighter.offset().top - 46; //trừ đi height của button edit, tuỳ chỉnh con số 46 sao cho hợp mắt
      // thay đổi số này để chỉnh sửa nút theo trục ngang = margin trái + 1/2 width = giữa
      position.left = $selectedHighlighter.offset().left + $selectedHighlighter.width() / 2

      if (elementsContainerAbsolute) {
          position.top += elementsContainer.scrollTop - elementsContainerBoundary.top;
          position.left -= elementsContainerBoundary.left;
          containerWidth = $(elementsContainer).width();
      }

      if (position.left + $toolbar.width() > containerWidth) {
          position.left = containerWidth - $toolbar.width();
      }

      $toolbar.css(position);
  }


    if ($toolbar2.length) {
        var position = {};
        // thay đổi số này để chỉnh sửa nút theo trục dọc
        position.top = $selectedHighlighter.offset().top + 2;
        // thay đổi số này để chỉnh sửa nút theo trục ngang
        position.left = $selectedHighlighter.offset().left + $selectedHighlighter.width() - $toolbar2.width() - 8;

        if (elementsContainerAbsolute) {
            position.top += elementsContainer.scrollTop - elementsContainerBoundary.top;
            position.left -= elementsContainerBoundary.left;
            containerWidth = $(elementsContainer).width();
        }

        if (position.left + $toolbar2.width() > containerWidth) {
            position.left = containerWidth - $toolbar2.width();
        }

        $toolbar2.css(position);
    }
  }

  /**
   * Add custom content
   *
   * This function is called when user click on the addon's icon
   *
   * @return {void}
   */

  Quoter.prototype.add = function (isEditing, dataEdit) {
    this.isEditClicked = isEditing;
    this.title = 'Tạo quote';
    this.buttonPrimaryLabel = 'Tạo mới'
    this.content = '';
    this.image = defaults.icons.defaultBrand;
    this.brandName = '';
    this.link = '';
    this.template = 1;
    this.uploadText = 'Upload';
    if (this.isEditClicked) {
      this.title = 'Chỉnh sửa quote'
      this.content = dataEdit[0];
      this.image = dataEdit[1];
      this.brandName = dataEdit[2];
      this.link = dataEdit[3];
      this.template = dataEdit[4];
      this.buttonPrimaryLabel = 'Cập nhật'
    }

    this._mainBlock = '<div class="lh-editor-popupForm" id="lhCustomQuote" data-key="">\n' +
    '                <span class="editorClosePopup">'+closeSvgIcon+'</span>\n' +
    '                <div class="headbar">\n' +
    '                    <div class="titlebar">'+ this.title +'</div>\n' +
    '                </div>\n' +
    '                <div class="customQuotenBox">\n' +
    '                    <div class="boxContent">\n' +
    '                        <textarea name="customQuoteContent" maxlength="350" placeholder="Nhập nội dung tại đây">'+this.content+'</textarea>\n' +
    '                        <div class="char-count"><span class="character-count">0</span>/350</div>\n' +
    '                    </div>\n' +
    '                    <div class="boxBrand customQuoteInfo">\n' +
    '                        <label for="brandName">THƯƠNG HIỆU, TÁC GIẢ</label>\n' +
    '                        <div class="iconBrandInsert">\n' +
    '                            <div id="mediaPostContent">\n' +
    '                                <span class="brandIcon">'+this.image+'</span>\n' +
    '                                <div class="edit-button" id="iconBrandUpload">'+this.uploadText+'</div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="inputCol">\n' +
    '                            <input type="text" name="brandName" id="brandName" value="'+this.brandName+'"/>\n' +
    '                            <span>Không bắt buộc</span>\n' +
    '                        </div>\n' +
    '                        <div class="clearfix"></div>\n' +
    '                    </div>\n' +
    '                    <div class="boxLinkAttach customQuoteInfo">\n' +
    '                        <label for="linkAttach">LINK ĐÍNH KÈM</label>\n' +
    '                        <div class="inputCol">\n' +
    '                            <input type="text" name="linkAttach" id="linkAttach" value="'+this.link+'"/>\n' +
    '                            <span>Không bắt buộc</span>\n' +
    '                        </div>\n' +
    '                        <div class="clearfix"></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="imageSubmitBox">\n' +
    '                    <button class="js-editorCustomQuoteSelectTemplate new-btn new-btn__width-icon new-btn__width-icon--blue">'+this.buttonPrimaryLabel+'</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="lh-editor-popupForm" id="selectTemplatesForm" style="display: none">\n' +
    '                <div class="card lh-quote-card lh-quote-card-1" data-template="1" data-active="true">\n' +
    '                    <div class="card-body">\n' +
    '                        <h5 class="card-title"><img src="'+linkhay_url+'/resource/template/image/default/core/editor/medium/quote.svg"> </h5>\n' +
    '                        <p class="card-text"></p>\n' +
    '                        <div class="card-footer">\n' +
    '                            <div class="col-sm-6 p-0 pull-left">\n' +
    '                                    <span class="avatar"></span>\n' +
    '                                <span class="official"></span>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-6 p-0 pull-left text-right">\n' +
    '                                <span class="link-site"> <i class="fa fa-external-link" aria-hidden="true"></i></span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="lh-quote-footer-button">\n' +
    '                        <button class="js-editCustomQuoteContentBtn btn btn-default btn-edit-quote">Sửa nội dung quote</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="card lh-quote-card lh-quote-card-2" data-template="2" style="display: none;">\n' +
    '                    <div class="card-body">\n' +
    '                        <h5 class="lh-quote-card-icon">\n' +
    '                            <span class="lh-quote-card-bg"></span>\n' +
    '                        </h5>\n' +
    '                        <p class="card-text"></p>\n' +
    '                        <div class="card-footer">\n' +
    '                            <div class="col-sm-6 p-0 pull-left">\n' +
    '                                    <span class="avatar"></span>\n' +
    '                                <span class="official"></span>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-6 p-0 pull-left text-right">\n' +
    '                                <span class="link-site"> <i class="fa fa-external-link" aria-hidden="true"></i></span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="lh-quote-footer-button">\n' +
    '                        <button class="js-editCustomQuoteContentBtn btn btn-default btn-edit-quote">Sửa nội dung quote</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="card lh-quote-card lh-quote-card-3" data-template="3" style="display: none;">\n' +
    '                    <div class="card-body">\n' +
    '                        <h5 class="card-title">\n' +
    '                            <div class="col-sm-12 p-0 text-center">\n' +
    '                                <div class="avatar"></div>\n' +
    '                                <div class="official mt-3"></div>\n' +
    '                            </div>\n' +
    '                        </h5>\n' +
    '                        <p class="card-text">\n' +
    '                            <img src="'+linkhay_url+'/resource/template/image/default/core/editor/medium/quote.svg"></p>\n' +
    '                        <div class="card-footer">\n' +
    '                            <div class="col-sm-12 p-0 pull-left text-center">\n' +
    '                                <span class="link-site"> <i class="fa fa-external-link" aria-hidden="true"></i></span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="lh-quote-footer-button">\n' +
    '                        <button class="js-editCustomQuoteContentBtn btn btn-default btn-edit-quote">Sửa nội dung quote</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="card lh-quote-card lh-quote-card-4" data-template="4" style="display: none;">\n' +
    '                    <div class="card-body">\n' +
    '                        <p><img src="'+linkhay_url+'/resource/template/image/default/core/editor/medium/quote.svg"> </p>\n' +
    '                        <p class="card-text"></p>\n' +
    '                        <div class="card-footer">\n' +
    '                            <div class="col-sm-6 p-0 pull-left">\n' +
    '                                    <span class="avatar"></span>\n' +
    '                                <span class="official"></span>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-6 p-0 pull-left text-right">\n' +
    '                                <span class="link-site"> <i class="fa fa-external-link" aria-hidden="true"></i></span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="lh-quote-footer-button">\n' +
    '                        <button class="js-editCustomQuoteContentBtn btn btn-default btn-edit-quote">Sửa nội dung quote</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="card lh-quote-card lh-quote-card-5" data-template="5" style="display: none;">\n' +
    '                        <div class="card-body">\n' +
    '                            <h5 class="lh-quote-card-icon">\n' +
    '                                <span class="lh-quote-card-bg"></span>\n' +
    '                            </h5>\n' +
    '                            <p class="card-text"></p>\n' +
    '                            <div class="card-footer">\n' +
    '                                <div class="col-sm-6 p-0 pull-left">\n' +
    '                                        <span class="avatar"></span>\n' +
    '                                    <span class="official"></span>\n' +
    '                                </div>\n' +
    '                                <div class="col-sm-6 p-0 pull-left text-right">\n' +
    '                                    <span class="link-site"> <i class="fa fa-external-link" aria-hidden="true"></i></span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="lh-quote-footer-button">\n' +
    '                            <button class="js-editCustomQuoteContentBtn btn btn-default btn-edit-quote">Sửa nội dung quote</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '            </div>\n'+
    '            <div class="imageSubmitBox lh-button-complete" style="display: none;">\n' +
    '                <button class="js-editorInsertcustomQuote new-btn new-btn__width-icon new-btn__width-icon--blue">Hoàn tất</button>\n' +
    '            </div>\n';

this._sidebarBlock = '<h4 class="sidebar-title">Chọn kiểu hiển thị‹</h4>\n' +
    '        <div class="lh-editorSidebar-list">\n' +
    '            <div class="item">\n' +
    '                <div class="lh-editorSidebar-checkbox">\n' +
    '                    <div class="round">\n' +
    '                        <input type="checkbox" id="checkbox1" class="editorSelectTemplateCheckbox" value="1" checked="checked">\n' +
    '                        <label for="checkbox1"></label>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="lh-editorSidebar-content lh-quote-content type-1">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="item">\n' +
    '                <div class="lh-editorSidebar-checkbox">\n' +
    '                    <div class="round">\n' +
    '                        <input type="checkbox" id="checkbox2" class="editorSelectTemplateCheckbox" value="2">\n' +
    '                        <label for="checkbox2"></label>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="lh-editorSidebar-content lh-quote-content type-2">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="item">\n' +
    '                <div class="lh-editorSidebar-checkbox">\n' +
    '                    <div class="round">\n' +
    '                        <input type="checkbox" id="checkbox3" class="editorSelectTemplateCheckbox" value="3">\n' +
    '                        <label for="checkbox3"></label>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="lh-editorSidebar-content lh-quote-content type-3">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="item">\n' +
    '                <div class="lh-editorSidebar-checkbox">\n' +
    '                    <div class="round">\n' +
    '                        <input type="checkbox" id="checkbox4" class="editorSelectTemplateCheckbox" value="4">\n' +
    '                        <label for="checkbox4"></label>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="lh-editorSidebar-content lh-quote-content type-4">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="item">\n' +
    '                <div class="lh-editorSidebar-checkbox">\n' +
    '                    <div class="round">\n' +
    '                        <input type="checkbox" id="checkbox5" class="editorSelectTemplateCheckbox" value="5">\n' +
    '                        <label for="checkbox5"></label>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="lh-editorSidebar-content lh-quote-content type-5">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>';

    this._wrap = null;
    this._contentContainer = null;
    this._selectTemplateForm = null;
    this._selectedTemplateForm = null;
    this._sidebarContainer = null;
    this._lhEditorWrap = null;

    this._setup(this._mainBlock, this._sidebarBlock); //gọi làm _setup phía dưới để chèn template vào trang dưới dạng modal box

    /**
     * hide button insert
     */
    try {
        this.$el.data('plugin_mediumInsert').hideButtons()
    } catch (e) {}
  };

  /**
   * Hiển thị template HTML quote lên
   */

  Quoter.prototype._setup = function (main, sidebar) { // mục đích hàm này được dùng để setup template
    $.mediumEditorTemplates({ //đã được build sẵn trong source code
        destroy_hook: function () {
            //Remove html
        },
        title: '',
        popupOverlayBg: true,
        content: main,
        sidebar: sidebar
    });

    this._lhEditorWrap = $(".lh-editor-wrap");
    this._wrap = $("#lhCustomQuote");
    this._contentContainer=$(".content-container");
    this._selectTemplateForm = $("#selectTemplatesForm");
    this._selectedTemplateForm = $("#selectTemplatesForm").find('.lh-quote-card[data-active="true"]');
    this._sidebarContainer = $(".sidebar-container");


    this.initActions(); // run action ví dụ như xoá hoặc tạo mới on created element -
  };

  /**
   * Xử lí hành động khi người dùng bấm vào nút "X" hoặc ấn vào nút "Tiếp theo"
   */
  Quoter.prototype.initActions = function() {
    var self = this;
    // nếu nút "Xoá" được ấn
    self._closePopup = this._wrap.find('.editorClosePopup').on('click', function() {
        $('.lh-editor-wrap *').unbind();
        $('.addAnImage button').unbind();
        //xoá backdrop
        $('.lh-editor-wrap').remove();
        //cho phép cuộn xuống trang cuối cùng để nhập tiếp
        self.resetScroll();
    });

    //nếu nút upload brand (upload ảnh được ấn)
    this._wrap.find("#iconBrandUpload").on('click', function() {
      self.setupUploader();
    });

    //nếu ấn nút "Tạo mới" sau khi đã upload xong và nhập text vào các trường trong customQuote
    this._wrap.find('.js-editorCustomQuoteSelectTemplate').on('click', function() {
      //Lấy toàn bộ nội dung để tiến hành validate
      var _content = $("textarea[name='customQuoteContent']").val(),
      _brandName = $("#brandName").val(),
      _linkAttach = $("#linkAttach").val(),
      _brandImage = $('.brandIcon').html();
      //validate fields, ví dụ (đã được comment)
      // if(!_content) {
      //   alert('Bạn chưa nhập nội dung quote');
      //   return;
      // }

      self._selectTemplateForm.find('.card-text').html(_content);
      //Sau khi đã validate hết toàn bộ nội dung, mở trang mới để tiến hành chọn template
      //Ẩn trang chính (trang editing có caret I nhấp nháy) ==> Hầu hết là để chỉnh DOM
      self._wrap.hide();
      //mở trang cho phép chọn template customQuote
      self._selectTemplateForm.show();
      //hiển thị nút "Hoàn tất"
      $('.lh-button-complete').show();
      //hiển thị sidebar chọn template, sở dĩ cho right: 0 thay vì display: block do muốn có animations kéo trượt Right-to-left
      $('.sidebar-container').css({'right':'0px'});
      //thêm class sidebar-active để mở sidebar bên phải và xoá bỏ background overlay (backdrop)
      self._contentContainer.addClass('sidebar-active').removeClass('overlay-bg');
      //điền thông tin tương ứng vào "quote" được preview
      if(_brandName) {
        self._selectTemplateForm.find('.official').html(_brandName);
    }
    if(_linkAttach) {
        self._selectTemplateForm.find('.link-site').html('<a href="'+_linkAttach+'">'+_linkAttach+'</a> <i class="fa fa-external-link" aria-hidden="true"></i>');
    }

        self._selectTemplateForm.find('.avatar').html(_brandImage);
    });

    //Tiến hành sửa Quote, ngược lại của hàm trên
    this._selectTemplateForm.find('.js-editCustomQuoteContentBtn').on('click', function() {
      var brand = self._selectedTemplateForm.find('.official').text(),
      linkAttach = self._selectedTemplateForm.find('.link-site').text(),
      avatar = self._selectedTemplateForm.find('.avatar').html(),
      content = self._selectedTemplateForm.find('.card-text').html()

      $('.sidebar-container').css({'right':'-333px'});
      self._selectTemplateForm.hide();
      $('.lh-button-complete').hide();
      self._contentContainer.removeClass('sidebar-active').addClass('overlay-bg');
      //đẩy data ở trên vào các trường tương ứng
      $("textarea[name='customQuoteContent']").val(content),
      $("#brandName").val(brand),
      $("#linkAttach").val(linkAttach),
      $('.brandIcon').html(avatar);

      self._wrap.show();
    });

    //Chọn template
    this._sidebarContainer.find('.item').on('click', function() {
      //bỏ checked tất cả item
      $('.editorSelectTemplateCheckbox').prop('checked', false);
      //check template sidebar được chọn
      var selectedTemplateCheckbox = $(this).find('.editorSelectTemplateCheckbox');

      selectedTemplateCheckbox.prop('checked', true);
      //lấy giá trị của selected template sidebar
      var selectedTemplateCheckboxValue = selectedTemplateCheckbox.val();
      //hide all template preview, remove previous active template indicator
      self._selectTemplateForm.find('.lh-quote-card').removeAttr('data-active').hide();
      //only show template which is checked
      self._selectTemplateForm.find('.lh-quote-card[data-template="'+selectedTemplateCheckboxValue+'"]').attr('data-active', 'true').show();
    });

    //Hoàn tất
    this._contentContainer.find('.js-editorInsertcustomQuote').click(function() {
      //nếu nút hoàn tất được ấn sau khi Edit, thay thế element được chọn = template
      if (self.isEditClicked == true) {
        self.selectedCustomQuote.html(self.getQuoteInHtml());
      } else {
        //nếu chế độ đang làm là thêm mới customQuote => chèn khối customQuote được tạo ra vào
        $('.medium-insert-active').before(self.getQuoteInHtml());
      }
      self._closePopup.trigger('click');
    });
  }


  Quoter.prototype.resetScroll = function() {
    $('body').removeClass('disabled-scroll');
  }

  /**
   * Get quote in html format
   */
  Quoter.prototype.getQuoteInHtml = function () {
    var _html = this._selectTemplateForm.find('.lh-quote-card[data-active="true"]')[0].outerHTML;
    return '<div class="medium-insert-customQuote" contenteditable="false"><div class="customQuoteContent">'+_html+'</div></div>';
  }

  /**
   * Setup uploader cho ảnh
   */
  Quoter.prototype.setupUploader = function () {
    var that = this,
        $file = $(this.templates['src/js/templates/images-fileupload.hbs']()),
        fileUploadOptions = {
          url: 'http://react-backend.test/api/upload', //url được upload
          type: 'post',
          acceptFileTypes: /(.|\/)(gif|jpe?g|png)$/i,
          dataType: 'json',
          add: function (e, data) {
              $.proxy(that, 'uploadAdd', e, data)();
          },
          done: function (e, data) {
              $.proxy(that, 'uploadDone', e, data )();
          }
        };

    // Only add progress callbacks for browsers that support XHR2,
    // and test for XHR2 per:
    // http://stackoverflow.com/questions/6767887/
    // what-is-the-best-way-to-check-for-xhr2-file-upload-support
    if (new XMLHttpRequest().upload) {
        fileUploadOptions.progress = function (e, data) {
            $.proxy(that, 'uploadProgress', e, data)();
        };

        fileUploadOptions.progressall = function (e, data) {
            $.proxy(that, 'uploadProgressall', e, data)();
        };
    }

    $file.fileupload($.extend(true, {}, this.options.fileUploadOptions, fileUploadOptions));

    $file.click();
  }

  /**
   * Trigger when user choose file to begin uploading
   */
  Quoter.prototype.uploadAdd = function(e, data) {
    data.process().done(function() {
      data.submit();
    })
  }


  /**
     * Callback for global upload progress events
     * https://github.com/blueimp/jQuery-File-Upload/wiki/Options#progressall
     *
     * @param {Event} e
     * @param {object} data
     * @return {void}
     */

    Quoter.prototype.uploadProgressall = function (e, data) {
      var progress, $progressbar;

      if (this.options.preview === false) {
          progress = parseInt(data.loaded / data.total * 100, 10);
          $progressbar = this.$el.find('.medium-insert-active').find('progress');

          $progressbar
              .attr('value', progress)
              .text(progress);

          if (progress === 100) {
              $progressbar.remove();
          }
      }
  };

  /**
   * After upload file thì làm gì
   */
  Quoter.prototype.uploadDone = function(e, data) {
    var imageUrl = data.result.files[0].url,    //lấy response từ server về sau khi upload với key là result
        imageId = data.result.files[0].media_id;  //lấy response từ server về sau khi upload với key là result
    //tạo thẻ img mới
    var imgHtml = $("<img />").attr({ 'src': imageUrl, 'data-lh-media-id': imageId })
    //Tiến hành thay thế ô svg thành image đã được upload.
    $('.iconBrandInsert').find('.brandIcon').html(imgHtml);
    //Thay đổi text thành edit
    $("#iconBrandUpload").text('Edit');
  }

  /**
   * Callback for upload progress events.
   * https://github.com/blueimp/jQuery-File-Upload/wiki/Options#progress
   *
   * @param {Event} e
   * @param {object} data
   * @return {void}
   */

  Quoter.prototype.uploadProgress = function (e, data) {
      var progress, $progressbar;

      if (this.options.preview) {
          progress = 100 - parseInt(data.loaded / data.total * 100, 10);
          $progressbar = data.context.find('.medium-insert-images-progress');

          $progressbar.css('width', progress + '%');

          if (progress === 0) {
              $progressbar.remove();
          }
      }
  };

/** Addon initialization */

  $.fn[pluginName + addonName] = function (options) {
      return this.each(function () {
          if (!$.data(this, 'plugin_' + pluginName + addonName)) {
              $.data(this, 'plugin_' + pluginName + addonName, instance = new Quoter(this, options));
          }
      });
  };
})(jQuery, window, document);

