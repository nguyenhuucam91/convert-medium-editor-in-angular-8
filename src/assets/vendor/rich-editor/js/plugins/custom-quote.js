const editTemplate = "<div class='medium-insert-customQuote-toolbar medium-editor-toolbar medium-toolbar-arrow-under medium-editor-toolbar-active'>\
                                    <ul class='medium-editor-toolbar-actions clearfix'>\
                                        <li>\
                                            <button class='medium-editor-action medium-editor-button-active' data-action='edit'>Edit</button>\
                                        </li>\
                                    </ul>\
                                  </div>"
const removeButton = "<div class='medium-insert-customQuote-toolbar2 medium-editor-toolbar medium-editor-toolbar-active'>\
                        <ul class='medium-editor-toolbar-actions clearfix'>\
                        <li>\
                            <button class='medium-editor-action' data-action='remove'>"+
                            deleteSvgIcon
                        +"</button>\
                        </li>\
                      </ul></div>";


/** insert CustomQuote **/
;(function ($, window, document, undefined) {
  'use strict';


  /** Default values */
  var instance = null,
      pluginName = 'mediumInsert',
      addonName = 'CustomQuote', // first char is uppercase
      defaults = {
          label: quoteSvgIcon,
          icons: {
            defaultBrand: brandSvgIcon
          }
      },
      options = {
          styles: {
              edit: {
                  label: 'Edit',
                  clicked: function (e) {
                      instance.edit = true;
                      var selectedElement = $('.medium-insert-customQuote.medium-insert-customQuote-selected'),
                          elementContent = selectedElement.find('.card-text').html(),
                          elementImage = selectedElement.find('.avatar').html(),
                          elementBrandName = selectedElement.find('.official').text(),
                          elementLink = selectedElement.find('.link-site a').attr('href'),
                          elementTemplate = selectedElement.find('.lh-quote-card').data('template');

                      instance.selectedElement = selectedElement;

                      instance.add(true, [elementContent,elementImage,elementBrandName,elementLink,elementTemplate]);
                  }
              }
          },
          actions: {
              remove: {
                  label: deleteSvgIcon,
                  clicked: function () {
                      var e = $.Event("keydown");
                      e.which = 8, $(document).trigger(e)
                  }
              }
          }
      };

  /**
   * Custom Addon object
   *
   * Sets options, variables and calls init() function
   *
   * @constructor
   * @param {DOM} el - DOM element to init the plugin on
   * @param {object} options - Options to override defaults
   * @return {void}
   */

  function CustomAddon(el, options) {
      this.el = el;
      this.$el = $(el);
      this.templates = window.MediumInsert.Templates;
      this.core = this.$el.data('plugin_'+ pluginName);

      this.options = $.extend(true, {}, defaults, options);

      this._defaults = defaults;
      this._name = pluginName;

      this.edit = false;

      this.init();
  }

  /**
   * Initialization
   *
   * @return {void}
   */

  CustomAddon.prototype.init = function () {
      var e = this.$el.find(".medium-insert-customQuote");
      e.find(".customQuoteContent").attr("contenteditable", !1), this.events()
  },

  CustomAddon.prototype.selectcustomQuote = function (e) {
      var t, i = this,
          t = $(e.target);
          if (t.hasClass('medium-insert-customQuote') == false) {
            t = $(t).parents('.medium-insert-customQuote');
          }

      t.addClass('medium-insert-customQuote-selected'),
          setTimeout(function () {
              i.addToolbar()
          }, 50),
          this.$currentcustomQuote = t,
          this.$el.blur();
  },

  CustomAddon.prototype.unselectcustomQuote = function (e) {
      var t = $(e.target).hasClass("medium-insert-customQuote") ? $(e.target) : $(e.target).closest(".medium-insert-customQuote"),
          i = this.$el.find(".medium-insert-customQuote-selected"),
          b = $(e.target);
      if ($(b).hasClass('medium-editor-action') || $(b).parents('button:first').hasClass('medium-editor-action')) {
          return;
      }
      if (t.hasClass("medium-insert-customQuote-selected")) return i.not(t).removeClass("medium-insert-customQuote-selected"), $(".medium-insert-customQuote-toolbar, .medium-insert-customQuote-toolbar2").remove();
      i.removeClass("medium-insert-customQuote-selected"), $(".medium-insert-customQuote-toolbar, .medium-insert-customQuote-toolbar2").remove();
  },

  CustomAddon.prototype.removecustomQuote = function (e) {
      var t, i;
      8 !== e.which && 46 !== e.which || (t = this.$el.find(".medium-insert-customQuote-selected")).length && (e.preventDefault(), $(".medium-insert-customQuote-toolbar, .medium-insert-customQuote-toolbar2").remove(), i = $(this.templates["src/js/templates/core-empty-line.hbs"]().trim()), t.before(i), t.remove(), this.core.hideAddons(), this.core.moveCaret(i), this.core.triggerInput())
  },

  CustomAddon.prototype.addToolbar = function () {
      var e, t,
          i = this.$el.find(".medium-insert-customQuote-selected").closest(".medium-insert-customQuote"),
          s = !1,
          o = this.core.getEditor().options.elementsContainer || "body";

      $(o).append(editTemplate);
      $(o).append(removeButton);

      e = $(".medium-insert-customQuote-toolbar"), t = $(".medium-insert-customQuote-toolbar2"), e.find("button").each(function () {
          i.hasClass("medium-insert-customQuote-" + $(this).data("action")) && ($(this).addClass("medium-editor-button-active"), s = !0)
      }), !1 === s && e.find("button").first().addClass("medium-editor-button-active"), this.repositionToolbars(), e.fadeIn(), t.fadeIn()
  },

  CustomAddon.prototype.autoRepositionToolbars = function () {
      setTimeout(function () {
          this.repositionToolbars(), this.repositionToolbars()
      }.bind(this), 0)
  },

  CustomAddon.prototype.repositionToolbars = function () {
      var e = $(".medium-insert-customQuote-toolbar"),
          t = $(".medium-insert-customQuote-toolbar2"),
          i = this.$el.find(".medium-insert-customQuote-selected .customQuoteContent"),
          s = this.core.getEditor().options.elementsContainer,
          o = -1 < ["absolute", "fixed"].indexOf(window.getComputedStyle(s).getPropertyValue("position")),
          n = o ? s.getBoundingClientRect() : null,
          a = $(window).width(),
          r = {};
      if (typeof (i.offset()) == 'undefined') {
          return;
      }

      t.find('.medium-editor-action[data-action="edit"]').css({
          position: 'absolute',
          top: i.height() - 46,
          left: -(i.width() / 2 + 40)
      });
      t.length && (r.top = i.offset().top + 2, r.left = i.offset().left + i.width() - t.width() - 4, o && (r.top += s.scrollTop - n.top, r.left -= n.left, a = $(s).width()), r.left + t.width() > a && (r.left = a - t.width()), t.css(r)), e.length && (r.left = i.offset().left + i.width() / 2 - e.width() / 2, r.top = i.offset().top - e.height() - 8 - 2 - 5, o && (r.top += s.scrollTop - n.top, r.left -= n.left), r.top < 0 && (r.top = 0), e.css(r))
  },

  CustomAddon.prototype.toolbarAction = function (e) {
      var t = $(e.target).is("button") ? $(e.target) : $(e.target).closest("button"),
          i = (options.styles[t.data("action")] || {}).clicked;
      i && i(this.$el.find(".medium-insert-customQuote-selected")), this.core.triggerInput(), this.repositionToolbars()
  },

  /**
   * Toolbar2 actions
   * @param {*} e
   */
  CustomAddon.prototype.toolbar2Action = function (e) {
      var t = $(e.target).is("button") ? $(e.target) : $(e.target).closest("button"),
          i = (options.actions[t.data("action")] || {}).clicked;
      i && i(this.$el.find(".medium-insert-customQuote-selected")), this.core.triggerInput(), this.repositionToolbars()
  };

  /**
   * Event listeners
   *
   * @return {void}
   */

  CustomAddon.prototype.events = function () {
      $(document).on("click", $.proxy(this, "unselectcustomQuote"))
          .on("keydown", $.proxy(this, "removecustomQuote"))
          .on("click", ".medium-insert-customQuote-toolbar .medium-editor-action", $.proxy(this, "toolbarAction"))
          .on("click", ".medium-insert-customQuote-toolbar2 .medium-editor-action", $.proxy(this, "toolbar2Action")),
          this.$el.on("click", ".medium-insert-customQuote .customQuoteContent", $.proxy(this, "selectcustomQuote")),
          $(window).on("resize", $.proxy(this, "autoRepositionToolbars"));
  };

  /**
   * Get the Core object
   *
   * @return {object} Core object
   */
  CustomAddon.prototype.getCore = function () {
      return this.core;
  };

  /**
   * Add custom content
   *
   * This function is called when user click on the addon's icon
   *
   * @return {void}
   */

  CustomAddon.prototype.add = function (isEdit, dataEdit) {
      this.edit = typeof(isEdit) != 'undefined' && isEdit == true ? true : false;
      this.selectedElement = this.edit == true ? this.selectedElement : null;
      editor.saveSelection();
      var _el = this.$el, self = this, html;

      this.content = '';
      this.image = defaults.icons.defaultBrand;
      this.brandName = '';
      this.link = '';
      this.template = 1;
      this.uploadText = 'Upload';

      if(this.edit == true && Array.isArray(dataEdit)) {
          this.content = dataEdit[0];
          this.image = dataEdit[1];
          this.brandName = dataEdit[2];
          this.link = dataEdit[3];
          this.template = dataEdit[4];
          this.uploadText = 'Edit';
      }

      this._mainBlock = '<div class="lh-editor-popupForm" id="lhCustomQuote" data-key="">\n' +
          '                <span class="editorClosePopup">'+closeSvgIcon+'</span>\n' +
          '                <div class="headbar">\n' +
          '                    <div class="titlebar">Tạo quote</div>\n' +
          '                </div>\n' +
          '                <div class="customQuotenBox">\n' +
          '                    <div class="boxContent">\n' +
          '                        <textarea name="customQuoteContent" maxlength="350" placeholder="Nhập nội dung tại đây">'+this.content+'</textarea>\n' +
          '                        <div class="char-count"><span class="character-count">0</span>/350</div>\n' +
          '                    </div>\n' +
          '                    <div class="boxBrand customQuoteInfo">\n' +
          '                        <label for="brandName">THƯƠNG HIỆU, TÁC GIẢ</label>\n' +
          '                        <div class="iconBrandInsert">\n' +
          '                            <div id="mediaPostContent">\n' +
          '                                <span class="brandIcon">'+this.image+'</span>\n' +
          '                                <div class="edit-button" id="iconBrandUpload">'+this.uploadText+'</div>\n' +
          '                            </div>\n' +
          '                        </div>\n' +
          '                        <div class="inputCol">\n' +
          '                            <input type="text" name="brandName" id="brandName" value="'+this.brandName+'"/>\n' +
          '                            <span>Không bắt buộc</span>\n' +
          '                        </div>\n' +
          '                        <div class="clearfix"></div>\n' +
          '                    </div>\n' +
          '                    <div class="boxLinkAttach customQuoteInfo">\n' +
          '                        <label for="linkAttach">LINK ĐÍNH KÈM</label>\n' +
          '                        <div class="inputCol">\n' +
          '                            <input type="text" name="linkAttach" id="linkAttach" value="'+this.link+'"/>\n' +
          '                            <span>Không bắt buộc</span>\n' +
          '                        </div>\n' +
          '                        <div class="clearfix"></div>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="imageSubmitBox">\n' +
          '                    <button class="js-editorCustomQuoteSelectTemplate new-btn new-btn__width-icon new-btn__width-icon--blue">Tạo mới</button>\n' +
          '                </div>\n' +
          '            </div>\n' +
          '            <div class="lh-editor-popupForm" id="selectTemplatesForm" style="display: none">\n' +
          '                <div class="card lh-quote-card lh-quote-card-1" data-template="1" data-active="true">\n' +
          '                    <div class="card-body">\n' +
          '                        <h5 class="card-title"><img src="'+linkhay_url+'/resource/template/image/default/core/editor/medium/quote.svg"> </h5>\n' +
          '                        <p class="card-text"></p>\n' +
          '                        <div class="card-footer">\n' +
          '                            <div class="col-sm-6 p-0 pull-left">\n' +
          '                                    <span class="avatar"></span>\n' +
          '                                <span class="official"></span>\n' +
          '                            </div>\n' +
          '                            <div class="col-sm-6 p-0 pull-left text-right">\n' +
          '                                <span class="link-site"> <i class="fa fa-external-link" aria-hidden="true"></i></span>\n' +
          '                            </div>\n' +
          '                        </div>\n' +
          '                    </div>\n' +
          '                    <div class="lh-quote-footer-button">\n' +
          '                        <button class="js-editCustomQuoteContentBtn btn btn-default btn-edit-quote">Sửa nội dung quote</button>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="card lh-quote-card lh-quote-card-2" data-template="2" style="display: none;">\n' +
          '                    <div class="card-body">\n' +
          '                        <h5 class="lh-quote-card-icon">\n' +
          '                            <span class="lh-quote-card-bg"></span>\n' +
          '                        </h5>\n' +
          '                        <p class="card-text"></p>\n' +
          '                        <div class="card-footer">\n' +
          '                            <div class="col-sm-6 p-0 pull-left">\n' +
          '                                    <span class="avatar"></span>\n' +
          '                                <span class="official"></span>\n' +
          '                            </div>\n' +
          '                            <div class="col-sm-6 p-0 pull-left text-right">\n' +
          '                                <span class="link-site"> <i class="fa fa-external-link" aria-hidden="true"></i></span>\n' +
          '                            </div>\n' +
          '                        </div>\n' +
          '                    </div>\n' +
          '                    <div class="lh-quote-footer-button">\n' +
          '                        <button class="js-editCustomQuoteContentBtn btn btn-default btn-edit-quote">Sửa nội dung quote</button>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="card lh-quote-card lh-quote-card-3" data-template="3" style="display: none;">\n' +
          '                    <div class="card-body">\n' +
          '                        <h5 class="card-title">\n' +
          '                            <div class="col-sm-12 p-0 text-center">\n' +
          '                                <div class="avatar"></div>\n' +
          '                                <div class="official mt-3"></div>\n' +
          '                            </div>\n' +
          '                        </h5>\n' +
          '                        <p class="card-text">\n' +
          '                            <img src="'+linkhay_url+'/resource/template/image/default/core/editor/medium/quote.svg"></p>\n' +
          '                        <div class="card-footer">\n' +
          '                            <div class="col-sm-12 p-0 pull-left text-center">\n' +
          '                                <span class="link-site"> <i class="fa fa-external-link" aria-hidden="true"></i></span>\n' +
          '                            </div>\n' +
          '                        </div>\n' +
          '                    </div>\n' +
          '                    <div class="lh-quote-footer-button">\n' +
          '                        <button class="js-editCustomQuoteContentBtn btn btn-default btn-edit-quote">Sửa nội dung quote</button>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="card lh-quote-card lh-quote-card-4" data-template="4" style="display: none;">\n' +
          '                    <div class="card-body">\n' +
          '                        <p><img src="'+linkhay_url+'/resource/template/image/default/core/editor/medium/quote.svg"> </p>\n' +
          '                        <p class="card-text"></p>\n' +
          '                        <div class="card-footer">\n' +
          '                            <div class="col-sm-6 p-0 pull-left">\n' +
          '                                    <span class="avatar"></span>\n' +
          '                                <span class="official"></span>\n' +
          '                            </div>\n' +
          '                            <div class="col-sm-6 p-0 pull-left text-right">\n' +
          '                                <span class="link-site"> <i class="fa fa-external-link" aria-hidden="true"></i></span>\n' +
          '                            </div>\n' +
          '                        </div>\n' +
          '                    </div>\n' +
          '                    <div class="lh-quote-footer-button">\n' +
          '                        <button class="js-editCustomQuoteContentBtn btn btn-default btn-edit-quote">Sửa nội dung quote</button>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="card lh-quote-card lh-quote-card-5" data-template="5" style="display: none;">\n' +
          '                        <div class="card-body">\n' +
          '                            <h5 class="lh-quote-card-icon">\n' +
          '                                <span class="lh-quote-card-bg"></span>\n' +
          '                            </h5>\n' +
          '                            <p class="card-text"></p>\n' +
          '                            <div class="card-footer">\n' +
          '                                <div class="col-sm-6 p-0 pull-left">\n' +
          '                                        <span class="avatar"></span>\n' +
          '                                    <span class="official"></span>\n' +
          '                                </div>\n' +
          '                                <div class="col-sm-6 p-0 pull-left text-right">\n' +
          '                                    <span class="link-site"> <i class="fa fa-external-link" aria-hidden="true"></i></span>\n' +
          '                                </div>\n' +
          '                            </div>\n' +
          '                        </div>\n' +
          '                        <div class="lh-quote-footer-button">\n' +
          '                            <button class="js-editCustomQuoteContentBtn btn btn-default btn-edit-quote">Sửa nội dung quote</button>\n' +
          '                        </div>\n' +
          '                    </div>\n' +
          '            </div>\n'+
          '            <div class="imageSubmitBox lh-button-complete" style="display: none;">\n' +
          '                <button class="js-editorInsertcustomQuote new-btn new-btn__width-icon new-btn__width-icon--blue">Hoàn tất</button>\n' +
          '            </div>\n';

      this._sidebarBlock = '<h4 class="sidebar-title">Chọn kiểu hiển thị‹</h4>\n' +
          '        <div class="lh-editorSidebar-list">\n' +
          '            <div class="item">\n' +
          '                <div class="lh-editorSidebar-checkbox">\n' +
          '                    <div class="round">\n' +
          '                        <input type="checkbox" id="checkbox1" class="editorSelectTemplateCheckbox" value="1" checked="checked">\n' +
          '                        <label for="checkbox1"></label>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="lh-editorSidebar-content lh-quote-content type-1">\n' +
          '                </div>\n' +
          '            </div>\n' +
          '            <div class="item">\n' +
          '                <div class="lh-editorSidebar-checkbox">\n' +
          '                    <div class="round">\n' +
          '                        <input type="checkbox" id="checkbox2" class="editorSelectTemplateCheckbox" value="2">\n' +
          '                        <label for="checkbox2"></label>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="lh-editorSidebar-content lh-quote-content type-2">\n' +
          '                </div>\n' +
          '            </div>\n' +
          '            <div class="item">\n' +
          '                <div class="lh-editorSidebar-checkbox">\n' +
          '                    <div class="round">\n' +
          '                        <input type="checkbox" id="checkbox3" class="editorSelectTemplateCheckbox" value="3">\n' +
          '                        <label for="checkbox3"></label>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="lh-editorSidebar-content lh-quote-content type-3">\n' +
          '                </div>\n' +
          '            </div>\n' +
          '            <div class="item">\n' +
          '                <div class="lh-editorSidebar-checkbox">\n' +
          '                    <div class="round">\n' +
          '                        <input type="checkbox" id="checkbox4" class="editorSelectTemplateCheckbox" value="4">\n' +
          '                        <label for="checkbox4"></label>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="lh-editorSidebar-content lh-quote-content type-4">\n' +
          '                </div>\n' +
          '            </div>\n' +
          '            <div class="item">\n' +
          '                <div class="lh-editorSidebar-checkbox">\n' +
          '                    <div class="round">\n' +
          '                        <input type="checkbox" id="checkbox5" class="editorSelectTemplateCheckbox" value="5">\n' +
          '                        <label for="checkbox5"></label>\n' +
          '                    </div>\n' +
          '                </div>\n' +
          '                <div class="lh-editorSidebar-content lh-quote-content type-5">\n' +
          '                </div>\n' +
          '            </div>\n' +
          '        </div>';
      this._wrap = null;
      this._contentContainer = null;
      this._inputForm = null;
      this._selectTemplates = null;
      this.contentWrap = null;
      this.textarea = null;
      this._closePopup = null;

      this._setup = function (main, sidebar) {
          var win = null, self = this;

          win = $.mediumEditorTemplates({
              destroy_hook: function () {
                  //Remove html
              },
              title: '',
              popupOverlayBg: true,
              content: main,
              sidebar: sidebar
          });


          self._wrap = $('.lh-editor-wrap');
          self._contentContainer = self._wrap.find('.content-container');
          self._inputForm = self._wrap.find('#lhCustomQuote');
          self._selectTemplates = self._wrap.find('#selectTemplatesForm');
          self.contentWrap = $('#mediaPostContent');
          self._initAction();

      };

      this._replaceBrN = function (content, rev) {
          var regex = /<br\s*[\/]?>/gi;
          if(rev) {
              content = content.replace(regex, "\n");
          } else {
              regex = /(?:\r\n|\r|\n)/g;
              content = content.replace(regex, '<br>');
          }
          return content;
      };

      this._getHtml = function () {
          var self = this;
          var _html = self._selectTemplates.find('.lh-quote-card[data-active="true"]')[0].outerHTML;
          return '<div class="medium-insert-customQuote" contenteditable="false"><div class="customQuoteContent">'+_html+'</div></div>';
      };

      /**
       * Prepare upload process when user click on upload custom quote image
       */
      this._setupUploader = function(selector) {
          var that = this,
          $file = $(this.templates['src/js/templates/images-fileupload.hbs']()),
          fileUploadOptions = {
              dataType: 'json',
              add: function (e, data) {
                  $.proxy(that, 'uploadAdd', e, data)();
              },
              done: function (e, data) {
                  $.proxy(that, 'uploadDone', e, data, selector)();
              }
          };

        // Only add progress callbacks for browsers that support XHR2,
        // and test for XHR2 per:
        // http://stackoverflow.com/questions/6767887/
        // what-is-the-best-way-to-check-for-xhr2-file-upload-support
        if (new XMLHttpRequest().upload) {
            fileUploadOptions.progress = function (e, data) {
                $.proxy(that, 'uploadProgress', e, data)();
            };

            fileUploadOptions.progressall = function (e, data) {
                $.proxy(that, 'uploadProgressall', e, data)();
            };
        }

        $file.fileupload($.extend(true, {}, this.options.fileUploadOptions, fileUploadOptions));

        $file.click();
      }

      /**
       * Trigger when user choose file to begin uploading
       */
      CustomAddon.prototype.uploadAdd = function(e, data) {
        data.process().done(function() {
          data.submit();
        })
      }

      /**
       *  Trigger after user
       */
      CustomAddon.prototype.uploadDone = function(e, t, selector) {
        var imageUrl = t.result.files[0].url,
                    imageId = t.result.files[0].media_id,
                    _img_class = (t.result.files[0].height < t.result.files[0].width)?'scaleByHeight':'scaleByWidth',
                    img = $('<img/>').attr({'src': imageUrl,'data-source':'lh-user-media', 'data-lh-media-id': imageId, 'data-w': t.result.files[0].width, 'data-h': t.result.files[0].height}).addClass(_img_class);
                var uploadBox = selector.closest('.iconBrandInsert');
                uploadBox.find('.brandIcon').html(img);
                selector.text('Edit');
      }

      /**
     * Callback for global upload progress events
     * https://github.com/blueimp/jQuery-File-Upload/wiki/Options#progressall
     *
     * @param {Event} e
     * @param {object} data
     * @return {void}
     */

    CustomAddon.prototype.uploadProgressall = function (e, data) {
        var progress, $progressbar;

        if (this.options.preview === false) {
            progress = parseInt(data.loaded / data.total * 100, 10);
            $progressbar = this.$el.find('.medium-insert-active').find('progress');

            $progressbar
                .attr('value', progress)
                .text(progress);

            if (progress === 100) {
                $progressbar.remove();
            }
        }
    };

    /**
     * Callback for upload progress events.
     * https://github.com/blueimp/jQuery-File-Upload/wiki/Options#progress
     *
     * @param {Event} e
     * @param {object} data
     * @return {void}
     */

    CustomAddon.prototype.uploadProgress = function (e, data) {
        var progress, $progressbar;

        if (this.options.preview) {
            progress = 100 - parseInt(data.loaded / data.total * 100, 10);
            $progressbar = data.context.find('.medium-insert-images-progress');

            $progressbar.css('width', progress + '%');

            if (progress === 0) {
                $progressbar.remove();
            }
        }
    };

      /**
       * Reset scroll
       */
      this._resetScroll = function () {
          $('body').removeClass('disabled-scroll');
      };

      this._disabledScroll = function () {
          $('body').addClass('disabled-scroll');
      };

      this._initAction = function () {
          var self = this;
          self.textarea = self._wrap.find('textarea');
          self.textarea.on('keyup', function(){
              var _el = $(this);
              setTimeout(function(){
                  _el.css({'height':_el[0].scrollHeight + 'px'});
              },0);
          });

          self._closePopup = self._wrap.find('.editorClosePopup').click(function () {
              $('.lh-editor-wrap *').unbind();
              $('.addAnImage button').unbind();
              $('.lh-editor-wrap').remove();
              self._resetScroll();
          });

          self._wrap.find("#iconBrandUpload").on('click', function(){
              self._setupUploader($(this));
          });

          self._wrap.find('.js-editorCustomQuoteSelectTemplate').on('click', function () {
              var _content = self.textarea.val(),
                  _brandName = $("#brandName").val(),
                  _linkAttach = $("#linkAttach").val(),
                  _brandImage = $('.brandIcon').html();

              if(!_content) {
                  alert('Bạn chưa nhập nội dung quote');
                  return;
              }
              if($(_brandImage).find('img').length > 0) {
                  if(!_brandName) {
                      alert('Vui lòng nhập tên thương hiệu, tác giả');
                      return;
                  }
              }
              /* fill content */
              self._selectTemplates.find('.card-text').html(_content);
              if(_brandName) {
                  self._selectTemplates.find('.official').html(_brandName);
              }
              if(_linkAttach) {
                  self._selectTemplates.find('.link-site').html('<a href="'+_linkAttach+'">'+_linkAttach+'</a> <i class="fa fa-external-link" aria-hidden="true"></i>');
              }

                  self._selectTemplates.find('.avatar').html(_brandImage);

              /* Start animation */
              self._wrap.find('.lh-button-complete').show();
              self._wrap.find('.sidebar-container').css({'right':'0px'});
              self._contentContainer.addClass('sidebar-active').removeClass('overlay-bg');
              self._inputForm.hide();
              self._selectTemplates.show();
          });

          self._wrap.find('.js-editCustomQuoteContentBtn').on('click', function () {
              self._wrap.find('.lh-button-complete').hide();
              self._wrap.find('.sidebar-container').css({'right':'-333px'});
              self._contentContainer.removeClass('sidebar-active').addClass('overlay-bg');
              self._inputForm.show();
              self._selectTemplates.hide();
          });

          self._wrap.find(".editorSelectTemplateCheckbox").on('change',function() {
              if(this.checked) {
                  var id = $(this).val();
                  $('.editorSelectTemplateCheckbox').prop('checked', false);
                  $(this).prop('checked', true);
                  $('.lh-quote-card').removeClass('templateSelected').removeAttr('data-active').hide();
                  $('.lh-quote-card-' + id).addClass('templateSelected').attr('data-active', 'true').show();
              }
          });

          self._wrap.find('.js-editorInsertcustomQuote').on('click', function () {
              if (self.edit == true) {
                  $(self.selectedElement).html(html = self._getHtml());
              } else {
                  $('.medium-insert-active').before(self._getHtml());
              }

              self._closePopup.trigger('click');
          });

          if(self.edit == true) {
              self._wrap.find(".imageSubmitBox button").text('Update');
              self.textarea.css({'height':self.textarea[0].scrollHeight + 'px'});
              // self._listenerUploaderHasFile(true);
              self._wrap.find('#customQuoteEditorUpload').addClass('hasImage');
          }
      };

      this._setup(self._mainBlock, self._sidebarBlock);

      /**
       * hide button insert
       */
      try {
          this.$el.data('plugin_mediumInsert').hideButtons()
      } catch (e) {
      }
  };


  /** Addon initialization */

  $.fn[pluginName + addonName] = function (options) {
      return this.each(function () {
          if (!$.data(this, 'plugin_' + pluginName + addonName)) {
              $.data(this, 'plugin_' + pluginName + addonName, instance = new CustomAddon(this, options));
          }
      });
  };
})(jQuery, window, document);
