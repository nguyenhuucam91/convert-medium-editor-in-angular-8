(function($) {
    function LH_Editor_Templates(options) {
        this.render = function() {
            var self = this;

            this.win = $('<div />').addClass('lh-editor-wrap');
            $(document.body).append(this.win);

            this.title_bar = $('<div />').addClass('editor-header');
            this.win.append(this.title_bar);

            if(!options.skipCloseBtn) {
                this.close_btn = $('<span />').addClass('close-btn').append($('' + closeSvgIcon)).click(function(e){ self.destroy(e); });
                this.backBtn = $('<span />').addClass('back-btn').append($('' + backSvgIcon)).click(function (e) {self.destroy(e);});

                this.title_bar.append(this.backBtn);
            } else {

            }

            this.title_container = $('<h2 />').addClass('heading');

            this.title_bar.append(this.title_container);
            if(this.title) {
                this.title_container.html(this.title);
            }

            this.scene = $('<div />').addClass('editor-scene');
            this.contentContainer = $('<div />').addClass('content-container').appendTo(this.scene);
            if(this.popupOverlayBg) this.contentContainer.addClass('overlay-bg');
            if(this.content) {
                this.contentContainer.html(this.content);
            }
            this.win.append(this.scene);

            this.sidebarContainer = $('<div />').addClass('sidebar-container').appendTo(this.win);
            if(this.sidebar) {
                this.sidebarContainer.html(this.sidebar);
            }

            self._disabledScroll();

        };

        this._resetScroll = function () {
            $('body').removeClass('disabled-scroll');
        };

        this._disabledScroll = function () {
            $('body').addClass('disabled-scroll');
        };

        this.destroy = function(e, force) {
            var self = this;
            if(! force) {
                if(typeof this.destroy_hook === 'function') {
                    if(this.destroy_hook(e) === false) {
                        return false;
                    }
                }
            }

            var buff = [];
            var inst = null;

            for(var x = 0; x < $.lh_editor_template.length; x ++) {
                if(x !== this.index) {
                    inst = $.lh_editor_template[x];
                    inst.index = buff.length;
                    buff.push(inst);
                }
            }

            $.lh_editor_template = buff;

            this.win.remove();
            self._resetScroll();
        };

        if(typeof options !== 'object') {
            options = {};
        }

        options.index = $.lh_editor_template.length;

        this.index = null;
        this.destroy_hook = null;
        this.title = null;
        this.title_bar = null;
        this.close_btn = null;
        this.title_container = null;
        this.content = null;
        this.sidebar = null;
        this.win = null;
        this.scene = null;
        this.skipCloseBtn = null;
        this.backBtn = null;
        this.popupOverlayBg = null;

        $.extend(this, options);

        this.render();
    }

    $.lh_editor_template = [];

    $.mediumEditorTemplates = function(options) {
        var inst = new LH_Editor_Templates(options);
        $.lh_editor_template.push(inst);

        return inst;
    };
})(jQuery);
