/**
 * OSECORE
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU General Public License version 3
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@osecore.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade OSECORE to newer
 * versions in the future. If you wish to customize OSECORE for your
 * needs please refer to http://www.osecore.com for more information.
 *
 * @copyright	Copyright (C) 2011 by Sang Le Tuan (http://www.osecore.com). All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License version 3
 * @author      Sang Le Tuan - batsatla@gmail.com
 */
function osc_dragger_setter() {
  if(typeof jQuery == 'undefined' || typeof jQuery.OSC == 'undefined') {
      setTimeout(osc_dragger_setter, 100);
      return;
  }

  (function($){
      $.oscDraggerReady = true;

      function OSC_Dragger(bar, options) {
          this.lock = function() {
              this.locked = true;
          }

          this.unlock = function() {
              this.locked = false;
          }

          this.fire = function(e) {
              e.stopPropagation();

              if(this.locked) {
                  return true;
              }

              $.DRAGGER_CUR_INST = this;

              if(typeof this.callback.fire != 'undefined') {
                  this.callback.fire({
                      e : e,
                      inst : this
                  });
              }

              this._min_coords = {};
              this._max_coords = {};

              var docDim = $.getDocumentDim();

              var maxX = docDim.w - $(this.target).width();
              var maxY = docDim.h - $(this.target).height();

              if(this.min_coords == null) {
                  this._min_coords.x = 0;
                  this._min_coords.y = 0;
              } else {
                  this._min_coords = this.min_coords;
              }

              if(this.max_coords == null) {
                  this._max_coords.x = maxX;
                  this._max_coords.y = maxY;
              } else {
                  if(this.max_coords.x > maxX) {
                      this.max_coords.x = maxX;
                  }

                  if(this.max_coords.y > maxY) {
                      this.max_coords.y = maxY;
                  }

                  this._max_coords = this.max_coords;
              }

              var cursorCoords = {
                  x : e.pageX,
                  y : e.pageY
              };

              var targetCoords = $(this.target).offset();

              targetCoords = {
                  x : targetCoords.left,
                  y : targetCoords.top
              };

              this.coords_in_bar.x = cursorCoords.x - targetCoords.x;
              this.coords_in_bar.y = cursorCoords.y - targetCoords.y;

              if(this.hook_func.drag != null) {
                  $(document).unbind('mousemove', this.hook_func.drag).unbind('mouseup', this.hook_func.drop);
              }

              var self = this;

              this.hook_func.drag = function(e){
                  self.drag(e);
              };
              this.hook_func.drop = function(e){
                  self.drop(e);
              };

              $(document).bind('mousemove', this.hook_func.drag).bind('mouseup', this.hook_func.drop);
              $(document.body).addClass('dis-sel');

              return false;
          }

          this.drag = function(e) {
              var cursorCoords = {
                  x : e.pageX,
                  y : e.pageY
              };

              var x = cursorCoords.x - this.coords_in_bar.x;
              var y = cursorCoords.y - this.coords_in_bar.y;

              if(x < this._min_coords.x) {
                  x = this._min_coords.x;
              } else if(x > this._max_coords.x) {
                  x = this._max_coords.x;
              }

              if(y < this._min_coords.y) {
                  y = this._min_coords.y;
              } else if(y > this._max_coords.y) {
                  y = this._max_coords.y;
              }

              if(this.divergent) {
                  x += this.divergent.x;
                  y += this.divergent.y;
              }

              $(this.target).offset({
                  left : x,
                  top: y
              });

              this.coords = {
                  x : x,
                  y : y
              };

              if(typeof this.callback.drag != 'undefined') {
                  this.callback.drag({
                      e : e,
                      inst : this
                  });
              }
          }

          this.drop = function(e) {
              if(typeof this.callback.drop != 'undefined') {
                  this.callback.drop({
                      e : e,
                      inst : this
                  });
              }

              $(document).unbind('mousemove', this.hook_func.drag).unbind('mouseup', this.hook_func.drop);
              $(document.body).removeClass('dis-sel');

              this.hook_func.drag = null;
              this.hook_func.drop = null;

              $.DRAGGER_CUR_INST = null;
          }

          if(typeof bar != 'object') {
              bar = $('#' + bar)[0];
          }

          if(!options.target) {
              options.target = bar;
          } else if(typeof options.target != 'object') {
              options.target = $('#' + options.target)[0];
          }

          options.index = bar.id;
          options.bar = bar;

          this.index = null;
          this.bar = null;
          this.target = null;
          this.coords_in_bar = {
              x : 0,
              y : 0
          };
          this.coords = {
              x : 0,
              y : 0
          };
          this.min_coords = null;
          this.max_coords = null;
          this._min_coords = {};
          this._max_coords = {};
          this.divergent = null;
          this.callback = {};
          this.cursor = 'move';
          this.hook_func = {};
          this.locked = false;

          $.extend(this, options);

          var self = this;

          this.hook_func.fire = function(e){
              self.fire(e);
          };

          $(this.bar).bind('mousedown', this.hook_func.fire).css('cursor', this.cursor);
      }

      $.fn.osc_dragger = function() {
          var func = null;

          if(arguments.length > 0 && typeof arguments[0] == 'string') {
              func = arguments[0];
          }

          if(func) {
              var opts = [];

              for(var x = 1; x < arguments.length; x ++) {
                  opts.push(arguments[x]);
              }
          } else {
              opts = arguments[0];
          }

          return this.each(function() {
              if(func) {
                  var instance = $(this).data('osc-dragger');
                  instance[func].apply(instance, opts);
              } else {
                  $(this).data('osc-dragger', new OSC_Dragger(this, opts));
              }
          });
      };
  })(jQuery);
}

osc_dragger_setter();
