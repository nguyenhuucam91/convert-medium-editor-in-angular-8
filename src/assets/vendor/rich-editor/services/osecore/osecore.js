/**
 * OSECORE
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU General Public License version 3
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@osecore.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade OSECORE to newer
 * versions in the future. If you wish to customize OSECORE for your
 * needs please refer to http://www.osecore.com for more information.
 *
 * @copyright	Copyright (C) 2011 by Sang Le Tuan (http://www.osecore.com). All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License version 3
 * @author      Sang Le Tuan - batsatla@gmail.com
 */
function osc_core_setter() {
  if (typeof jQuery == 'undefined') {
      setTimeout(osc_core_setter, 100);
      return;
  }

  try {
      Object.defineProperty(HTMLMediaElement.prototype, 'isPlaying', {
          get: function () {
              return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
          }
      });
  } catch (e) {
  }

  Node.prototype.prepend = function (node) {
      if (this.firstChild) {
          this.insertBefore(node, this.firstChild);
      } else {
          this.appendChild(node);
      }

      return this;
  };

  Element.prototype.getParsedCssText = function (attr_names) {
      var parsed_css_text = {};

      if (typeof attr_names !== 'string') {
          attr_names = null;
      } else {
          attr_names = attr_names.replace(/[^\w-,]/g, '').split(',');
      }

      var found_counter = 0;

      var matches = this.style.cssText.match(/([\w-]+)\s*:\s*((?:[^;'"]+|"(?:[^"\\]|\\.)*"|'(?:[^'\\]|\\.)*')*)\s*;?/g);

      if (matches) {
          for (var i = 0; i < matches.length; i++) {
              var _matches = matches[i].match(/^([\w-]+)\s*:\s*(.+?)\;?$/);

              if (_matches && (!attr_names || attr_names.indexOf(_matches[1].toLowerCase()) >= 0)) {
                  parsed_css_text[_matches[1].toLowerCase()] = _matches[2];

                  found_counter++;
              }
          }
      }

      if (Array.isArray(attr_names) && attr_names.length === 1) {
          return found_counter < 1 ? null : parsed_css_text[attr_names[0]];
      }

      return parsed_css_text;
  };

  Array.prototype.unique = function () {
      var a = this.concat();

      for (var i = 0; i < a.length; ++i) {
          for (var j = i + 1; j < a.length; ++j) {
              if (a[i] === a[j]) {
                  a.splice(j--, 1);
              }
          }
      }

      return a;
  };

  String.prototype.nl2br = function () {
      return this.replace(/\n/g, '<br />');
  };

  String.prototype.br2nl = function () {
      return this.replace(/<br\s*\/?>/gi, "\n");
  };

  String.prototype.str_pad = function (length, pad_string) {
      if (!pad_string) {
          pad_string = '0';
      } else {
          pad_string = new String(pad_string);
      }

      length = parseInt(length);

      if (isNaN(length) || length < 1) {
          return this;
      }

      if (this.length >= length) {
          return this;
      }

      var pad_text = '';

      for (var i = this.length; i < length; i++) {
          pad_text += pad_string;
      }

      return pad_text + this;
  };

  jQuery.event.props.push('clipboardData');

  function OSC_Base64() {
      this.encode = function (input) {
          var output = '';
          var i = 0;
          var chr1, chr2, chr3, enc1, enc2, enc3, enc4;


          input = this._utf8_encode(input);

          while (i < input.length) {
              chr1 = input.charCodeAt(i++);
              chr2 = input.charCodeAt(i++);
              chr3 = input.charCodeAt(i++);

              enc1 = chr1 >> 2;
              enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
              enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
              enc4 = chr3 & 63;

              if (isNaN(chr2)) {
                  enc3 = enc4 = 64;
              } else if (isNaN(chr3)) {
                  enc4 = 64;
              }

              output += this._keyStr.charAt(enc1);
              output += this._keyStr.charAt(enc2);
              output += this._keyStr.charAt(enc3);
              output += this._keyStr.charAt(enc4);
          }

          return output;
      }

      this.decode = function (input) {
          var output = '';
          var i = 0;
          var chr1, chr2, chr3, enc1, enc2, enc3, enc4;

          input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

          while (i < input.length) {
              enc1 = this._keyStr.indexOf(input.charAt(i++));
              enc2 = this._keyStr.indexOf(input.charAt(i++));
              enc3 = this._keyStr.indexOf(input.charAt(i++));
              enc4 = this._keyStr.indexOf(input.charAt(i++));

              chr1 = (enc1 << 2) | (enc2 >> 4);
              chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
              chr3 = ((enc3 & 3) << 6) | enc4;

              output += String.fromCharCode(chr1);

              if (enc3 != 64) {
                  output += String.fromCharCode(chr2);
              }

              if (enc4 != 64) {
                  output += String.fromCharCode(chr3);
              }
          }

          output = this._utf8_decode(output);

          return output;
      }

      this._utf8_encode = function (string) {
          string = string.replace(/\r\n/g, "\n");
          var utftext = '';

          for (var i = 0; i < string.length; i++) {
              var c = string.charCodeAt(i);

              if (c < 128) {
                  utftext += String.fromCharCode(c);
              } else if ((c > 127) && (c < 2048)) {
                  utftext += String.fromCharCode((c >> 6) | 192);
                  utftext += String.fromCharCode((c & 63) | 128);
              } else {
                  utftext += String.fromCharCode((c >> 12) | 224);
                  utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                  utftext += String.fromCharCode((c & 63) | 128);
              }
          }

          return utftext;
      }

      this._utf8_decode = function (utftext) {
          var string = '';
          var i = 0;
          var c = 0;
          var c1 = 0;
          var c2 = 0;
          var c3;

          while (i < utftext.length) {
              c = utftext.charCodeAt(i);

              if (c < 128) {
                  string += String.fromCharCode(c);
                  i++;
              } else if ((c > 191) && (c < 224)) {
                  c2 = utftext.charCodeAt(i + 1);
                  string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                  i += 2;
              } else {
                  c2 = utftext.charCodeAt(i + 1);
                  c3 = utftext.charCodeAt(i + 2);
                  string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                  i += 3;
              }
          }

          return string;
      }

      this._keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  }

  var base64 = new OSC_Base64();

  var md5 =
          {
              encode: function (string)
              {
                  var a = 0x67452301;
                  var b = 0xEFCDAB89;
                  var c = 0x98BADCFE;
                  var d = 0x10325476;
                  var x = Array();
                  var S11 = 7;
                  var S12 = 12;
                  var S13 = 17;
                  var S14 = 22;
                  var S21 = 5;
                  var S22 = 9;
                  var S23 = 14;
                  var S24 = 20;
                  var S31 = 4;
                  var S32 = 11;
                  var S33 = 16;
                  var S34 = 23;
                  var S41 = 6;
                  var S42 = 10;
                  var S43 = 15
                  var S44 = 21;
                  var AA;
                  var BB;
                  var CC;
                  var DD;
                  var k;

                  string = this.Utf8Encode(string);

                  x = this.ConvertToWordArray(string);

                  for (k = 0; k < x.length; k += 16)
                  {
                      AA = a;
                      BB = b;
                      CC = c;
                      DD = d;

                      a = this.FF(a, b, c, d, x[ k + 0 ], S11, 0xD76AA478);
                      d = this.FF(d, a, b, c, x[ k + 1 ], S12, 0xE8C7B756);
                      c = this.FF(c, d, a, b, x[ k + 2 ], S13, 0x242070DB);
                      b = this.FF(b, c, d, a, x[ k + 3 ], S14, 0xC1BDCEEE);
                      a = this.FF(a, b, c, d, x[ k + 4 ], S11, 0xF57C0FAF);
                      d = this.FF(d, a, b, c, x[ k + 5 ], S12, 0x4787C62A);
                      c = this.FF(c, d, a, b, x[ k + 6 ], S13, 0xA8304613);
                      b = this.FF(b, c, d, a, x[ k + 7 ], S14, 0xFD469501);
                      a = this.FF(a, b, c, d, x[ k + 8 ], S11, 0x698098D8);
                      d = this.FF(d, a, b, c, x[ k + 9 ], S12, 0x8B44F7AF);
                      c = this.FF(c, d, a, b, x[ k + 10 ], S13, 0xFFFF5BB1);
                      b = this.FF(b, c, d, a, x[ k + 11 ], S14, 0x895CD7BE);
                      a = this.FF(a, b, c, d, x[ k + 12 ], S11, 0x6B901122);
                      d = this.FF(d, a, b, c, x[ k + 13 ], S12, 0xFD987193);
                      c = this.FF(c, d, a, b, x[ k + 14 ], S13, 0xA679438E);
                      b = this.FF(b, c, d, a, x[ k + 15 ], S14, 0x49B40821);

                      a = this.GG(a, b, c, d, x[ k + 1 ], S21, 0xF61E2562);
                      d = this.GG(d, a, b, c, x[ k + 6 ], S22, 0xC040B340);
                      c = this.GG(c, d, a, b, x[ k + 11 ], S23, 0x265E5A51);
                      b = this.GG(b, c, d, a, x[ k + 0 ], S24, 0xE9B6C7AA);
                      a = this.GG(a, b, c, d, x[ k + 5 ], S21, 0xD62F105D);
                      d = this.GG(d, a, b, c, x[ k + 10 ], S22, 0x2441453);
                      c = this.GG(c, d, a, b, x[ k + 15 ], S23, 0xD8A1E681);
                      b = this.GG(b, c, d, a, x[ k + 4 ], S24, 0xE7D3FBC8);
                      a = this.GG(a, b, c, d, x[ k + 9 ], S21, 0x21E1CDE6);
                      d = this.GG(d, a, b, c, x[ k + 14 ], S22, 0xC33707D6);
                      c = this.GG(c, d, a, b, x[ k + 3 ], S23, 0xF4D50D87);
                      b = this.GG(b, c, d, a, x[ k + 8 ], S24, 0x455A14ED);
                      a = this.GG(a, b, c, d, x[ k + 13 ], S21, 0xA9E3E905);
                      d = this.GG(d, a, b, c, x[ k + 2 ], S22, 0xFCEFA3F8);
                      c = this.GG(c, d, a, b, x[ k + 7 ], S23, 0x676F02D9);
                      b = this.GG(b, c, d, a, x[ k + 12 ], S24, 0x8D2A4C8A);

                      a = this.HH(a, b, c, d, x[ k + 5 ], S31, 0xFFFA3942);
                      d = this.HH(d, a, b, c, x[ k + 8 ], S32, 0x8771F681);
                      c = this.HH(c, d, a, b, x[ k + 11 ], S33, 0x6D9D6122);
                      b = this.HH(b, c, d, a, x[ k + 14 ], S34, 0xFDE5380C);
                      a = this.HH(a, b, c, d, x[ k + 1 ], S31, 0xA4BEEA44);
                      d = this.HH(d, a, b, c, x[ k + 4 ], S32, 0x4BDECFA9);
                      c = this.HH(c, d, a, b, x[ k + 7 ], S33, 0xF6BB4B60);
                      b = this.HH(b, c, d, a, x[ k + 10 ], S34, 0xBEBFBC70);
                      a = this.HH(a, b, c, d, x[ k + 13 ], S31, 0x289B7EC6);
                      d = this.HH(d, a, b, c, x[ k + 0 ], S32, 0xEAA127FA);
                      c = this.HH(c, d, a, b, x[ k + 3 ], S33, 0xD4EF3085);
                      b = this.HH(b, c, d, a, x[ k + 6 ], S34, 0x4881D05);
                      a = this.HH(a, b, c, d, x[ k + 9 ], S31, 0xD9D4D039);
                      d = this.HH(d, a, b, c, x[ k + 12 ], S32, 0xE6DB99E5);
                      c = this.HH(c, d, a, b, x[ k + 15 ], S33, 0x1FA27CF8);
                      b = this.HH(b, c, d, a, x[ k + 2 ], S34, 0xC4AC5665);

                      a = this.II(a, b, c, d, x[ k + 0 ], S41, 0xF4292244);
                      d = this.II(d, a, b, c, x[ k + 7 ], S42, 0x432AFF97);
                      c = this.II(c, d, a, b, x[ k + 14 ], S43, 0xAB9423A7);
                      b = this.II(b, c, d, a, x[ k + 5 ], S44, 0xFC93A039);
                      a = this.II(a, b, c, d, x[ k + 12 ], S41, 0x655B59C3);
                      d = this.II(d, a, b, c, x[ k + 3 ], S42, 0x8F0CCC92);
                      c = this.II(c, d, a, b, x[ k + 10 ], S43, 0xFFEFF47D);
                      b = this.II(b, c, d, a, x[ k + 1 ], S44, 0x85845DD1);
                      a = this.II(a, b, c, d, x[ k + 8 ], S41, 0x6FA87E4F);
                      d = this.II(d, a, b, c, x[ k + 15 ], S42, 0xFE2CE6E0);
                      c = this.II(c, d, a, b, x[ k + 6 ], S43, 0xA3014314);
                      b = this.II(b, c, d, a, x[ k + 13 ], S44, 0x4E0811A1);
                      a = this.II(a, b, c, d, x[ k + 4 ], S41, 0xF7537E82);
                      d = this.II(d, a, b, c, x[ k + 11 ], S42, 0xBD3AF235);
                      c = this.II(c, d, a, b, x[ k + 2 ], S43, 0x2AD7D2BB);
                      b = this.II(b, c, d, a, x[ k + 9 ], S44, 0xEB86D391);

                      a = this.AddUnsigned(a, AA);
                      b = this.AddUnsigned(b, BB);
                      c = this.AddUnsigned(c, CC);
                      d = this.AddUnsigned(d, DD);
                  }

                  var temp = this.WordToHex(a) + this.WordToHex(b) + this.WordToHex(c) + this.WordToHex(d);

                  return temp.toLowerCase();
              },
              RotateLeft: function (lValue, iShiftBits)
              {
                  return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
              },
              AddUnsigned: function (lX, lY)
              {
                  var lX4 = (lX & 0x40000000);
                  var lY4 = (lY & 0x40000000);
                  var lX8 = (lX & 0x80000000);
                  var lY8 = (lY & 0x80000000);
                  var lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);

                  if (lX4 & lY4)
                  {
                      return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
                  }

                  if (lX4 | lY4)
                  {
                      if (lResult & 0x40000000)
                      {
                          return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                      } else
                      {
                          return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                      }
                  } else
                  {
                      return (lResult ^ lX8 ^ lY8);
                  }
              },
              F: function (x, y, z) {
                  return (x & y) | ((~x) & z);
              },
              G: function (x, y, z) {
                  return (x & z) | (y & (~z));
              },
              H: function (x, y, z) {
                  return (x ^ y ^ z);
              },
              I: function (x, y, z) {
                  return (y ^ (x | (~z)));
              },
              FF: function (a, b, c, d, x, s, ac)
              {
                  a = this.AddUnsigned(a, this.AddUnsigned(this.AddUnsigned(this.F(b, c, d), x), ac));
                  return this.AddUnsigned(this.RotateLeft(a, s), b);
              },
              GG: function (a, b, c, d, x, s, ac)
              {
                  a = this.AddUnsigned(a, this.AddUnsigned(this.AddUnsigned(this.G(b, c, d), x), ac));
                  return this.AddUnsigned(this.RotateLeft(a, s), b);
              },
              HH: function (a, b, c, d, x, s, ac)
              {
                  a = this.AddUnsigned(a, this.AddUnsigned(this.AddUnsigned(this.H(b, c, d), x), ac));
                  return this.AddUnsigned(this.RotateLeft(a, s), b);
              },
              II: function (a, b, c, d, x, s, ac)
              {
                  a = this.AddUnsigned(a, this.AddUnsigned(this.AddUnsigned(this.I(b, c, d), x), ac));
                  return this.AddUnsigned(this.RotateLeft(a, s), b);
              },
              ConvertToWordArray: function (string)
              {
                  var lWordCount;
                  var lMessageLength = string.length;
                  var lNumberOfWords_temp1 = lMessageLength + 8;
                  var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
                  var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
                  var lWordArray = Array(lNumberOfWords - 1);
                  var lBytePosition = 0;
                  var lByteCount = 0;

                  while (lByteCount < lMessageLength)
                  {
                      lWordCount = (lByteCount - (lByteCount % 4)) / 4;
                      lBytePosition = (lByteCount % 4) * 8;
                      lWordArray[ lWordCount ] = (lWordArray[ lWordCount ] | (string.charCodeAt(lByteCount) << lBytePosition));
                      lByteCount++;
                  }

                  lWordCount = (lByteCount - (lByteCount % 4)) / 4;
                  lBytePosition = (lByteCount % 4) * 8;
                  lWordArray[ lWordCount ] = lWordArray[ lWordCount ] | (0x80 << lBytePosition);
                  lWordArray[ lNumberOfWords - 2 ] = lMessageLength << 3;
                  lWordArray[ lNumberOfWords - 1 ] = lMessageLength >>> 29;

                  return lWordArray;
              },
              WordToHex: function (lValue)
              {
                  var WordToHexValue = '';
                  var WordToHexValue_temp = '';
                  var lByte;
                  var lCount;

                  for (lCount = 0; lCount <= 3; lCount++)
                  {
                      lByte = (lValue >>> (lCount * 8)) & 255;
                      WordToHexValue_temp = '0' + lByte.toString(16);
                      WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
                  }

                  return WordToHexValue;
              },
              Utf8Encode: function (string)
              {
                  string = string.replace(/\r\n/g, "\n");
                  var utftext = '';

                  for (var n = 0; n < string.length; n++)
                  {
                      var c = string.charCodeAt(n);

                      if (c < 128)
                      {
                          utftext += String.fromCharCode(c);
                      } else if ((c > 127) && (c < 2048))
                      {
                          utftext += String.fromCharCode((c >> 6) | 192);
                          utftext += String.fromCharCode((c & 63) | 128);
                      } else
                      {
                          utftext += String.fromCharCode((c >> 12) | 224);
                          utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                          utftext += String.fromCharCode((c & 63) | 128);
                      }
                  }

                  return utftext;
              }
          };

  (function ($) {
      var ICON_MAP = null;

      window.configOSCIcon = function () {
          if (ICON_MAP === null) {
              ICON_MAP = true;
              $(document).ready(function () {
                  __loadSVGIconSprites();
              });
              return;
          } else if (ICON_MAP === true) {
              return;
          }

          var name = this.getAttribute('data-icon').replace(/^osc-/, '');

          if (typeof ICON_MAP[name] !== 'undefined') {
              this.setAttribute('viewBox', ICON_MAP[name]);
          }

          var marker = $('<span />').insertBefore(this);
          $(this).detach().insertBefore(marker);
          marker.remove();
      };

      function __loadSVGIconSprites() {
          $.ajax({
              type: 'get',
              url: OSC_TEMPLATE_SPRITES_URL,
              crossDomain: true,
              xhrFields: {withCredentials: true},
              headers: {
                  'X-Requested-With': 'XMLHttpRequest',
                  'X-OSC-Cross-Request': 'OK'
              },
              success: function (response) {
                  var symbols = response.documentElement.getElementsByTagName('symbol');

                  ICON_MAP = {};

                  for (var i = 0; i < symbols.length; i++) {
                      var symbol = symbols[i];
                      ICON_MAP[symbol.getAttribute('id')] = symbol.getAttribute('viewBox');
                      symbol.setAttribute('id', 'osc-' + symbol.getAttribute('id'));
                  }

                  var res_container = document.createElement("div");
                  document.body.insertBefore(res_container, document.body.firstChild);
                  res_container.innerHTML = new XMLSerializer().serializeToString(response.documentElement);

                  var icons = document.body.querySelectorAll('svg[data-icon^="osc-"]');

                  if (icons.length > 0) {
                      for (var i = 0; i < icons.length; i++) {
                          configOSCIcon.apply(icons[i]);
                      }
                  }
              }
          });
      }

      $.extend($, {
          OSC: true,
          md5: function (text) {
              return md5.encode(text);
          },
          renderIcon: function (name) {
              var viewBox = '0 0 512 512';

              if (ICON_MAP === null) {
                  ICON_MAP = true;
                  $(document).ready(function () {
                      __loadSVGIconSprites();
                  });
              } else if (ICON_MAP !== true && typeof ICON_MAP[name] !== 'undefined') {
                  viewBox = ICON_MAP[name];
              }
              ;

              var icon = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
              icon.setAttribute('data-icon', 'osc-' + name);
              icon.setAttribute('viewBox', viewBox);
              var use = document.createElementNS('http://www.w3.org/2000/svg', 'use');
              use.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", '#osc-' + name);
              icon.appendChild(use);

              return icon;
          },
          round: function (value, precision, mode) {
              var m, f, isHalf, sgn;
              precision |= 0;
              m = Math.pow(10, precision);
              value *= m;
              sgn = (value > 0) | -(value < 0);
              isHalf = value % 1 === 0.5 * sgn;
              f = Math.floor(value);

              if (isHalf) {
                  switch (mode) {
                      case 'PHP_ROUND_HALF_DOWN':
                          value = f + (sgn < 0); // rounds .5 toward zero
                          break;
                      case 'PHP_ROUND_HALF_EVEN':
                          value = f + (f % 2 * sgn); // rouds .5 towards the next even integer
                          break;
                      case 'PHP_ROUND_HALF_ODD':
                          value = f + !(f % 2); // rounds .5 towards the next odd integer
                          break;
                      default:
                          value = f + (sgn > 0); // rounds .5 away from zero
                  }
              }

              return (isHalf ? value : Math.round(value)) / m;
          },
          makeUniqid: function (strongest) {
              var length = 10;

              if (strongest) {
                  length = 15;
              }

              var timestamp = +new Date;

              var ts = timestamp.toString();
              var parts = ts.split("").reverse();
              var id = "";

              for (var i = 0; i < length; ++i) {
                  var index = $.rand(0, parts.length - 1);
                  id += parts[index];
              }

              return id;
          },
          rand: function (min, max) {
              return Math.floor(Math.random() * (max - min + 1)) + min;
          },
          calculateNewDim: function (w, h, mw, mh) {
              mw = parseInt(mw);
              mh = parseInt(mh);

              if (isNaN(mw) || mw < 1) {
                  mw = 0;
              }

              if (isNaN(mh) || mh < 1) {
                  mh = 0;
              }

              if (mw < 1 && mh < 1) {
                  return {w: w, h: h};
              }

              if (mw > 0 && mw < w) {
                  h = h / (w / mw);
                  w = mw;
              }

              if (mh > 0 && mh < h) {
                  w = w / (h / mh);
                  h = mh;
              }

              return {w: w, h: h};
          },
          base64Encode: function (text) {
              return base64.encode(text);
          },
          base64Decode: function (text) {
              return base64.decode(text);
          },
          cleanVNMask: function (text) {
              var search_arr = [['á', 'à', 'ả', 'ạ', 'ã', 'â', 'ấ', 'ầ', 'ẩ', 'ậ', 'ẫ', 'ă', 'ắ', 'ằ', 'ẳ', 'ặ', 'ẵ'],
                  ['á', 'À', 'Ả', 'Ạ', 'Ã', 'Â', 'Ấ', 'Ầ', 'Ẩ', 'Ậ', 'Ẫ', 'Ă', 'Ắ', 'Ằ', 'Ẳ', 'Ặ', 'Ẵ'],
                  ['ó', 'ò', 'ỏ', 'ọ', 'õ', 'ô', 'ố', 'ồ', 'ổ', 'ộ', 'ỗ', 'ơ', 'ớ', 'ờ', 'ở', 'ợ', 'ỡ'],
                  ['Ó', 'Ò', 'Ỏ', 'Ọ', 'Õ', 'Ô', 'Ố', 'Ồ', 'Ổ', 'Ộ', 'Ỗ', 'Ơ', 'Ớ', 'Ờ', 'Ở', 'Ợ', 'Ỡ'],
                  ['é', 'è', 'ẻ', 'ẹ', 'ẽ', 'ê', 'ế', 'ề', 'ể', 'ệ', 'ễ'],
                  ['É', 'È', 'Ẻ', 'Ẹ', 'Ẽ', 'Ê', 'Ế', 'Ề', 'Ể', 'Ệ', 'Ễ'],
                  ['ú', 'ù', 'ủ', 'ụ', 'ũ', 'ư', 'ứ', 'ừ', 'ử', 'ự', 'ữ'],
                  ['Ú', 'Ù', 'Ủ', 'Ụ', 'Ũ', 'Ư', 'Ứ', 'Ừ', 'Ử', 'Ự', 'Ữ'],
                  ['í', 'ì', 'ỉ', 'ị', 'ĩ'],
                  ['í', 'Ì', 'Ỉ', 'Ị', 'Ĩ'],
                  ['ý', 'ỳ', 'ỷ', 'ỵ', 'ỹ'],
                  ['Ý', 'Ỳ', 'Ỷ', 'Ỵ', 'Ỹ'],
                  ['đ'],
                  ['Đ']];

              var replace_arr = ['a', 'A', 'o', 'O', 'e', 'E', 'u', 'U', 'i', 'I', 'y', 'Y', 'd', 'D'];

              for (var k = 0; k < search_arr.length; k++) {
                  eval("text = text.replace(/(" + search_arr[k].join('|') + ")/g, replace_arr[k]);");
              }

              return text;
          },
          str_pad: function (text, length, padstring) {
              text = new String(text);
              padstring = new String(padstring);

              if (text.length < length) {
                  var padtext = new String(padstring);

                  while (padtext.length < (length - text.length)) {
                      padtext += padstring;
                  }

                  text = padtext.substr(0, length - text.length) + text;
              }

              return text;
          },
          mb_strlen: function (str) {
              return ($.browser.msie && str.indexOf('\n') != -1) ? str.replace(/\r?\n/g, '_').length : str.length;
          },
          strip_tags: function (txt) {
              return txt.toString().replace(/<\S[^><]*>/g, "");
          },
          htmlspecialchars: function (str) {
              var f = [
                  $.browser.mac && $.browser.msie ? new RegExp('&', 'g') : new RegExp('&(?!#[0-9]+;)', 'g'),
                  new RegExp('<', 'g'),
                  new RegExp('>', 'g'),
                  new RegExp('"', 'g')
              ];

              var r = ['&amp;', '&lt;', '&gt;', '&quot;'];

              for (var i = 0; i < f.length; i++) {
                  str = str.replace(f[i], r[i]);
              }

              return str;
          },
          validateEmbedCode: function (code) {
              if (code.indexOf("<script") >= 0) {
                  return false;
              }

              var patts = [
                  /<object[^>]*>(.*?)<\/object>/i,
                  /<object[^>]*>/i,
                  /<embed[^>]*>(.*?)<\/embed>/i,
                  /<embed[^>]*>/i
              ];

              for (var x = 0; x < patts.length; x++) {
                  if (patts[x].test(code)) {
                      return true;
                  }
              }

              return false;
          },
          parseEmbedTag: function (node) {
              var node_obj = $(node);

              var media_attrs = {
                  width: node_obj.attr('width'),
                  height: node_obj.attr('height'),
                  src: node_obj.attr('src'),
                  flashvars: node_obj.attr('flashvars'),
                  name: node_obj.attr('name'),
                  id: node_obj.attr('id')
              };

              var errors = 0;

              for (var x in media_attrs) {
                  if (!media_attrs[x] && x != 'flashvars' && x != 'name' && x != 'id') {
                      return false;
                  }
              }

              return media_attrs;
          },
          parseObjectTag: function (node) {
              var node_obj = $(node);

              var media_attrs = {
                  width: node_obj.attr('width'),
                  height: node_obj.attr('height'),
                  src: node_obj.attr('data'),
                  flashvars: node_obj.attr('flashvars'),
                  name: node_obj.attr('name'),
                  id: node_obj.attr('id')
              };

              for (var x in media_attrs) {
                  if (!media_attrs[x] && x != 'name' && x != 'id') {
                      var attr_keys = {};
                      attr_keys[x] = 1;

                      if (x == 'src') {
                          attr_keys['movie'] = 1;
                      }

                      media_attrs[x] = $._getObjectParamTagValue(node, attr_keys);
                  }
              }

              var embed = node.getElementsByTagName('embed')[0];
              var embed_obj = false;

              if (embed) {
                  embed_obj = $(embed);
              }

              var errors = 0;

              for (var x in media_attrs) {
                  if (!media_attrs[x] && x != 'name' && x != 'id') {
                      errors++;

                      if (embed_obj) {
                          media_attrs[x] = embed_obj.attr(x);

                          if (media_attrs[x] || x == 'flashvars') {
                              errors--;
                          }
                      } else if (x == 'flashvars') {
                          errors--;
                      }
                  }
              }

              if (errors > 0) {
                  return false;
              }

              return media_attrs;
          },
          _getObjectParamTagValue: function (node, attrs) {
              var params = node.getElementsByTagName('param');

              for (var x = 0; x < params.length; x++) {
                  var paramObj = $(params[x]);

                  if (attrs[paramObj.attr('name').toString().toLowerCase()]) {
                      return paramObj.attr('value');
                  }
              }

              return null;
          },
          serializeStyle: function (o) {
              var s = '';

              $.each(o, function (k, v) {
                  if (k && v) {
                      if ($.browser.mozilla && k.indexOf('-moz-') === 0)
                          return;

                      switch (k) {
                          case 'color':
                          case 'background-color':
                              v = v.toLowerCase();
                              break;
                      }

                      s += (s ? ' ' : '') + k + ': ' + v + ';';
                  }
              });

              return s;
          },
          parseStyle: function (st) {
              var o = {};

              if (!st) {
                  return o;
              }

              function compress(p, s, ot) {
                  var t, r, b, l;

                  t = o[p + '-top' + s];

                  if (!t)
                      return;

                  r = o[p + '-right' + s];

                  if (t != r)
                      return;

                  b = o[p + '-bottom' + s];

                  if (r != b)
                      return;

                  l = o[p + '-left' + s];

                  if (b != l)
                      return;

                  o[ot] = l;

                  delete o[p + '-top' + s];
                  delete o[p + '-right' + s];
                  delete o[p + '-bottom' + s];
                  delete o[p + '-left' + s];
              }

              function compress2(ta, a, b, c) {
                  var t;

                  t = o[a];

                  if (!t)
                      return;

                  t = o[b];

                  if (!t)
                      return;

                  t = o[c];

                  if (!t)
                      return;

                  o[ta] = o[a] + ' ' + o[b] + ' ' + o[c];

                  delete o[a];
                  delete o[b];
                  delete o[c];
              }
              ;

              st = st.replace(/&(#?[a-z0-9]+);/g, '&$1_MCE_SEMI_'); // Protect entities

              $.each(st.split(';'), function (k, v) {
                  var sv, ur = [];

                  if (v) {
                      v = v.replace(/_MCE_SEMI_/g, ';'); // Restore entities
                      v = v.replace(/url\([^\)]+\)/g, function (v) {
                          ur.push(v);
                          return 'url(' + ur.length + ')'
                      });
                      v = v.split(':');
                      sv = $.trim(v[1]);
                      sv = sv.replace(/url\(([^\)]+)\)/g, function (a, b) {
                          return ur[parseInt(b) - 1]
                      });
                      sv = sv.replace(/(rgb\([^\)]+\))/g, function (a, b) {
                          return $.rgbToColor(b);
                      });

                      o[$.trim(v[0]).toLowerCase()] = sv;
                  }
              });

              compress("border", "", "border");
              compress("border", "-width", "border-width");
              compress("border", "-color", "border-color");
              compress("border", "-style", "border-style");
              compress("padding", "", "padding");
              compress("margin", "", "margin");
              compress2('border', 'border-width', 'border-style', 'border-color');

              if ($.browser.msie) {
                  if (o.border == 'medium none') {
                      o.border = '';
                  }
              }

              return o;
          },
          rgbToColor: function (forecolor) {
              if (forecolor == '' || forecolor == null) {
                  return '';
              }

              var matches = forecolor.match(/^rgb\s*\(([0-9]+),\s*([0-9]+),\s*([0-9]+)\)$/i);

              if (matches) {
                  return $.rgbhexToColor((matches[1] & 0xFF).toString(16), (matches[2] & 0xFF).toString(16), (matches[3] & 0xFF).toString(16));
              } else {
                  return $.rgbhexToColor((forecolor & 0xFF).toString(16), ((forecolor >> 8) & 0xFF).toString(16), ((forecolor >> 16) & 0xFF).toString(16));
              }
          },
          rgbhexToColor: function (r, g, b) {
              return '#' + ($.str_pad(r, 2, 0) + $.str_pad(g, 2, 0) + $.str_pad(b, 2, 0));
          },
          TXT2XML: function (TXT) {
              try {
                  XML = new ActiveXObject("Microsoft.XMLDOM");
                  XML.async = "false";
                  XML.loadXML(TXT);
              } catch (e) {
                  try {
                      parser = new DOMParser();
                      XML = parser.parseFromString(TXT, "text/xml");
                  } catch (e) {
                  }
              }

              return XML;
          },
          XML2TXT: function (XML) {
              try {
                  TXT = XML.xml;
              } catch (e) {
                  try {
                      TXT = (new XMLSerializer()).serializeToString(XML);
                  } catch (e) {
                  }
              }

              return TXT;
          },
          createElement: function (tag, attribs, styles, parent) {
              var el = document.createElement(tag);

              var obj = $(el);

              if (attribs) {
                  obj.prop(attribs);
              }

              if (styles) {
                  obj.css(styles);
              }

              if (parent) {
                  parent.appendChild(el);
              }

              return el;
          },
          validate: function (data) {
              var result, field, type, args, errors = 0;

              for (var x in data) {
                  field = $('#' + x);

                  if (field.length < 1) {
                      continue;
                  }

                  if (typeof data[x] == 'function') {
                      result = data[x](field);
                  } else {
                      args = {};

                      if (typeof data[x] == 'string') {
                          type = '_' + data[x].replace(/^([a-zA-Z0-9_]+)(:.*)?$/, '$1');

                          if ((/:/).test(data[x])) {
                              args = data[x].replace(/^([a-zA-Z0-9_]+):(.*)?$/, '$2').split(':');
                          }
                      }

                      result = this._validator[type](field, args);
                  }

                  if (result != 'OK') {
                      errors++;

                      $('#' + x + '__err').show().each(function () {
                          this.innerHTML = result;
                      });

                      field.addClass('error');
                  } else {
                      $('#' + x + '__err').hide();
                      field.removeClass('error');
                  }
              }

              return errors < 1;
          },
          _validator: {
              _selector: function (field, arguments) {
                  var totalOptSelected = 0;

                  for (var x = 0; x < field.options.length; x++) {
                      if (!field.options[x].selected) {
                          continue;
                      }

                      if (field.options[x].value == '') {
                          continue;
                      }

                      totalOptSelected++;
                  }

                  return totalOptSelected > 0 ? true : 'You must select least one option';
              },
              _string: function (field, arguments) {
                  var error = '';

                  var counter = 0;

                  field.each(function () {
                      if (this.tagName == 'INPUT') {
                          this.value = $.trim(this.value);

                          if (this.value == '') {
                              error = 'The field is must enter value';
                          }
                      } else if (this.tagName == 'SELECT') {
                          var result = $._validator._selector(this, arguments);

                          if (result !== true) {
                              error = result;
                          }
                      }

                      counter++;
                  });

                  return error == '' ? 'OK' : error;
              },
              _number: function (field, arguments) {
                  var error = '';

                  var counter = 0;

                  field.each(function () {
                      if (this.tagName == 'INPUT') {
                          this.value = $.trim(this.value);

                          if (this.value == '') {
                              error = 'The field is must enter value';
                          } else {
                              this.value = arguments.isFloat ? parseFloat(this.value) : parseInt(this.value);

                              if (isNaN(this.value)) {
                                  error = 'ERR: The value is not number';
                              } else if (arguments.min && this.value < arguments.min) {
                                  error = 'ERR: The value is less than ' + arguments.min;
                              } else if (arguments.max && this.value > arguments.max) {
                                  error = 'ERR: The value is greater than ' + arguments.max;
                              }
                          }
                      } else if (this.tagName == 'SELECT') {
                          var result = $._validator._selector(this, arguments);

                          if (result !== true) {
                              error = result;
                          }
                      }

                      counter++;
                  });

                  return error == '' ? 'OK' : error;
              }
          },
          confirmAction: function (message, url) {
              var confirmVar = confirm(message);

              if (typeof url == 'undefined') {
                  return confirmVar;
              }

              if (confirmVar) {
                  window.location = url;
              }
          },
          renderPagination: function (page_size, cur_page, total_items, section, callback) {
              page_size = parseInt(page_size);
              cur_page = parseInt(cur_page);
              total_items = parseInt(total_items);
              section = parseInt(section);

              var total_page = Math.ceil(total_items / page_size);

              cur_page = cur_page >= 1 ? cur_page : 1;

              section = section > 0 ? section : 7;

              if (total_page < 2) {
                  return null;
              }

              var pager = {
                  pages: []
              };

              var start = cur_page - section;

              if (start <= 1) {
                  start = 1;
              } else if (start > total_page) {
                  start = total_page;
              } else if (total_page > section * 2 + 1) {
                  pager.last = 1;
              }

              var end = start + section * 2;

              if (end >= total_page) {
                  end = total_page;

                  start = end - section * 2;

                  if (start < 1) {
                      start = 1;
                  }
              } else {
                  pager.first = total_page;
              }

              for (var page = start; page <= end; page++) {
                  pager.pages.push(page);
              }

              var next = start - section - 1;
              var previous = end + section + 1;

              if (next > (section + 1)) {
                  pager.next = next;
              }

              if (previous < (total_page - section)) {
                  pager.previous = previous;
              }

              var page_item = null;
              var pagination = $('<ul />').addClass('pagination small');

              if (typeof pager.last !== 'undefined') {
                  page_item = $('<a />').prop({href: 'javascript:void(0)'});
                  page_item.append($('<i />').addClass('fa fa-angle-double-left'));

                  callback(page_item, pager.last);

                  pagination.append($('<li />').append(page_item));
              }

              if (typeof pager.next !== 'undefined') {
                  page_item = $('<a />').prop({href: 'javascript:void(0)'});
                  page_item.append($('<i />').addClass('fa fa-angle-left'));

                  callback(page_item, pager.next);

                  pagination.append($('<li />').append(page_item));
              }

              for (var i = 0; i < pager.pages.length; i++) {
                  if (pager.pages[i] === cur_page) {
                      page_item = $('<div />');
                  } else {
                      page_item = $('<a />').prop({href: 'javascript:void(0)'});
                      callback(page_item, pager.pages[i]);
                  }

                  page_item.html(pager.pages[i]);

                  pagination.append($('<li />').addClass(pager.pages[i] === cur_page ? 'current' : '').append(page_item));
              }

              if (typeof pager.previous !== 'undefined') {
                  page_item = $('<a />').prop({href: 'javascript:void(0)'});
                  page_item.append($('<i />').addClass('fa fa-angle-right'));

                  callback(page_item, pager.previous);

                  pagination.append($('<li />').append(page_item));
              }

              if (typeof pager.first !== 'undefined') {
                  page_item = $('<a />').prop({href: 'javascript:void(0)'});
                  page_item.append($('<i />').addClass('fa fa-angle-double-right'));

                  callback(page_item, pager.first);

                  pagination.append($('<li />').append(page_item));
              }

              return pagination;
          },
          _content_wrapped: {},
          wrapContent: function (content, opts) {
              var config = {
                  key: null,
                  close_callback: null
              };

              $.extend(config, opts);

              if (typeof config.key !== 'string') {
                  config.key = '';
              } else {
                  config.key = config.key.replace(/[^a-zA-Z0-9_]/g, '');
              }

              if (!config.key) {
                  config.key = $.makeUniqid();
              }

              $(document.body).addClass('osc-wrap-helper-page-unscrollable');

              var unclickable_helper = $('.osc-wrap-helper-page-unclickable');

              if (!unclickable_helper[0]) {
                  unclickable_helper = $('<div />').addClass('osc-wrap-helper-page-unclickable').appendTo(document.body);
              }

              unclickable_helper.swapZIndex();

              if (typeof $._content_wrapped[config.key] !== 'undefined') {
                  var wrap = $._content_wrapped[config.key].wrap;
                  var container = $._content_wrapped[config.key].container;
              } else {
                  var wrap = $('<div />').addClass('osc-wrap').attr('data-scroller', 1).appendTo(document.body).fixScrollable();
                  var container = $('<div />').attr('wrap-key', config.key).addClass('osc-wrap-container').appendTo(wrap);

                  container.mousedown(function (e) {
                      if (e.target === this) {
                          $.unwrapContent(config.key);
                      }
                  }).bind('close', function () {
                      $.unwrapContent(config.key);
                  });
              }

              container.empty().append(content);

              $._content_wrapped[config.key] = {
                  close_callback: config.close_callback,
                  wrap: wrap,
                  container: container
              };

              wrap.swapZIndex();

              return config.key;
          },
          unwrapContent: function (key, force_flag) {
              if (typeof $._content_wrapped[key] === 'undefined') {
                  return;
              }

              var data = $._content_wrapped[key];

              if (typeof data.close_callback === 'function') {
                  if (data.close_callback(data.container[0], key, force_flag) === false && !force_flag) {
                      return;
                  }
              }

              data.wrap.remove();

              delete $._content_wrapped[key];

              var last_wrap = null;

              for (var k in $._content_wrapped) {
                  last_wrap = $._content_wrapped[k].wrap;
              }

              var unclickable_helper = $('.osc-wrap-helper-page-unclickable');

              if (last_wrap === null) {
                  $(document.body).removeClass('osc-wrap-helper-page-unscrollable');
                  unclickable_helper.remove();
              } else {
                  unclickable_helper.swapZIndex();
                  last_wrap.swapZIndex();
              }
          },
          disablePage: function () {
              var obj = $('#disable');

              if (obj.css('display') != '') {
                  var docDim = $.getDocumentDim();
                  obj.show().css({
                      width: docDim.w + 'px',
                      height: docDim.h + 'px'
                  });
              }
          },
          enablePage: function () {
              $('#disable').hide();
          },
          showLoadingForm: function (message, skipDisable) {
              if (!skipDisable) {
                  $.disablePage();
              }

              message = !message ? 'Please wait for loading' : message;

              $('#loading-message').html(message + '...');
              $('#loading').show().moveToCenter();
          },
          hideLoadingForm: function (skipAvailable) {
              $('#loading').hide();

              if (!skipAvailable) {
                  $.enablePage();
              }
          },
          getDocumentDim: function () {
              var mode = document.compatMode;

              var browserDim = $.getBrowserDim();

              return {
                  w: Math.max((mode != 'CSS1Compat') ? document.body.scrollWidth : document.documentElement.scrollWidth, browserDim.w),
                  h: Math.max((mode != 'CSS1Compat') ? document.body.scrollHeight : document.documentElement.scrollHeight, browserDim.h)
              };
          },
          getViewPort: function (win) {
              if (!win) {
                  win = window;
              }

              var CSS1Mode = window.document.compatMode == 'CSS1Compat';

              return {
                  x: win.pageXOffset || (CSS1Mode ? win.document.documentElement.scrollLeft : win.document.body.scrollLeft),
                  y: win.pageYOffset || (CSS1Mode ? win.document.documentElement.scrollTop : win.document.body.scrollTop),
                  w: win.innerWidth || (CSS1Mode ? win.document.documentElement.clientWidth : win.document.body.clientWidth),
                  h: win.innerHeight || (CSS1Mode ? win.document.documentElement.clientHeight : win.document.body.clientHeight)
              };
          },
          getBrowserDim: function () {
              var dim = {
                  w: self.innerWidth,
                  h: self.innerHeight
              };

              var mode = document.compatMode;

              if ((mode || $.browser.msie) && !$.browser.opera) {
                  dim.w = (mode == 'CSS1Compat') ? document.documentElement.clientWidth : document.body.clientWidth;
                  dim.h = (mode == 'CSS1Compat') ? document.documentElement.clientHeight : document.body.clientHeight;
              }

              return dim;
          },
          winReady: function (p) {
              return $.WIN_READY;
          },
          indexOf: function (arr, elt, from) {
              if (arr.indexOf)
                  return arr.indexOf(elt, from);

              from = from || 0;
              var len = arr.length;

              if (from < 0)
                  from += len;

              for (; from < len; from++) {
                  if (from in arr && arr[from] === elt) {
                      return from;
                  }
              }
              return -1;
          },
          formatSize: function (bytes) {
              var i = -1;

              do {
                  bytes = bytes / 1024;
                  i++;
              } while (bytes > 99);

              return Math.max(bytes, 0.1).toFixed(1) + ['kB', 'MB', 'GB', 'TB', 'PB', 'EB'][i];
          }
      });

      window.osc_fixScrollable = function () {
          $(this).fixScrollable();
      };

      $.extend($.fn, {
          bindUp: function () {
              var event_names = {};
              var event_name_alias = {mouseenter: 'mouseover', mouseleave: 'mouseout'};

              this.bind.apply(this, arguments);

              $.each(arguments[0].split(/\s+/), function (k, v) {
                  if (v.trim() === '') {
                      return;
                  }

                  event_names[v.split('.')[0]] = 1;
              });

              return this.each(function () {
                  var is_model_jquery = typeof $._data !== 'undefined' && typeof $.data(this, 'events') === 'undefined';

                  var data_func = is_model_jquery ? $._data : $.data;

                  var events = data_func(this, 'events');

                  if (!events) {
                      return;
                  }

                  for (var event_name in event_names) {
                      var event_handlers = events[event_name];

                      if (!event_handlers) {
                          if (!is_model_jquery || typeof event_name_alias[event_name] === 'undefined') {
                              continue;
                          }

                          event_handlers = events[event_name_alias[event_name]];
                      }

                      event_handlers.splice(0, 0, event_handlers.pop());
                  }
              });
          },
          fixScrollable: function () {
              $(this[0]).attr('tabindex', '-1').focus();
              return this;
          },
          swapZIndex: function () {
              return this.each(function () {
                  var main_node = $(this);

                  if (main_node.css('position') !== 'static') {
                      main_node.css('z-index', 1);

                      var root_node = main_node.parent();

                      while (root_node[0] && root_node[0].nodeName !== 'BODY' && (root_node.css('position') === 'static' || (/[^0-9]/).test(root_node.css('z-index')))) {
                          root_node = root_node.parent();
                      }

                      if (root_node[0]) {
                          var zIndexArr = [1];

                          var zIndexCollector = function (parent, zIndexArr) {
                              $.map(parent.children(), function (node) {
                                  node = $(node);
                                  var zIndex = parseInt(node.css('z-index')) || 1;

                                  if (zIndex > 1) {
                                      zIndexArr.push(zIndex);
                                  } else {
                                      zIndexCollector(node, zIndexArr);
                                  }
                              });
                          };

                          zIndexCollector(root_node, zIndexArr);

                          main_node.css('z-index', Math.max.apply(null, zIndexArr) + 1);
                      }

//                        main_node.css('z-index', 1);
//
//                        var parent = main_node.parent();
//
//                        var max_zindex = Math.max.apply(null, $.map(main_node.parent().children(), function (node) {
//                            node = $(node);
//
//                            //if (node.css('position') !== 'static') {
//                            return parseInt(node.css('z-index')) || 1;
//                            //} else {
//                            //    return 1;
//                            //}
//                        }));
//
//                        main_node.css('z-index', max_zindex + 1);
                  }
              });
          },
          getAttrConfig: function (config_map, opts) {
              var config = {}, config_value, is_json, keep_attr = true, attr_prefix = 'osc-', config_key, attr_name;

              if (typeof opts === 'object' && opts !== null) {
                  if (typeof opts.keep_attr !== 'undefined') {
                      keep_attr = opts.keep_attr ? true : false;
                  }

                  if (typeof opts.attr_prefix !== 'undefined') {
                      attr_prefix = (opts.attr_prefix + '').trim();
                  }
              }

              for (var config_key in config_map) {
                  if (Array.isArray(config_map[config_key])) {
                      attr_name = config_map[config_key][0];
                      is_json = config_map[config_key][1];
                  } else {
                      attr_name = config_map[config_key];
                      is_json = false;
                  }

                  attr_name = attr_prefix + attr_name;

                  if (!this.hasAttr(attr_name)) {
                      continue;
                  }

                  config_value = this.attr(attr_name);

                  if (!keep_attr) {
                      this.removeAttr(attr_name);
                  }

                  if (is_json) {
                      eval('config_value = ' + config_value);
                  }

                  config[config_key] = config_value;
              }

              return config;
          },
          hasAttr: function (attr_name) {
              var attr_value = this.attr(attr_name);
              return typeof attr_value !== 'undefined' && attr_value !== false;
          },
          getVal: function () {
              var elm = $(this[0]);

              var _custom = elm.data('custom_method__getVal');

              if (_custom) {
                  return _custom.apply(this, arguments);
              }

              switch (this[0].tagName.toLowerCase()) {
                  case 'input':
                      var type = elm.attr('type').toLowerCase();

                      if (type == 'radio' || type == 'checkbox') {
                          var name = elm.attr('name');

                          if (name) {
                              var parent = elm.parent();

                              while (parent[0].tagName.toLowerCase() != 'body' && parent[0].tagName.toLowerCase() != 'form') {
                                  parent = parent.parent();
                              }

                              var collection = 'input[type="' + type + '"][name="' + name + '"]';

                              if (parent[0].tagName.toLowerCase() == 'body') {
                                  collection = ':not(form) ' + collection;
                              }

                              collection = parent.find(collection);

                              if (collection.length > 1) {
                                  var data = [];

                                  collection.each(function () {
                                      if (this.checked) {
                                          data.push($(this).val());
                                      }
                                  });

                                  return data;
                              }
                          }

                          return this[0].checked ? elm.val() : '';
                      } else if (!type || type == 'text' || type == 'password') {
                          return elm.attr('placeholder') && elm.val() == elm.attr('placeholder') ? '' : elm.val();
                      }
                      break;
                  case 'select':
                      var data = [];
                      for (var i = 0; i < this[0].options.length; i++) {
                          if (this[0].options[i].selected) {
                              data.push(this[0].options[i].value);
                          }
                      }
                      return data;
                      break;
                  default:
                      if (elm.data('osc-editor')) {
                          return $(this).data('osc-editor').getContent();
                      }
              }

              return elm.val();
          },
          findAll: function (selector) {
              return this.find(selector).andSelf().filter(selector);
          },
          realWidth: function () {
              var dom = this[0];

              if (dom.tagName != 'IMG') {
                  return $(dom).width();
              }

              var img = $(dom);

              if (img.prop('naturalWidth') == undefined) {
                  var tmp = $('<img/>').attr('src', img.attr('src'));
                  img.prop('naturalWidth', tmp[0].width);
                  img.prop('naturalHeight', tmp[0].height);
              }

              return img.prop('naturalWidth');

          },
          realHeight: function () {
              var dom = this[0];

              if (dom.tagName != 'IMG') {
                  return $(dom).height();
              }

              var img = $(dom);

              if (img.prop('naturalHeight') == undefined) {
                  var tmp = $('<img/>').attr('src', img.attr('src'));
                  img.prop('naturalWidth', tmp[0].width);
                  img.prop('naturalHeight', tmp[0].height);
              }

              return img.prop('naturalHeight');

          },
          moveToTop: function () {
              return this.each(function () {
                  var scroll_top = $(window).scrollTop();
                  var scroll_left = $(window).scrollLeft();
                  var win_width = $(window).width();
                  var win_height = $(window).height();

                  $(this).offset({
                      top: scroll_top + 100,
                      left: scroll_left + (win_width - $(this).outerWidth()) / 2
                  });
              });
          },
          moveToCenter: function () {
              return this.each(function () {
                  var scroll_top = $(window).scrollTop();
                  var scroll_left = $(window).scrollLeft();
                  var win_width = $(window).width();
                  var win_height = $(window).height();

                  $(this).offset({
                      top: scroll_top + (win_height - $(this).outerHeight()) / 2,
                      left: scroll_left + (win_width - $(this).outerWidth()) / 2
                  });
              });
          },
          realOffset: function () {
              return $(this[0]).offset();
          },
          markElement: function (sExpandoProperty) {
              return this.each(function () {
                  this['MenuElem'] = 1;

                  for (var x = 0, node = null; x < this.childNodes.length; x++) {
                      node = this.childNodes[x];

                      if (node.tagName) {
                          $(node).markElement(sExpandoProperty);
                      }
                  }
              });
          }
      });

      $(window).load(function () {
          $.WIN_READY = true;
      });

      var _RESTORE_HISTORY_STATE_TITLE = $('head title').text();
      var _RESTORE_HISTORY_STATE_URL = window.location.href;
      var _RESTORE_HISTORY_STATE_KEY = $.makeUniqid();
      var _RESTORE_HISTORY_STATE_CALLBACK = null;
      var _HISTORY_STATE_RESTORING_FLAG = false;

      var new_state = {};

//        if(history.state) {
//            $.extend(new_state, history.state);
//        }

      new_state.key = _RESTORE_HISTORY_STATE_KEY;

      history.replaceState(new_state, _RESTORE_HISTORY_STATE_TITLE, _RESTORE_HISTORY_STATE_URL);

      $.extend($, {
          pushHistory: function (url, title, callback, params) {
              if (typeof callback !== 'undefined') {
                  if (typeof params === 'undefined' || params === null) {
                      params = [];
                  } else if (params.constructor.toString().indexOf('Array') != -1) {
                      params = [params];
                  }

                  params.unshift(url.replace(/^http(s)?:/, '').replace(/\/{2,}/g, '/').replace((my_base_url.replace(/^http(s)?:/, '') + my_pligg_base + '/').replace(/\/{2,}/g, '/'), ''));

                  var state = {callback: callback, params: JSON.stringify(params)};
              } else {
                  var state = {key: $.makeUniqid()};
                  _RESTORE_HISTORY_STATE_KEY = state.key;
                  _RESTORE_HISTORY_STATE_CALLBACK = null;
              }


              try {
                  window.history.pushState(state, title, url);
              } catch (e) {
                  window.location = url;
              }

              _processHistoryState(state);
          },
          setRestoreHistoryStateCallback: function (callback) {
              _RESTORE_HISTORY_STATE_CALLBACK = callback;
          },
          restoreHistoryState: function () {
              if (!_HISTORY_STATE_RESTORING_FLAG) {
                  $.pushHistory(_RESTORE_HISTORY_STATE_URL, _RESTORE_HISTORY_STATE_TITLE);
              }
          }
      });

      function _processHistoryState(state) {
          if (state.key && state.key === _RESTORE_HISTORY_STATE_KEY) {
              _HISTORY_STATE_RESTORING_FLAG = true;

              if (_RESTORE_HISTORY_STATE_CALLBACK) {
                  _RESTORE_HISTORY_STATE_CALLBACK();
                  _RESTORE_HISTORY_STATE_CALLBACK = null;
              }

              _HISTORY_STATE_RESTORING_FLAG = false;

              return;
          }

          if (state.callback) {
              callback_params = [];

              eval('state.params = ' + state.params);

              state.params[0] = my_base_url + my_pligg_base + '/' + state.params[0];

              for (var x = 0; x < state.params.length; x++) {
                  callback_params.push('state.params[' + x + ']');
              }

              eval(state.callback + '(' + callback_params.join(',') + ');');
          } else {
              window.location.reload();
          }
      }

      $(window).bind('popstate', function (e) {
          if (e.originalEvent.state) {
              _processHistoryState(e.originalEvent.state);
          } else if (window.location.href.split('#')[0] !== _RESTORE_HISTORY_STATE_URL.split('#')[0]) {
              window.location.reload();
          }
      });
  })(jQuery);
}

osc_core_setter();
