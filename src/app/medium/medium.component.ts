import { Renderer2, Component, OnInit, Inject, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';

declare var $: any

@Component({
  selector: 'app-medium',
  templateUrl: './medium.component.html',
})

export class MediumComponent implements OnInit, AfterViewInit {

  richEditorFolder = "../../assets/vendor/rich-editor";

  constructor(private renderer2: Renderer2, @Inject(DOCUMENT) private _document) {

  }

  /**
   * Render block of scripts tag to load javascript
   * @param documentBody
   * @param renderer
   * @param scripts
   */
  renderScripts(documentBody, renderer, scripts) {
    scripts.map(script => {
      const scriptElement = renderer.createElement('script');
      scriptElement.type = 'text/javascript';
      scriptElement.src = script;
      scriptElement.text = ``;
      this.renderer2.appendChild(documentBody, scriptElement);
    })
  }

  /**
   * Render block of <link> tag to render CSS
   * @param documentHead
   * @param renderer
   * @param styles
   */
  renderStyles(documentHead, renderer, styles) {
    styles.map(style => {
      const styleTag = renderer.createElement('link');
      styleTag.href = style;
      styleTag.rel = "stylesheet";
      this.renderer2.appendChild(documentHead, styleTag);
    })
  }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    // Render scripts
    var scripts = [
      `${this.richEditorFolder}/services/osecore/osecore.js`,
      `${this.richEditorFolder}/services/osecore/dragger.js`,
      `${this.richEditorFolder}/services/osecore/osecore-dragger.js`,
      `${this.richEditorFolder}/services/osecore/jquery-scroller.js`,
      `${this.richEditorFolder}/services/osecore/osecore-scroller.js`,
      `${this.richEditorFolder}/services/osecore/uploader.js`,
      `${this.richEditorFolder}/js/components/icons.js`,
      `${this.richEditorFolder}/js/mediumEditorTemplates.js`,
      `${this.richEditorFolder}/js/plugins/images.js`,
      `${this.richEditorFolder}/js/plugins/core.js`,
      `${this.richEditorFolder}/js/plugins/hr.js`,
      `${this.richEditorFolder}/js/plugins/highlight.js`,
      `${this.richEditorFolder}/js/plugins/album.js`,
      `${this.richEditorFolder}/js/plugins/custom-quote.js`,
      /* writing document, experimental purposes */
      `${this.richEditorFolder}/js/plugins/divider.js`,
      `${this.richEditorFolder}/js/plugins/highlighter.js`,
      `${this.richEditorFolder}/js/plugins/quoter.js`,
      /* end experiment */
      `${this.richEditorFolder}/js/plugins/image-description.js`,
      `${this.richEditorFolder}/js/medium-insert-plugin-base.js`
    ];

    this.renderScripts(this._document.body, this.renderer2, scripts);

    // Render styles
    var styles = [
      `${this.richEditorFolder}/css/style.css`,
      `${this.richEditorFolder}/css/mediumEditor.css`,
      `${this.richEditorFolder}/css/dark.css`,
      `${this.richEditorFolder}/css/rich-editor-content.css`,
      `${this.richEditorFolder}/services/osecore/uploader.css`
    ]

    this.renderStyles(this._document.head, this.renderer2, styles);

  }

  /**
   * Publish article
   */
  publish() {
    //remove all inner html elements without buttons and active caret;
    var innerHtmlContent = $(".editable").clone();
    innerHtmlContent.find('.medium-insert-buttons').remove();
    innerHtmlContent.find('p.medium-insert-active').remove();
    //get inner content
    console.log(innerHtmlContent.html())
  }
}
